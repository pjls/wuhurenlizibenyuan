<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\common\controller;

use think\Db;
use cmf\controller\RestBaseController;

class BaseController extends RestBaseController
{
    /**
     * 架构函数
     * @access public
     */
    public function initialize(){
        $module = request()->module();
        $controller = request()->controller();
        $action = request()->action();
        $url = strtolower($module.'/'.$controller.'/'.$action);
        $urlArray = [
            'article/index/article_list',
            'article/index/article_page',
            'index/slide/slide_item',
            'job/position/position_list',
            'workstu/position/position_list',
        ];
        if(session('ADMIN_ID') || in_array($url,$urlArray)) {
            return true;
        }
        $this->checkLogin();
        return true;
    }

    /**
     * 析构函数
     * @access public
     * @throws \think\Exception
     */
    public function __destruct(){
        $module = request()->module();
        $controller = request()->controller();
        $action = request()->action();
        $url = strtolower($module.'/'.$controller.'/'.$action);
        $loginIp = request()->ip();

        //触发到积分操作
        if($action = Db::table('cmf_user_action')->where('FIND_IN_SET("'.$url.'",url)')->find()){
            $user_id = request()->uid;
            $now = time();
            $today = strtotime('today');
            $where = ['user_id'=>$user_id,'action_id'=>$action['id']];

            //周期按天算，加入筛选
            if($action['cycle_type'] == 1) $where['date'] = $today;

            Db::startTrans();
            try{
                //任何操作第一次都操作成功，可变更积分
                if(!$log = Db::table('cmf_user_action_log')->where($where)->field('id,count')->find()){
                    Db::table('cmf_user_action_log')->insert(['action_id'=>$action['id'],'date'=>$today,'user_id'=>$user_id,'ip'=>$loginIp]);
                    //无限次数 || 限定次数内  可变更积分,以及操作次数
                }elseif($action['cycle_type'] == 0 || $log['count'] < $action['reward_number'] ){
                    Db::table('cmf_user_action_log')->where('id',$log['id'])->setInc('count');
                    //超过限定次数，删除变更积分权利
                }else{
                    unset($action['score']);
                    unset($action['grow_score']);
                }

                //变更积分
                if(@$action['score']){
                    Db::table('cmf_user')->where('id',$user_id)->setInc('score',$action['score']);
                    Db::table('cmf_user_score_log')->insert(['user_id'=>$user_id,'create_time'=>$now,'action'=>$action['name'],'score'=>$action['score']]);
                }
                if(@$action['grow_score']){
                    Db::table('cmf_user')->where('id',$user_id)->setInc('grow_score',$action['grow_score']);
                    Db::table('cmf_user_grow_score_log')->insert(['user_id'=>$user_id,'create_time'=>$now,'action'=>$action['name'],'grow_score'=>$action['grow_score']]);
                }
                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                // 回滚事务
                Db::rollback();
                return $e->getMessage();
            }

        }
        return true;
    }

    public function checkLogin(){
        // 存在签名，验证
        if ($sign = request()->header(config('JWT_SIGN_NAME'))) {
            file_put_contents('./sign_name.log',$sign,FILE_APPEND);
            try{
                $decoded = \Firebase\JWT\JWT::decode($sign, config('PUBLIC_KEY'), array('RS256'));
            }catch(\UnexpectedValueException $e){
                // 捕捉异常，直接抛出报错
                json(['code'=>0,'status'=>0,'msg'=>$e->getMessage(),'errcode'=>500])->send();
                exit();
            }
            request()->uid = $decoded->uid;
            try{
                request()->user_relation = Db::table('cmf_user_relation')->find($decoded->uid);
            }catch(\Exception $e){}
            if(Db::table('cmf_company')->where('id',request()->user_relation['company_id'])->value('delete_time') != 0 && request()->user_relation['role_id'] == 3){
                Db::table('cmf_user_relation')->where('user_id',$decoded->uid)->update(['role_id'=>5,'company_id'=>0]);
                json(['code'=>0,'status'=>0,'msg'=>'企业已被删除！请重新实名认证！','errcode'=>500,'errRedirect'=>'/pages/home/index'])->send();
                exit();
            }elseif(Db::table('cmf_company')->where('id',request()->user_relation['company_id'])->value('status') == 0 && request()->user_relation['role_id'] == 3){
                json(['code'=>0,'status'=>0,'msg'=>'企业已被禁用！请联系管理员开通！','errcode'=>500])->send();
                exit();
            }elseif(Db::table('cmf_user')->where('id',$decoded->uid)->value('user_status') == 0){
                json(['code'=>0,'status'=>0,'msg'=>'账号已被禁用！请联系管理员开通！','errcode'=>500])->send();
                exit;
            }

        }else {
            json(['code'=>0,'status'=>0,'msg'=>'无权限访问','errcode'=>500])->send();
            exit();
        }
    }

    // api 首页
    public function index()
    {
        $this->success("欢迎来到芜湖人力资源！");
    }
}