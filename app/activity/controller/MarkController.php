<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\activity\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\activity\model\ActivityModel;
use app\activity\model\ActivityMemberModel;

class MarkController extends AdminBaseController
{

    /**
     * 创客活动
     * @adminMenu(
     *     'name'   => '创客活动',
     *     'parent' => 'activity/Park/default2',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '创客活动列表',
     *     'param'  => ''
     * )
     * @throws \think\exception\DbException
     */
    public function index(){
        $ActivityModel = new ActivityModel();
        $list = $ActivityModel->where(['type'=>2,'is_admin'=>1,'delete_time'=>0])->paginate(10);
        $this->assign("list", $list);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    /**
     * 添加活动页面
     * @adminMenu(
     *     'name'   => '添加活动页面',
     *     'parent' => 'activity/Park/index',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加活动页面',
     *     'param'  => ''
     * )
     */
    public function add(){
        return $this->fetch();
    }
    /**
     * 添加活动提交
     * @adminMenu(
     *     'name'   => '添加活动提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加活动提交',
     *     'param'  => ''
     * )
     */
    public function add_post(){
        $ActivityModel = new ActivityModel();
        if (!$this->request->isPost()) $this->error('访问失败');

        $data = $this->request->param();
        $result = $this->validate($data, 'Activity');
        if ($result !== true) {
            $this->error($result);
        }
        $data['is_admin'] = 1;
        $data['type'] = 2;
        $data['user_id'] = cmf_get_current_admin_id();
        $data['create_time'] = $data['update_time'] = time();
        $data['cut_date'] = strtotime($data['cut_date']);
        if(empty($data['is_top'])) $data['is_top'] = 0;

        $add = $ActivityModel->insert($data);

        $add?$this->success('添加成功',url('index')):$this->error('添加失败');
    }

    /**
     * 活動編輯
     * @adminMenu(
     *     'name'   => '活動編輯',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '活動編輯',
     *     'param'  => ''
     * )
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function edit(){
        $ActivityModel = new ActivityModel();
        $id = $this->request->param('id', 0, 'intval');
        $info = $ActivityModel->where('id',$id)->find();
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * 编辑活动提交
     * @adminMenu(
     *     'name'   => '编辑活动提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '编辑活动提交',
     *     'param'  => ''
     * )
     */
    public function edit_post(){
        if (!$this->request->isPost()) $this->error('访问方式错误');
        $data = $this->request->param();
        $ActivityModel = new ActivityModel();
        $data['update_time'] = time();
        $data['cut_date'] = strtotime($data['cut_date']);
        if(empty($data['is_top'])) $data['is_top'] = 0;
        $save = $ActivityModel->save($data,['id'=>$data['id']]);
        $save?$this->success('编辑成功'):$this->error('编辑失败');
    }

    /**
     * 活動详情
     * @adminMenu(
     *     'name'   => '活動详情',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '活動详情',
     *     'param'  => ''
     * )
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function info(){
        $ActivityModel = new ActivityModel();
        $id = $this->request->param('id', 0, 'intval');
        $info = $ActivityModel->where('id',$id)->find();
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * 活動删除
     * @adminMenu(
     *     'name'   => '活動删除',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '活動删除',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function delete(){
        $ActivityModel = new ActivityModel();
        $param           = $this->request->param();

        if (isset($param['id'])) {
            $id           = $this->request->param('id', 0, 'intval');
            $result       = $ActivityModel->where('id', $id)->find();
            $data         = [
                'object_id'   => $result['id'],
                'create_time' => time(),
                'table_name'  => 'activity',
                'name'        => $result['title'],
                'user_id'     => cmf_get_current_admin_id()
            ];
            $resultPortal = $ActivityModel
                ->where('id', $id)
                ->update(['delete_time' => time()]);
            if ($resultPortal) {
                Db::name('recycleBin')->insert($data);
            }
            $this->success("删除成功！");
        }

        if (isset($param['ids'])) {
            $ids     = $this->request->param('ids/a');
            $recycle = $ActivityModel->where('id', 'in', $ids)->select();
            $result  = $ActivityModel->where('id', 'in', $ids)->update(['delete_time' => time()]);
            if ($result) {
                foreach ($recycle as $value) {
                    $data = [
                        'object_id'   => $value['id'],
                        'create_time' => time(),
                        'table_name'  => 'activity',
                        'name'        => $value['title'],
                        'user_id'     => cmf_get_current_admin_id()
                    ];
                    Db::name('recycleBin')->insert($data);
                }
                $this->success("删除成功！");
            }
        }
    }

    /**
     * 报名名单
     * @adminMenu(
     *     'name'   => '报名名单',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '报名名单',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function sign_list(){
        $activity_id = $this->request->param('id', 0, 'intval');
        $keyword = $this->request->param('keyword','','trim');
        $where['m.activity_id'] = $activity_id;
        if($keyword) $where['u.real_name|u.mobile|u.company'] = ['like','%'.$keyword.'%'];

        $ActivityMemberModel = new ActivityMemberModel();
        $list = $ActivityMemberModel->alias('m')
            ->join('cmf_user u','u.id = m.user_id')
            ->join('cmf_activity a','a.id = m.activity_id')
            ->field('If(u.real_name,u.real_name,u.user_nickname) as user_name,u.mobile,u.company,a.title as activity_name,m.user_id,m.activity_id,m.id,m.create_time,m.delete_time')
            ->order('m.delete_time asc')
            ->where($where)->paginate(10);
        $this->assign('list',$list);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    /**
     * 活动置顶
     * @adminMenu(
     *     'name'   => '活动置顶',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '活动置顶',
     *     'param'  => ''
     * )
     */
    public function top()
    {
        $param           = $this->request->param();
        $ActivityModel = new ActivityModel();

        if (isset($param['ids']) && isset($param["yes"])) {
            $ids = $this->request->param('ids/a');

            $ActivityModel->where('id', 'in', $ids)->update(['is_top' => 1]);

            $this->success("置顶成功！", '');

        }

        if (isset($_POST['ids']) && isset($param["no"])) {
            $ids = $this->request->param('ids/a');

            $ActivityModel->where('id', 'in', $ids)->update(['is_top' => 0]);

            $this->success("取消置顶成功！", '');
        }
    }

}