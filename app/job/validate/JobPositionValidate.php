<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\job\validate;

use think\Validate;

class JobPositionValidate extends Validate
{
    protected $rule = [
        'type_id' => 'require',
        'experience_id' => 'require',
        'education_id' => 'require',
        'area_id' => 'require',
        'name' => 'require|min:2',
        'work_number' => 'require',
        'contact' => 'require',
        'phone' => 'require|checkPhone:thinkphp',
        'address' => 'require',
        'lat' => 'require',
        'lng' => 'require',
        'description' => 'min:20',
        'welfare' => 'min:20',
    ];

    protected $message = [
        'type_id.require' => '请选择职业类型',
        'experience_id.require' => '请选择工作经验要求',
        'education_id.require' => '请选择学历要求',
        'area_id.require'  => '请选择地址',
        'name.require' => '请填写职位名称',
        'name.min' => '职位名称不得少于两个字',
        'work_number.require' => '请填写招聘人数',
        'contact.require' => '请填写职位联系人',
        'phone.require' => '请填写职位联系人电话',
        'address.require'  => '请选择地址',
        'lat.require'  => '请选择地址',
        'lng.require'  => '请选择地址',
        'description.min'  => '岗位职责不得少于两个字',
        'welfare.min'  => '岗位福利不得少于两个字',
    ];

    // protected $scene = [
    // ];

    // 自定义验证规则
    protected function checkPhone($value)
    {
        if(!preg_match('/^1\d{10}$/',$value) && !preg_match('/^([0-9]{3,4}-)?[0-9]{7,8}$/',$value)){
            return '联系电话格式错误';
        }else{
            return true;
        }
    }
}