<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\activity\controller;

use think\Db;
use api\common\controller\BaseController;
use api\activity\model\ActivityMemberModel;

class SignController extends BaseController
{
    /**
     * 活动下的报名
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function activity_sign(){
        $ActivityMemberModel = new ActivityMemberModel();
        $activity_id = $this->request->param('activity_id', 0, 'intval');
        $page = $this->request->param('page', 0, 'intval');
        $limit = $this->request->param('limit', 10, 'intval');

        $where = ['m.activity_id'=>$activity_id];

        $data['list']  = $ActivityMemberModel->alias('m')
            ->join('cmf_activity a','a.id = m.activity_id')
            ->join('cmf_user u','u.id = m.user_id')
            ->where($where)
            ->order('create_time desc')
            ->limit(($page*$limit),$limit)
            ->field('u.real_name,a.title,m.user_id,m.id,m.activity_id,FROM_UNIXTIME(m.create_time,"%Y-%m-%d %H:%i:%s") as create_time,m.delete_time')
            ->select()->toArray();
        $data['count'] = $ActivityMemberModel->alias('m')->where($where)->count();
        $data['page'] = $page;
        $data['limit'] = $limit;

        $this->success('获取成功',$data);
    }

    /**
     * 活动报名
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function sign(){
        $ActivityMemberModel = new ActivityMemberModel();
        $data['activity_id'] = $this->request->param('activity_id',0,'intval');
        $data['create_time'] = $data['update_time'] = time();
        $data['user_id'] = request()->uid;

        //活动结束|已经报名，禁止报名
        $end_time = Db::table('cmf_activity')->where('id',$data['activity_id'])->value('cut_date');
        if($end_time < time()) $this->error('活动已结束');
        if($ActivityMemberModel->where(['activity_id'=>$data['activity_id'],'user_id'=>$data['user_id'],'delete_time'=>0])->count()>0) $this->error('你已经报过名了');

        Db::startTrans();
        try{
            $add = $ActivityMemberModel->insertGetId($data);
            //记录报名数
            Db::table('cmf_activity')->where('id',$data['activity_id'])->setInc('sign_count');
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('系统错误');
            exit;
        }

        $add?$this->success('报名成功',['id'=>$add]):$this->error('报名失败');
    }

    /**
     * 活动报名撤销
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function sign_delete(){
        $id = $this->request->param('id', 0, 'intval');
        $user_id = request()->uid;
        $activity = Db::table('cmf_activity_member')->where('id',$id)->field('activity_id,user_id,delete_time')->find();
        if($user_id != @$activity['user_id']) $this->error('不是您的报名！');
        if($activity['delete_time'] != 0) $this->error('您已经删除了！');

        Db::startTrans();
        try{
            Db::table('cmf_activity_member')->where('id',$id)->update(['delete_time'=>time()]);
            //记录报名数
            Db::table('cmf_activity')->where('id',$activity['activity_id'])->setDec('sign_count');
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('撤销失败');
            exit;
        }
        $this->success('撤销成功');
    }

    /**
     * 我的活动报名
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function my_sign(){
        $ActivityMemberModel = new ActivityMemberModel();
        $page = $this->request->param('page', 0, 'intval');
        $limit = $this->request->param('limit', 10, 'intval');
        $user_id = request()->uid;

        $where = ['m.user_id'=>$user_id,'m.delete_time'=>0];

        $data['list']  = $ActivityMemberModel->alias('m')
            ->join('cmf_activity a','a.id = m.activity_id')
            ->where($where)
            ->order('m.create_time desc')
            ->limit(($page*$limit),$limit)
            ->field('a.*,If(a.cut_date < unix_timestamp(now()),1,0) as is_cut,m.id,m.activity_id,m.create_time,IF(a.delete_time = 0,0,1) as is_delete,IF(m.user_id = a.user_id,1,0) as is_editor')
            ->select()->toArray();
        $data['count'] = $ActivityMemberModel->alias('m')->where($where)->count();
        $data['page'] = $page;
        $data['limit'] = $limit;

        $this->success('获取成功',$data);
    }
}