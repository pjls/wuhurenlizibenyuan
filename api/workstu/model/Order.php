<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls < 1512273960@qq.com>
// +----------------------------------------------------------------------
namespace api\workstu\model;

use think\Model;

class Order extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'cmf_work_student_order_post';


    public static  $STATUS = array(
        0 => "未查看",
        1 => "被查看",
        2 => "不合适",
        3 => "有意向"
    );

    //预编译
    public function getStatusNameAttr($value,$data){
        return  @Order::$STATUS[$data['check_status']]?:$data['check_status'];
    }

    //投递的订单获取
    public function get_order($where,$field,$page,$limit){
        try{
            $list = $this->alias('o')
                ->join('cmf_user u','o.user_id = u.id')
                ->join('cmf_job_resume r','r.id = o.resume_id')
                ->join('cmf_work_student_position p','p.id = o.position_id')
                ->join('cmf_company c','c.id = p.company_id')
                ->join('cmf_work_student_close_type compute','compute.id = p.compute_id')
                ->order('o.create_time desc')
                ->where($where)->limit(($page*$limit),$limit)
                ->field($field)
                ->select()
                ->append(['status_name']);
        }catch (\Exception $e){
            return false;
        }
        foreach($list as $k => $v){
            if(isset($v['logo'])) $list[$k]['logo'] = cmf_get_image_preview_url($v['logo']);
        }
        return $list;
    }

    //投递的订单数量获取
    public function get_count($where){
        try{
            $count = $this->alias('o')
                ->join('cmf_work_student_position p','p.id = o.position_id')
                ->where($where)
                ->count();
        }catch (\Exception $e){
            return false;
        }
        return $count;
    }
}