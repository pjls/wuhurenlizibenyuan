<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\job\controller;

use think\Db;
use api\common\controller\BaseController;
use api\company\model\Company;


class CompanyController extends BaseController
{

    //公司列表筛选项获取
    public function search_type(){
        try{
            $data['industryList'] = Db::table('cmf_job_type')->where('Left(id,2) = 00')->where('id != 0')->field('id,name')->order('id asc')->select();
        }catch (\Exception $e) {
            $this->error('获取失败');
            exit;
        }
        $this->success('获取成功',$data);
    }

    /**
     * 十大热门公司列表
     * @throws \think\Exception
     */
    public function hot_company(){
        $Company = new Company();
        $data['list'] = $Company->alias('c')
            ->join('cmf_job_position p','p.company_id = c.id','left')
            ->where(['p.delete_time'=>0])->group('c.id')->field('c.id,c.area_id,c.user_id,c.company_number,c.address,c.company,c.logo,sum(p.sign_count) as sign_count')->order('sign_count desc')->limit('10')->select();
        $this->success('获取成功',$data);
    }

    //全部公司列表
    public function index($page = 0,$limit = 10,$keyword = '',$industry_id = 0,$area_id = 0){
        $Company = new Company();
        $where['c.status'] = 1;//非禁用
        $where['c.delete_time'] = 0;//非删除
        $whereString = '';
        if($keyword) $where['c.company|c.address'] = ['like','%'.$keyword.'%'];
        if($industry_id) $where['c.type_id'] = $industry_id;
        if($area_id && substr($area_id,-2) == '00'){
            $whereString = 'LEFT(c.area_id,4) = '.substr($area_id,0,4);
        }elseif($area_id){
            $where['c.area_id'] = $area_id;
        }

        //是否为推荐企业
        if($is_recommend = $this->request->get('is_recommend')){
            $where['c.is_recommend'] = 1;
        }

        try{
            $data['list'] = $Company->alias('c')
                ->join('cmf_job_position p','p.company_id = c.id','left')
                ->join('cmf_job_type industry','industry.id = c.type_id','left')
                ->join('cmf_area a','c.area_id = a.id','left')
                ->where($where)->where($whereString)->group('c.id')->field('c.id,c.area_id,c.user_id,c.company_number,c.address,c.company,c.logo,a.name as area_name,industry.name as industry_name,sum(p.sign_count) as sign_count,sum(p.delete_time = 0) as position_count')->order('sign_count desc')->limit($page*$limit,$limit)->select();
            $data['count'] = $Company->alias('c')->where($where)->where($whereString)->count();
        }catch (\Exception $e){
            return $e->getMessage();
        }
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
        exit;
    }

    private $CompanyTable = 'company';

    /**
     * 公司详情
     * @throws \think\Exception
     */
    public function info(){
        $id = $this->request->param('id',0,'intval');
        $user_id = request()->uid;
        $Company = new Company();
        $info = $Company::get($id);
        if(!$info) $this->error('没找到该公司！');
        $info = $info->hidden(['type','contact_name','contact_phone','checked','status','business_number']);
        $info['area_name'] = Db::table('cmf_area')->where('id',$info['area_id'])->value('name');
        $info['industry_name'] = Db::table('cmf_job_type')->where('id',$info['type_id'])->value('name');
        $info['is_like'] = Db::table('cmf_user_favorite')->where(['user_id'=>$user_id,'table_name'=>$this->CompanyTable,'object_id'=>$info['id']])->value('count(id)');
        $this->success('获取成功',$info);
    }
}