<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls < 1512273960@qq.com>
// +----------------------------------------------------------------------
namespace api\user\model;

use think\Model;

class UserGroupScoreLog extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'cmf_user_grow_score_log';

}