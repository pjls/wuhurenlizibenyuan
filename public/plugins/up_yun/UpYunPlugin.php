<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace plugins\up_yun;//Demo插件英文名，改成你的插件英文就行了
use cmf\lib\Plugin;

//Demo插件英文名，改成你的插件英文就行了
class UpYunPlugin extends Plugin
{

    public $info = [
        'name'        => 'UpYun',//Demo插件英文名，改成你的插件英文就行了
        'title'       => '又拍云存储',
        'description' => '又拍云存储：PJLS',
        'status'      => 1,
        'author'      => 'PjLs',
        'version'     => '1.0.0'
    ];

    public $hasAdmin = 0;//插件是否有后台管理界面

    // 插件安装
    public function install()
    {
        $storageOption = cmf_get_option('storage');
        if (empty($storageOption)) {
            $storageOption = [];
        }

        $storageOption['storages']['UpYun'] = ['name' => '又拍云存储', 'driver' => '\\plugins\\up_yun\\lib\\Qiniu'];

        cmf_set_option('storage', $storageOption);
        return true;//安装成功返回true，失败false
    }

    // 插件卸载
    public function uninstall()
    {
        $storageOption = cmf_get_option('storage');
        if (empty($storageOption)) {
            $storageOption = [];
        }

        unset($storageOption['storages']['UpYun']);

        cmf_set_option('storage', $storageOption);
        return true;//卸载成功返回true，失败false
    }

}