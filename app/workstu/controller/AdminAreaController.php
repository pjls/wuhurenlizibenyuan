<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls
// +----------------------------------------------------------------------
namespace app\workstu\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\workstu\model\AreaModel;

class AdminAreaController extends AdminBaseController
{
    /**
     * 筛选区域管理
     * @adminMenu(
     *     'name'   => '筛选区域管理',
     *     'parent' => '',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => 'file',
     *     'remark' => '筛选区域管理',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        $AreaModel  =  new AreaModel;
        try{
            $list = $AreaModel->paginate();
        }catch (\Exception $e) {
            return $e;
        }

        $this->assign("arrStatus", $AreaModel::$STATUS);
        $this->assign("list", $list);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    /**
     * 添加筛选区域
     * @adminMenu(
     *     'name'   => '添加筛选区域',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加筛选区域',
     *     'param'  => ''
     * )
     */
    public function add()
    {
        $AreaModel  =  new AreaModel;
        $this->assign("arrStatus", $AreaModel::$STATUS);
        return $this->fetch();
    }

    /**
     * 添加筛选区域提交
     * @adminMenu(
     *     'name'   => '添加筛选区域提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加筛选区域提交',
     *     'param'  => ''
     * )
     */
    public function addPost()
    {
        $AreaModel  =  new AreaModel;

        $arrData = $this->request->param();
        $AreaModel->isUpdate(false)->allowField(true)->save($arrData);

        $this->success('添加成功');

    }

    /**
     * 更新筛选区域状态
     * @adminMenu(
     *     'name'   => '更新筛选区域状态',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '更新筛选区域状态',
     *     'param'  => ''
     * )
     */
    public function upStatus()
    {
        $AreaModel  =  new AreaModel;
        $intId     = $this->request->param("id");
        $intStatus = $this->request->param("status");
        $intStatus = $intStatus ? 1 : 0;
        if (empty($intId)) {
            $this->error('没有ID');
        }
        $AreaModel->isUpdate(true)->save(["status" => $intStatus], ["id" => $intId]);

        $this->success('操作成功');

    }

}