<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\job\controller;

use think\Db;
use api\common\controller\BaseController;
use api\job\model\Resume;


class ResumeController extends BaseController
{

    /**
     * 架构函数
     * @access public
     * @throws \think\Exception
     */
    public function __construct(){
        parent::__construct();
        $user_id = request()->uid;
        $Resume = new Resume();
        $userInfo = Db::table('cmf_user')->where('id',$user_id)->field('real_name,user_email,sex,birthday,avatar')->find();
        if($Resume->where('user_id',$user_id)->count() <= 0 ){
            $Resume->insertGetId([
                'name'=>$userInfo['real_name'],
                'email'=>$userInfo['user_email'],
                'sex'=>$userInfo['sex'],
                'birthday'=>$userInfo['birthday']?date('Y-m',$userInfo['birthday']):'',
                'avatar'=>$userInfo['avatar'],
                'user_id'=>$user_id,
                'create_time'=>time(),
                'update_time'=>time()
            ]);
        }
    }
    /**
     * 简历基本信息获取
     * @throws \think\Exception
     */
    public function base_info(){
        $Resume = new Resume();
        $user_id = request()->uid;
        $info = $Resume->where('user_id',$user_id)->field('id,name,birthday,sex,email,mobile,avatar')->find();
        $info?$this->success('获取成功',$info):$this->error('获取失败');
    }

    /**
     * 简历基本信息更改提交
     */
    public function base_info_post(){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('name,sex,birthday,mobile,email,avatar','post');
        $result = $this->validate($data,
            [
                'name'  => 'require',
                'sex'  => 'require',
                'birthday'  => 'require',
                'mobile'  => 'require|regex:^1{1}[3-9]{1}[0-9]{9}',
                'email'   => 'require|email',
            ],
            [
                'name.require'  =>  '请输入您的姓名',
                'sex.require'  =>  '请选择您的性别',
                'birthday.require'  =>  '请选择您的生日',
                'mobile.require'  =>  '请输入您的手机号码',
                'mobile.regex'  =>  '手机号格式不正确',
                'email.require' =>  '请输入您的邮箱',
                'email.email' =>  '您的邮箱格式错误',
        ]
        );
        if(true !== $result){
            $this->error($result);// 验证失败 输出错误信息
        }

        $data['update_time'] = time();

        $Resume = new Resume();
        $user_id = request()->uid;
        $info = $Resume->save($data,['user_id'=>$user_id]);
        $info?$this->success('修改成功'):$this->error('修改失败');
    }

    /**
     * 简历基本信息获取
     * @throws \think\Exception
     */
    public function index(){
        $Resume = new Resume();
        $user_id = request()->uid;
        $info = $Resume->where(['user_id'=>$user_id])->find();
        $info['area_name'] = Db::table('cmf_area')->where('id',$info['area_id'])->value('name');
        $info['experience_list'] = Db::table('cmf_job_resume_experience')->where('resume_id',$info['id'])->order('id desc')->select()->toArray();
        $info['education_list'] = Db::table('cmf_job_resume_education')->where('resume_id',$info['id'])->order('education_id desc,id desc')->select()->toArray();
        $info['project_list'] = Db::table('cmf_job_resume_project')->where('resume_id',$info['id'])->order('id desc')->select()->toArray();


        $info?$this->success('获取成功',$info):$this->error('获取失败');

    }

    /**
     * 自我介绍更改提交
     */
    public function description_post(){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('description','post');
        $result = $this->validate($data,
            [
                'description'  => 'require|max:500',
            ],
            [
                'description.require'  =>  '请填写您的自我介绍',
                'description.max'  =>  '超过了500字',
            ]
        );
        if(true !== $result){
            $this->error($result);// 验证失败 输出错误信息
        }
        $data['update_time'] = time();

        $Resume = new Resume();
        $user_id = request()->uid;
        $info = $Resume->save($data,['user_id'=>$user_id]);
        $info?$this->success('修改成功'):$this->error('修改失败');
    }

    /**
     * 个人链接更新
     */
    public function link_post(){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('link,link_desc','post');
        $result = $this->validate($data,
            [
                'link'  => 'require|url',
            ],
            [
                'link.require'  =>  '请填写您的个人链接',
                'link.url'  =>  '链接格式错误',
            ]
        );
        if(true !== $result){
            $this->error($result);// 验证失败 输出错误信息
        }
        $data['update_time'] = time();

        $Resume = new Resume();
        $user_id = request()->uid;
        $info = $Resume->save($data,['user_id'=>$user_id]);
        $info?$this->success('修改成功'):$this->error('修改失败');
    }

    /**
     * 求职意愿修改提交
     */
    public function wish_post(){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('work_date,position,area_id,salary_lowest,salary_highest','post');
        $result = $this->validate($data,
            [
                'position'  => 'require',
                'area_id'  => 'require',
                'salary_lowest'  => 'require',
                'salary_highest'  => 'require',
                'work_date'  => 'require',
            ],
            [
                'position.require'  =>  '请填写期望职位',
                'area_id.require'  =>  '请选择期望地区',
                'salary_lowest.require'  =>  '请选择期待薪资',
                'salary_highest.require'  =>  '请选择期待薪资',
                'work_date.require' =>  '请选择您的参加工作时间',
            ]
        );
        if(true !== $result){
            $this->error($result);// 验证失败 输出错误信息
        }
        $data['update_time'] = time();

        $Resume = new Resume();
        $user_id = request()->uid;
        $info = $Resume->save($data,['user_id'=>$user_id]);
        $info?$this->success('修改成功'):$this->error('修改失败');
    }

    /**
     * 公用编辑提交
     */
    public function com_post(){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('lang,skill,certificate,award,close_search','post');
        if(@$data['lang']) $data['lang'] = $this->request->post('lang','','trim');
        if(@$data['award']) $data['award'] = $this->request->post('award','','trim');
        $data['update_time'] = time();

        $Resume = new Resume();
        $user_id = request()->uid;
        $info = $Resume->save($data,['user_id'=>$user_id]);
        $info?$this->success('修改成功'):$this->error('修改失败');
    }

    /**
     * 个人语言详情
     */
    public function lang_info(){
        $user_id = request()->uid;
        $Resume = new Resume();
        //$data['lang_level'] = $Resume::$LANG_LEVELS;
        $data = $Resume->where('user_id',$user_id)->value('lang');
        $this->success('获取成功',$data);
    }

    /**
     * 个人技能详情
     */
    public function skill_info(){
        $user_id = request()->uid;
        $Resume = new Resume();
        $data['list'] = $Resume->where('user_id',$user_id)->value('skill');
        $data['list'] = explode(',',$data['list']);
        $this->success('获取成功',$data);
    }

    /**
     * 个人证书详情
     */
    public function cert_info(){
        $user_id = request()->uid;
        $Resume = new Resume();
        $data['list'] = $Resume->where('user_id',$user_id)->value('certificate');
        $data['list'] = explode(',',$data['list']);
        $this->success('获取成功',$data);
    }

    /**
     * 奖项详情
     */
    public function award_info(){
        $user_id = request()->uid;
        $Resume = new Resume();
        $data = $Resume->where('user_id',$user_id)->value('award');
        $this->success('获取成功',$data);
    }

    /**
     * 求职者简历信息获取
     * @throws \think\Exception
     */
    public function info(){
        $Resume = new Resume();
        //$user_id = request()->uid;
        $id = $this->request->param('id',0,'intval');
        $user_id = $this->request->param('user_id',0,'intval');
        if($id){
            $info = $Resume::get($id);
        }elseif($user_id){
            $info = $Resume->where('user_id',$user_id)->find();
        }
        if(empty($info)) $this->error('参数错误！获取失败！');
        $info['experience_list'] = Db::table('cmf_job_resume_experience')->where('resume_id',$id)->order('id desc')->select()->toArray();
        $info['education_list'] = Db::table('cmf_job_resume_education')->where('resume_id',$id)->order('education_id desc,id desc')->select()->toArray();
        $info['project_list'] = Db::table('cmf_job_resume_project')->where('resume_id',$id)->order('id desc')->select()->toArray();
        $this->success('获取成功',$info);
    }

    //求职者被发现列表选项获取
    public function search_type(){
        try{
            $data['areaList'] = getAreaList();
            $data['industryList'] = Db::table('cmf_job_type')->where('Left(id,2) = 00')->where('id != 0')->field('id,name')->order('id asc')->select();
        }catch (\Exception $e) {
            $this->error('获取失败');
            exit;
        }
        $this->success('获取成功',$data);
    }

    //求职者被发现列表
    public function resume_list($page = 0,$limit = 10,$keyword = '',$education_id = '',$area_id = ''){

        $Resume = new Resume();
        $whereString = 'close_search = 0 AND mobile != "" AND position != ""';
        $where = [];
        if($keyword) $where['position'] = ['like','%'.$keyword.'%'];
        if($education_id) $where['education_id'] = ['in',$education_id];//区域筛选
        if($area_id && substr($area_id,-2) == '00'){
            $whereString .= ' AND LEFT(r.area_id,4) = '.substr($area_id,0,4);
        }elseif($area_id){
            $where['r.area_id'] = $area_id;
        }
        try{
            $data['list'] = $Resume->alias('r')
                ->join('cmf_job_education e','e.id = r.education_id','left')
                ->where($where)->where($whereString)->limit($page*$limit,$limit)->field('r.*,(TIMESTAMPDIFF(YEAR,r.work_date,CURDATE())) as experience_year,e.name as education_name')->select()->hidden(['method','mobile']);
            $data['count'] = $Resume->alias('r')->where($where)->where($whereString)->count();
        }catch (\Exception $e){
            return $e->getMessage();
        }
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
        exit;
    }

    //邀请面试/获取电话、电话联系
    public function get_mobile(){
        $id = $this->request->param('id',0,'intval');
        $Resume = new Resume();
        $mobile = $Resume->where('id',$id)->value('mobile');

        $mobile?$this->success('获取成功',['mobile'=>$mobile]):$this->error('获取失败');
    }

    //

}