<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\workstu\controller;

use think\Db;
use api\common\controller\BaseController;
use api\workstu\model\WorkStudentPosition as Position;
use api\common\model\User;


class PositionController extends BaseController
{

    //薪酬类别
    public $salaryExt = [['id'=>'1','name'=>'时薪'],['id'=>'2','name'=>'日薪']];
    //排序字段
    public  $orderList = [['id'=>'1','name'=>'按发布时间排序'],['id'=>'2','name'=>'按热度排序']];

    //企业选项接口
    public function get_type(){
        try{
            $data['computeList'] = Db::table('cmf_work_student_close_type')->where('status',1)->order('list_order asc,id asc')->field('name,id')->select()->toArray();
            $data['typeList'] = Db::table('cmf_work_student_type')->where('status',1)->order('list_order asc,id asc')->field('name,id')->select()->toArray();
        }catch (\Exception $e) {
            $this->error('获取失败');
            exit;
        }
        $data['salaryExt'] = $this->salaryExt;
        $this->success('获取成功',$data);
    }

    //企业发布职位提交
    public function add_post(Position $WorkStudentPosition,User $User){

        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('compute_id,type_id,name,start_date,end_date,work_number,salary,salary_method,phone,description,contact,lat,lng,address,area_id','post');

        /*$company_id = request()->user_relation['company_id'];
        $companyInfo = Db::table('cmf_company')->where('id',$company_id)->field('')->find();
        if($companyInfo)*/

        $result = $this->validate($data, 'work_student_position');
        if ($result !== true) $this->error($result);

        $data['start_date'] = strtotime($data['start_date']);
        $data['end_date'] = strtotime($data['end_date']);

        $data['user_id'] = request()->uid;

        try{
            $userInfo = $User->where('id',$data['user_id'])->field('real_name as name,mobile')->find();
            if(@empty($data['phone'])) $data['phone'] = $userInfo['mobile'];
            if(@empty($data['name'])) $data['contact'] = $userInfo['name'];
        }catch(\Exception $e){}

        $data['company_id'] = request()->user_relation['company_id'];
        $data['create_time'] = $data['update_time'] = time();
        $add = $WorkStudentPosition->insertGetId($data);
        $add?$this->success('发布成功',['id'=>$add]):$this->error('发布失败');
    }

    //企业编辑职位页面
    public function edit($id = 0,Position $Position){
        try{
            $data = $Position->field('id,type_id,delete_time,compute_id,name,FROM_UNIXTIME(start_date,"%Y-%m-%d") as start_date,FROM_UNIXTIME(end_date,"%Y-%m-%d") as end_date,work_number,salary,salary_method,phone,contact,description,area_id,lat,lng,address')->find($id);
        }catch (\Exception $e) {
            $this->error('获取失败');
            exit;
        }
        if($data['delete_time'] < 0 ) $this->error('您的职位被管理员删除了！');
        if($data['delete_time'] > 0 ) $this->error('您的职位已经删除了！');
        $this->success('获取成功',$data);
    }

    //企业编辑职位提交
    public function edit_post(Position $WorkStudentPosition,User $User){
        
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,compute_id,type_id,name,start_date,end_date,work_number,salary,salary_method,phone,description,contact,area_id,lat,lng,address','post');

        //已经有人在进行或者完成，不能删除
        if(Db::table('cmf_work_student_order_post')->where(['position_id'=>$data['id'],'delete_time'=>0,'check_status'=>3])->value('count(id)')>0) $this->error('该兼职已经有人开展或者完成！');

        $result = $this->validate($data, 'work_student_position');
        if ($result !== true) $this->error($result);

        $data['start_date'] = strtotime($data['start_date']);
        $data['end_date'] = strtotime($data['end_date']);

        $data['user_id'] = request()->uid;

        try{
            $userInfo = $User->where('id',$data['user_id'])->field('real_name as name,mobile')->find();
            if(@empty($data['phone'])) $data['phone'] = $userInfo['mobile'];
            if(@empty($data['name'])) $data['contact'] = $userInfo['name'];
        }catch(\Exception $e){}

        $data['company_id'] = request()->user_relation['company_id'];
        $data['update_time'] = time();
        $data['status'] = 0;
        $add = $WorkStudentPosition->save($data,['id'=>$data['id']]);
        $add?$this->success('编辑成功'):$this->error('编辑失败');
    }

    //企业删除职位
    public  function delete(Position $WorkStudentPosition,$id = 0){
        //已经有人在进行或者完成，不能删除
        if(Db::table('cmf_work_student_order_post')->where(['position_id'=>$id,'delete_time'=>0,'check_status'=>3])->value('count(id)')>0) $this->error('该兼职已经有人开展或者完成！');

        $delete = $WorkStudentPosition->save(['delete_time'=>time()],['id'=>$id]);
        $delete?$this->success('删除成功'):$this->error('删除失败');
    }

    //岗位列表筛选项获取
    public function search_type(){
        try{
            $data['computeList'] = Db::table('cmf_work_student_close_type')->where('status',1)->order('list_order asc,id asc')->field('name,id')->select()->toArray();
            $data['typeList'] = Db::table('cmf_work_student_type')->where('status',1)->order('list_order asc,id asc')->field('name,id')->select()->toArray();
            $data['areaList'] = Db::table('cmf_area')->where(['parent_id'=>340200,'status'=>1,'level'=>3])->field('id,fullname as name')->order('list_order asc')->select()->toArray();
        }catch (\Exception $e) {
            $this->error('获取失败');
            exit;
        }
        $data['orderList'] = $this->orderList;
        $this->success('获取成功',$data);
    }

    //全部岗位列表
    public function position_list($page = 0,$limit = 10,$keyword = '',$compute_id = 0,$type_id = 0,$area_id = 0,$order_id = 1,Position $WorkStudentPosition){
        $lat = $this->request->get('lat');
        $lng = $this->request->get('lng');
        $juli = $this->request->get('juli');
        $where = ['c.status'=>1,'c.delete_time'=>0,'p.delete_time'=>0,'p.status'=>1];
        $whereString = '';

        $Sql = $WorkStudentPosition
        ->alias('p')
        ->join('cmf_company c','c.id = p.company_id')
        ->join('cmf_work_student_close_type compute','compute.id = p.compute_id')
        ->join('cmf_work_student_type t','t.id = p.type_id')
        ->group('p.id');

        //排序方式
        if($order_id == 1){
            $Sql = $Sql->order('p.create_time desc,p.id desc');
        }elseif($order_id == 2){
            $Sql = $Sql->order('p.sign_count desc,p.id desc');
        }

        $field = 'p.id,p.company_id,p.name,p.salary,IF(p.salary_method=1,"小时","天") as salary_method,FROM_UNIXTIME(p.create_time,"%Y-%m-%d %H:%i:%s") as create_time,t.name as type,compute.name as compute,c.company as company_name,p.address,p.lat,p.lng,c.logo';

        //关键词
        if($keyword){
            $where['p.name|c.company|compute.name|t.name|c.address'] = ['like','%'.$keyword.'%'];
        }

        //结算方式
        if($compute_id){
            $where['compute.id'] = $compute_id;
        }

        //工作类型
        if($type_id){
            $where['t.id'] = $type_id;
        }

        //区域筛选
        if($area_id){
            $where['c.area_id'] = $area_id;
        }

        //距离之内
        if($lat && $lng && $juli){
            $whereString = 'ROUND(6378.138*2*ASIN(SQRT(POW(SIN(('.$lat.'*PI()/180 - p.lat*PI()/180)/2),2)+COS('.$lat.'*PI()/180)*COS(p.lat*PI()/180)*POW(SIN(('.$lng.'*PI()/180-p.lng*PI()/180)/2),2)))*1000) < '.$juli;
         }
        try{
            $data['list'] = $Sql->field($field)->where($where)->where($whereString)->limit(($page*$limit),$limit)->select();
            $data['count'] = Db::table('cmf_work_student_position')
                ->alias('p')
                ->join('cmf_company c','c.id = p.company_id')
                ->join('cmf_work_student_close_type compute','compute.id = p.compute_id')
                ->join('cmf_work_student_type t','t.id = p.type_id')->where($where)->where($whereString)->count();
        }catch (\Exception $e) {
            return $e->getMessage();
            /*$this->error('获取失败');
            exit;*/
        }
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
        exit;
    }

    //职位详情
    public function position_info(Position $Position,$id = 0){
        $user_id = request()->uid;
        try{
            $info = $Position->alias('p')
                ->join('cmf_company c','p.company_id = c.id','left')
                ->join('cmf_job_type industry','industry.id = c.type_id','left')
                ->join('cmf_work_student_close_type compute','p.compute_id = compute.id','left')
                ->join('cmf_work_student_type t','p.type_id = t.id','left')
                ->field('p.id,p.status,p.delete_time,p.work_number,p.name,p.salary,IF(p.salary_method=1,"小时","天") as salary_method,p.phone as contact_phone,p.contact as contact_name,p.description,p.address,p.lat,p.lng,c.company_number,c.company as company_name,c.logo as company_logo,compute.name as compute,p.company_id,FROM_UNIXTIME(p.create_time,"%Y-%m-%d %H:%i") as create_time,FROM_UNIXTIME(p.start_date,"%Y-%m-%d") as start_date,FROM_UNIXTIME(p.end_date,"%Y-%m-%d") as end_date,t.name as type_name,industry.name as industry_name')
                ->where('p.id',$id)->find();
            if($order_info = Db::table('cmf_work_student_order_post')->where(['position_id'=>$id,'delete_time'=>0,'user_id'=>$user_id])->find()){
                $info['is_post'] = 1;
                $info['order_id'] = $order_info['id'];
                $info['check_status'] = $order_info['check_status'];
            }else{
                $info->hidden(['contact_phone']);
                $info['is_post'] = 0;
            }
            $info['post_number'] = Db::table('cmf_work_student_order_post')->where('position_id',$id)->where('delete_time',0)->count();
            $info['company_logo'] = cmf_get_image_preview_url($info['company_logo']);
        }catch (\Exception $e){
            $this->error('系统错误');
            exit;
        }

        if($info['delete_time'] != 0) $this->error('该职位已被删除！');
        if($info['status'] != 1) $this->error('该职位尚未通过审核！');

        $this->success('获取成功',$info);
    }

    //发布的职位（学生查看）
    public function company_position(Position $Position,$company_id = 0,$page = 0,$limit = 10){

        $Sql = $Position
            ->alias('p')
            ->join('cmf_company c','c.id = p.company_id')
            ->join('cmf_work_student_close_type compute','compute.id = p.compute_id')
            ->join('cmf_work_student_type t','t.id = p.type_id')
            ->where(['p.delete_time'=>0,'p.status'=>1,'p.company_id'=>$company_id])
            ->order('create_time desc')
            ->group('p.id')
            ->limit(((int) $page * (int) $limit), $limit);

        $field = 'p.id,p.company_id,p.name,p.salary,IF(p.salary_method=1,"小时","天") as salary_method,FROM_UNIXTIME(p.create_time,"%Y-%m-%d %H:%i:%s") as create_time,t.name as type,compute.name as compute,c.company as company_name,p.address,p.lat,p.lng,c.logo';

        try{
            $data['list'] = $Sql->field($field)->select();
            $data['count'] = $Position->where(['delete_time'=>0,'company_id'=>$company_id])->count();
        }catch (\Exception $e) {
            $this->error('获取失败');
            exit;
        }
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
    }

    //发布的职位（公司内部）
    public function publish_position(Position $Position,$page = 0,$limit = 10){
        $company_id = request()->user_relation['company_id'];
        $Sql = $Position
            ->alias('p')
            ->join('cmf_work_student_close_type compute','compute.id = p.compute_id')
            ->join('cmf_work_student_type t','t.id = p.type_id')
            ->join('cmf_work_student_order_post o','o.position_id = p.id AND o.delete_time = 0','left')
            ->where(['p.delete_time'=>['eq',0],'p.company_id'=>$company_id])
            ->order('create_time desc')
            ->group('p.id')
            ->limit(((int) $page * (int) $limit), $limit);

        $field = [
            'p.id',
            'p.name',
            'p.status',
            'p.company_id',
            'p.salary',
            'IF(p.salary_method=1,"小时","天") as salary_method',
            'FROM_UNIXTIME(p.create_time,"%Y-%m-%d %H:%i:%s") as create_time',
            't.name as type',
            'compute.name as compute',
            'IfNull(sum(o.check_status = 3),0) as working_count',
            'IfNull(sum(o.check_status = 1 or o.check_status = 0),0) as checking_count'
        ];

        try{
            $data['list'] = $Sql->field($field)->select()->append(['status_name']);
            $data['count'] = $Position->where(['delete_time'=>0,'company_id'=>$company_id])->count();
        }catch (\Exception $e) {
            $this->error('获取失败');
            exit;
        }
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
    }
}