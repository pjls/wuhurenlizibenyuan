<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace api\workstu\validate;

use think\Db;
use think\Validate;

class WorkStudentPositionValidate extends Validate
{
    protected $rule = [
        'compute_id' => 'require',
        'type_id' => 'require',
        'name' => 'require',
        'start_date' => 'require',
        'end_date' => 'require',
        'work_number' => 'require',
        'salary' => 'require',
        'salary_method' => 'require',
        'phone' => 'checkPhone:thinkphp',
        'address' => 'require',
        'lat' => 'require',
        'lng' => 'require',
        'area_id' => 'require|checkArea:thinkphp',
    ];

    protected $message = [
        'compute_id.require' => '请选择结算方式',
        'type_id.require' => '请选择职业类型',
        'name.require' => '请填写职位名称',
        'start_date.require' => '请选择开始日期',
        'end_date.require' => '请选择结束时间',
        'work_number.require' => '请填写招聘人数',
        'salary.require' => '请填写薪酬',
        'salary_method.require' => '请选择薪酬方式',
        'address.require'  => '请选择地址',
        'lat.require'  => '请选择地址',
        'lng.require'  => '请选择地址',
        'area_id.require'  => '请选择地址',
        /*'area_id.checkArea'  => '请选择地址',*/
    ];

    // protected $scene = [
    // ];

    // 自定义验证规则
    protected function checkPhone($value)
    {
        if(!preg_match('/^1\d{10}$/',$value)){
            return '联系电话格式错误';
        }else{
            return true;
        }
    }

    // 自定义验证规则
    protected function checkArea($value)
    {
        if(!Db::table('cmf_area')->where(['parent_id'=>340200,'id'=>$value,'status'=>1,'level'=>3])->value('id')){
            return '请选择芜湖内的区域！';
        }else{
            return true;
        }
    }
}