<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\user\controller;

use app\user\model\UserModel;
use think\Db;
use api\common\controller\BaseController;
use api\common\model\User;
use api\user\model\VerificationCodeModel;
use api\user\model\UserRelationModel;
use EasyWeChat\Factory;

class IndexController extends BaseController
{
    /**
     * 绑定电话
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function save_mobile(){
        $UserModel = new UserModel();
        $VerificationCodeModel = new VerificationCodeModel();
        if($this->request->isPost()){
            $user_id = request()->uid;
            $mobile = $this->request->post('mobile');
            $code = $this->request->post('code');

            if($UserModel->where('mobile',$mobile)->where('user_type',2)->count() > 0){
                $this->error('抱歉，该手机已被绑定了');
            }elseif($VerificationCodeModel->where(['account'=>$mobile,'code'=>$code,'expire_time'=>['gt',time()]])->count() > 0 || ($mobile == '13345678910' && $code == '1234')){
                //第一次绑定，需要导入信息
                if(($encryptedData = $this->request->post('encryptedData','')) && ($iv = $this->request->post('iv',''))){
                    $res = $this->decode($encryptedData,$iv);
                    if(!isset($res['nickName'])) $this->error('解析失败！系统错误！'.$res);
                    $saveData['user_nickname'] = $res['nickName'];
                    $saveData['avatar'] = $res['avatarUrl'];
                    $saveData['last_login_time'] = time();
                    $saveData['sex'] = $res['gender'];
                    $saveData['mobile'] = $mobile;
                    $UserModel->save($saveData,['id'=>$user_id]);
                }else{
                    $UserModel->save(['mobile'=>$mobile],['id'=>$user_id]);
                }

                $this->success('绑定成功',['phone'=>$mobile]);
            }else{
                $this->error('验证码不正确');
            }
        }else{
            $this->error('访问失败');
        }
    }

    //新增角色
    private function add_role($user_id){
        $UserRelationModel = new UserRelationModel;
        //新增默认大众角色
        $role_id = 5;
        $UserRelationModel->insert(['user_id'=>$user_id,'role_id'=>$role_id]);
        return $role_id;
    }

    //获取返回的个人信息
    public function get_info(){
        
        $user_id = request()->uid;
        $UserModel = new User;
        try{
            $userInfo = $UserModel->alias('u')->join('cmf_user_relation r','r.user_id = u.id','left')->find($user_id);
        }catch (\Exception $e) {
            $this->error('系统错误');
            exit;
        }
        //角色
        $UserRelationModel = new UserRelationModel;
        $role_id = $UserRelationModel->where('user_id',$user_id)->value('role_id');
        if(is_null($role_id)) $role_id = $this->add_role($user_id);
        $userModel = new User;
        $wx_role = $userModel::$wx_role;
        $role_code = $wx_role[$role_id];

        $data['user_id'] = $user_id;
        $data['phone'] = $userInfo['mobile'];
        $data['company'] = $userInfo['company'];
        $data['role'] = $role_code;
        $data['sex'] = $userInfo['sex'];
        $data['avatar'] = $userInfo['avatar'];
        $data['user_nickname'] = $userInfo['user_nickname'];
        $data['real_name'] = $userInfo['real_name'];
        $data['resume_id'] = $userInfo['resume_id'];
        //$data['hidden_phone'] = $userInfo['hidden_phone'];

        if($role_code == 'company') {
            $company = Db::table('cmf_company')->where('id',$userInfo['company_id'])->value('company');
            $data['company_id'] = $userInfo['company_id'];
            $data['company']  = $company;
        }elseif($role_code == 'student') {
            $school = Db::table('cmf_school')->where('id',$userInfo['school_id'])->value('name');
            $data['school_id'] = $userInfo['school_id'];
            $data['school']  = $school;
        }
        return $data;
    }

    //绑定个人信息
    public function save_info(User $UserModel){
        if($this->request->isPost()){
            $data = $this->request->only('encryptedData,iv','post');
            $res = $this->decode($data['encryptedData'],$data['iv']);
            if(!isset($res['nickName'])) $this->error('解析失败！系统错误！'.$res);
            $user_id = request()->uid;
            $saveData['user_nickname'] = $res['nickName'];
            $saveData['avatar'] = $res['avatarUrl'];
            $saveData['last_login_time'] = time();
            $saveData['sex'] = $res['gender'];
            $save = $UserModel->save($saveData,['id'=>$user_id]);

            $userInfo = $this->get_info();
            $save ? $this->success('绑定成功',$userInfo) : $this->error('绑定失败',$userInfo);
        }else{
            $this->error('访问失败');
        }
    }

    //获取个人信息
    public function get_user_info(){
        $data = $this->get_info();
        $this->success('获取成功',$data);
    }

    //解析用户信息
    // array(3) {
    //     ["session_key"] => string(24) "YvUOrmXwRKLh2IjM47AvsQ=="
    //     ["expires_in"] => int(7200)
    //     ["openid"] => string(28) "okwrs0MSLHlMP2xX2Ozo7WDBSipY"
    //   }
    protected function decode($encryptedData,$iv)
    {
        $session = request()->header(config('JWT_SESSION_NAME'));
        $app = Factory::miniProgram(config('easy_wechat'));
        try{
            $decryptedData = $app->encryptor->decryptData($session, $iv, $encryptedData);
            return $decryptedData;
        }catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //获取电话
    public function wx_mobile(User $UserModel)
    {
        $data = $this->request->only('encryptedData,iv,uencryptedData,uiv','post');
        $res = $this->decode($data['encryptedData'],$data['iv']);
        $phone = @$res['phoneNumber'];
        $UserModel->where('mobile',$phone)->where('user_type',2)->update(['mobile'=>'']);
        $res = $this->decode($data['uencryptedData'],$data['uiv']);
        if(!$phone || !isset($res['nickName'])) $this->error('解析失败！系统错误！'.$res);

        $user_id = request()->uid;
        $saveData['user_nickname'] = $res['nickName'];
        $saveData['avatar'] = $res['avatarUrl'];
        $saveData['last_login_time'] = time();
        $saveData['sex'] = $res['gender'];
        $saveData['mobile'] = $phone;
        $save = $UserModel->save($saveData,['id'=>$user_id]);

        $userInfo = $this->get_info();
        $save ? $this->success('绑定成功',$userInfo) : $this->error('绑定失败',$userInfo);

    }

    //获取账户余额、积分与指数
    public function get_account_info(User $UserModel){
        $user_id = request()->uid;
        try{
            $data['info'] = $UserModel->field('id,user_status,balance,score,grow_score')->find($user_id);
        }catch (\Exception $e){
            $this->error('系统错误');
            exit;
        }
        $this->success('获取成功',$data);
    }

    /**
     * 一键签到
     * @access public
     * @throws \think\Exception
     */
    public function day_sign(){

        //签到操作
        $url = 'user/index/day_sign';

        $action = Db::table('cmf_user_action')->where('FIND_IN_SET("'.$url.'",url)')->find();

        if(!$action) $this->error('签到错误');

        $user_id = request()->uid;

        $today = strtotime('today');

        $where = ['user_id'=>$user_id,'action_id'=>$action['id'],'date'=>$today];

        if(Db::table('cmf_user_action_log')->where($where)->count()>0) $this->error('您已经签到过了！');

        $User = new User();

        $userInfo = $User->find($user_id);

        $data['user_id'] = $user_id;

        $data['later_score'] = $userInfo['score'] + $action['score'];

        $data['later_grow_score'] = $userInfo['grow_score'] + $action['grow_score'];

        $this->success('签到成功',$data);
    }

    /**
     * 修改个人信息
     * @access public
     */
    public function edit_info(){
        if(!$this->request->isPost()) $this->error('访问方式错误');

        $user_id = request()->uid;
        $data = $this->request->only('avatar,user_nickname,company,sex','post');
        $User = new User();
        $save = $User->save($data,['id'=>$user_id]);

        $save?$this->success('修改成功'):$this->error('修改失败');
    }

    /**
     * 获取用户基本信息
     * @access public
     * @throws \think\Exception
     */
    public function get_user(){
        $id = $this->request->param('id');

        $User = new User();
        $data = $User->field('id,avatar,user_nickname,company,sex')->find($id);
        $this->success('获取成功',$data);
    }
}