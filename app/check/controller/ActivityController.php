<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\check\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\activity\model\ActivityModel;

class ActivityController extends AdminBaseController
{

    public $checkedArray = array(0 => '未审核', 1 => '通过', 2 => '驳回');
    protected  $table_name = 'activity';

    /*活动申请列表*/
    public function index($keyword = '', ActivityModel $ActivityModel){
        $status = $this->request->param('status',-1,'intval');
        $where =  ['a.type'=>1,'a.is_admin'=>0,'a.delete_time'=>0];
        if($status != -1) $where['a.check_status'] = $status;
        if(!empty($keyword)) $where['a.title|u.real_name'] = ['like','%'.$keyword.'%'];

        try{
            $list = $ActivityModel->alias('a')
                ->join('cmf_user u','a.user_id = u.id')
                ->where($where)->order('create_time desc')->field('a.*,u.real_name')->paginate(10);
        }catch (\Exception $e){
            return $e;
        }

        $this->assign('list',$list);
        $this->assign('page', $list->render());
        $this->assign('keyword', $keyword);
        $this->assign('status', (int) $status);
        $this->assign('checkedArray',$this->checkedArray);
        return $this->fetch();
    }

    /**
     * 活動详情
     * @adminMenu(
     *     'name'   => '活動详情',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '活動详情',
     *     'param'  => ''
     * )
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function info(){
        $ActivityModel = new ActivityModel();
        $id = $this->request->param('id', 0, 'intval');
        $info = $ActivityModel->where('id',$id)->find();
        $this->assign('info',$info);
        $this->assign('checkedArray',$this->checkedArray);
        return $this->fetch();
    }

    //活动申请审核
    public function change_status(ActivityModel $ActivityModel){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,check_status,_msg','post');
        $data['update_time'] = time();

        try{
            $info = $ActivityModel::get($data['id']);
        }catch (\Exception $e){
            return $e->getMessage();
        }
        if($info['check_status'] == $data['check_status']) $this->error('相同操作');

        Db::startTrans();
        try{
            $save = Db::table('cmf_activity')->where(['id'=>$data['id']])->update($data);

            if($data['check_status'] == 1){//不同状态处理
                $message = '您的活动“'.$info['title'].'”通过了！';
            }else{
                $message = '对不起，您的活动“'.$info['title'].'”被驳回，详情：'.$data['_msg'];
            }

            send_message(
                session('ADMIN_ID'),//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您的活动申请有新进展！',//信息标题
                $message,//信息内容
                config('HR_PARK_MSG'),//redis前缀
                ''//路径
            );//消息提醒

            Db::commit();// 提交事务
        } catch (\Exception $e) {
            Db::rollback();// 回滚事务
            return $e->getMessage();
        }

        if($save){
            //wxSendMsg($message)//发送通知
            $this->success('审核成功');
        }else{
            $this->error('审核失败');
        }
        exit;
    }

}