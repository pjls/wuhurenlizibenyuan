<?php
// +----------------------------------------------------------------------
// | 文件说明：用户-幻灯片
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: wuwu <15093565100@163.com>
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Date: 2017-5-25
// +----------------------------------------------------------------------

namespace api\sharebbs\model;

use think\Model;

class ReportModel extends Model
{
    //type举报类型
    public static   $TYPE = array(
        array('id'=>1, 'name'=>'淫秽色情'),
        array('id'=>2, 'name'=>'违法消息'),
        array('id'=>3, 'name'=>'营销广告'),
        array('id'=>4, 'name'=>'攻击谩骂'),
        array('id'=>5, 'name'=>'其他')
    );

    // 设置当前模型对应的完整数据表名称
    protected $table = 'cmf_bbs_report';
}

