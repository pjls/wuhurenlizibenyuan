<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\user\controller;

use think\Db;
use api\common\controller\BaseController;
use api\common\model\User;
use api\user\model\UserRelationModel;
use api\user\model\UserReal;

class CheckController extends BaseController
{
    //实名认证进度
    public function checking_info(UserReal $UserReal,UserRelationModel $UserRelation){
        $userModel = new User;
        $wx_role = $userModel::$wx_role;

        $user_id = request()->uid;
        $role_id = $UserRelation->where('user_id',$user_id)->value('role_id');


        $where['user_id'] = $user_id;
        $where['delete_time'] = 0;
        try{
            $info = $UserReal->where($where)->order('id desc')->find();
            if($info['id_card']) $info['id_card'] = substr($info['id_card'],0,4)."****".substr($info['id_card'],-4,4);
        }catch (\Exception $e){
            $this->error('系統错误');
            exit;
        }

        if($role_id != 5 && $info['check_status'] == 1){
            $info['role_id'] = $role_id;
            $info['role'] = @$wx_role[$role_id];
            $this->success('您已经实名认证了！',$info);
        }else{
            $this->success('获取成功',$info);
        }
    }

    //学生认证提交
    public function student_post(UserReal $UserReal){
        if(!$this->request->isPost()) $this->error('访问方式错误');
        $data = $this->request->only('id_card,name,phone,card_before_image,card_after_image,school_name','post');

        $result = $this->validate($data, 'user_real.student');
        if ($result !== true) $this->error($result);

        $data['user_id'] = $user_id = request()->uid;
        $data['update_time'] = $data['create_time'] = time();
        $data['type'] = 1;

        //撤回重新提交
        if(!$data['id'] = $UserReal->where(['user_id'=>$data['user_id'],'delete_time'=>0])->value('id')) unset($data['id']);

        $add = $UserReal->insert($data,true);
        $add?$this->success('提交成功'):$this->error('提交失败');
    }

    //社会人员认证提交
    public function society_post(UserReal $UserReal){
        if(!$this->request->isPost()) $this->error('访问方式错误');
        $data = $this->request->only('id_card,name,phone,card_before_image,card_after_image','post');

        $result = $this->validate($data, 'user_real.society');
        if ($result !== true) $this->error($result);

        $data['user_id'] = $user_id = request()->uid;
        $data['update_time'] = $data['create_time'] = time();
        $data['type'] = 2;

        //撤回重新提交
        if(!$data['id'] = $UserReal->where('user_id',$data['user_id'])->value('id')) unset($data['id']);

        $add = $UserReal->insert($data,true);
        $add?$this->success('提交成功'):$this->error('提交失败');
    }

    //公司认证提交
    public function company_post(UserReal $UserReal){
        if(!$this->request->isPost()) $this->error('访问方式错误');
        $data = $this->request->only('id_card,name,phone,card_before_image,card_after_image,company_name,business_image','post');

        $result = $this->validate($data, 'user_real.company');
        if ($result !== true) $this->error($result);

        $data['user_id'] = $user_id = request()->uid;
        $data['update_time'] = $data['create_time'] = time();
        $data['type'] = 3;

        //撤回重新提交
        if(!$data['id'] = $UserReal->where('user_id',$data['user_id'])->value('id')) unset($data['id']);

        $add = $UserReal->insert($data,true);
        $add?$this->success('提交成功'):$this->error('提交失败');
    }

    //撤回實名認證
    public function cancel(UserReal $UserReal){
        $user_id = request()->uid;
        $del = $UserReal->where('user_id',$user_id)->delete();
        $del?$this->success('撤销成功！'):$this->error('撤销失败！');
    }
}