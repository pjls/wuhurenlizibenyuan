<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\check\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\job\model\PositionModel;

class JobPositionController extends AdminBaseController
{

    public $checkedArray = array(0 => '未审核', 1 => '通过', 2 => '驳回');
    protected  $table_name = 'job_position';

    /*职位提交列表*/
    public function index($keyword = '', PositionModel $PositionModel){
        $status = $this->request->param('status',-1,'intval');
        $where =  ['p.delete_time'=>0];
        if($status != -1) $where['p.status'] = $status;
        if($keyword) $where['p.name|c.company'] = ['like','%'.$keyword.'%'];

        try{
            $list = $PositionModel->alias('p')
                ->join('cmf_company c','c.id = p.company_id')
                ->field('p.id,p.name,p.salary_highest,p.salary_lowest,p.update_time,p.company_id,p.status,p.delete_time,c.company')
                ->where($where)
                ->order('p.update_time desc')
                ->paginate(10);
        }catch (\Exception $e){
            return $e;
        }

        $this->assign('list',$list);
        $this->assign('page', $list->render());
        $this->assign('keyword', $keyword);
        $this->assign('status', (int) $status);
        $this->assign('checkedArray',$this->checkedArray);
        return $this->fetch();
    }

    /**
     * 职位详情
     * @adminMenu(
     *     'name'   => '职位详情',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位详情',
     *     'param'  => ''
     * )
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function info(){
        $PositionModel = new PositionModel();
        $id = $this->request->param('id', 0, 'intval');
        $info = $PositionModel->alias('p')
            ->join('cmf_user u','p.user_id = u.id','left')
            ->join('cmf_company c','p.company_id = c.id','left')
            ->join('cmf_job_type industry','industry.id = c.type_id','left')
            ->join('cmf_job_type t','t.id = p.type_id','left')
            ->join('cmf_job_experience e','e.id = p.experience_id','left')
            ->join('cmf_job_education education','education.id = p.education_id','left')
            ->join('cmf_area a','a.id = p.area_id','left')
            ->field('p.*,u.real_name,education.name as education_name,e.name as experience_name,t.name as type_name,industry.name as industry_name,c.company,a.name as area')
            ->where('p.id',$id)->find();
        $this->assign('info',$info);
        $this->assign('checkedArray',$this->checkedArray);
        return $this->fetch();
    }

    //职位申请审核
    public function change_status(PositionModel $PositionModel){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,status,_msg','post');
        $data['update_time'] = time();

        try{
            $info = $PositionModel::get($data['id']);
        }catch (\Exception $e){
            return $e->getMessage();
        }
        if($info['status'] == $data['status']) $this->error('相同操作');

        Db::startTrans();
        try{
            $save = Db::table('cmf_job_position')->where(['id'=>$data['id']])->update($data);

            if($data['status'] == 1){//不同状态处理
                $message = '您发布的全职岗位“'.$info['name'].'”通过了！';

                //公司在招职位个数更新
                $position_count = Db::name('job_position')->where(['company_id'=>$info['company_id'],'delete_time'=>0,'status'=>1])->count();
                Db::table('cmf_company')->where('id',$info['company_id'])->update(['position_count'=>$position_count]);
            }else{
                $message = '对不起，您发布的全职岗位“'.$info['name'].'”被驳回，详情：'.$data['_msg'];
            }

            send_message(
                session('ADMIN_ID'),//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您发布的全职岗位有新进展！',//信息标题
                $message,//信息内容
                config('HR_PARK_MSG'),//redis前缀
                ''//路径
            );//消息提醒

            Db::commit();// 提交事务
        } catch (\Exception $e) {
            Db::rollback();// 回滚事务
            return $e->getMessage();
        }

        if($save){
            //wxSendMsg($message)//发送通知
            $this->success('审核成功');
        }else{
            $this->error('审核失败');
        }
        exit;
    }

}