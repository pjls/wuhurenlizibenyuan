<?php
/**
 * 公共函数
 */
function sendSms($text,$tel){
    $apikey = config("YUNPIAN.SMS_KEY");
    // 发送短信
    $text=urldecode($text);
    $data = array( 'text'=>$text , 'apikey' => $apikey , 'mobile' => $tel);
    $return = \Curl::Post('https://sms.yunpian.com/v2/sms/single_send.json',$data);
    return $return;
}

/**
 * 消息提醒被操作者
 * @param int $sender_id 发送者ID
 * @param int $to_user_id 被发送者ID
 * @param string $table_name 消息对应数据表
 * @param int $object_id 消息对应数据表里面的数据ID
 * @param string $title 消息标题
 * @param string $message 消息内容
 * @param string $PrefixComment redis消息命名
 * @param string $url 对应小程序的跳转路径
 */
function send_message($sender_id,$to_user_id,$table_name,$object_id,$title,$message='',$PrefixComment='',$url = ''){
    \think\Db::table('cmf_user_msg')->insert([
        'sender_id'=>$sender_id,
        'to_user_id'=>$to_user_id,
        'table_name'=>$table_name,
        'object_id'=>$object_id,
        'comment_id'=>0,
        'object_title'=>$title,
        '_msg'=>$message,
        'create_time'=>time(),
        'url'=>$url
    ]);
    $redis = new \Redis();
    $redis->connect('127.0.0.1',6379);
    $redis->incr($PrefixComment.$to_user_id);//被发送用户未读信息+1
}