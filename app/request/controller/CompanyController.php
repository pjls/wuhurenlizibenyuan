<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\request\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\request\model\CompanyPostModel;

class CompanyController extends AdminBaseController
{

    public $checkedArray = array(0 => '未审核', 1 => '通过', 2 => '驳回');
    protected  $table_name = 'company_post';

    /*入驻申请列表*/
    public function company_list($keyword = '', CompanyPostModel $CompanyPostModel){
        $where = ['delete_time'=>0];
        $status = $this->request->param('status',-1,'intval');
        if($status != -1) $where['status'] = $status;
        if(!empty($keyword)) $where['company|legal_man|contact_name|contact_phone'] = ['like','%'.$keyword.'%'];

        try{
            $list = $CompanyPostModel->where($where)->order('create_time desc')->paginate(10);
        }catch (\Exception $e){
            return $e;
        }

        $this->assign('list',$list);
        $this->assign('page', $list->render());
        $this->assign('keyword', $keyword);
        $this->assign('status', (int) $status);
        $this->assign('checkedArray',$this->checkedArray);
        return $this->fetch();
    }

    /**
     * 申请详情
     * @throws \think\Exception
     */
    public function info(){
        $id = $this->request->param('id');
        $CompanyPostModel = new CompanyPostModel();
        $info = $CompanyPostModel::get($id);
        $this->assign('info',$info);
        return $this->fetch();
    }

    //入驻申请审核
    public function change_status(CompanyPostModel $CompanyPostModel){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,status,_msg','post');
        $data['update_time'] = time();

        try{
            $info = $CompanyPostModel::get($data['id']);
        }catch (\Exception $e){
            return $e->getMessage();
        }
        if($info['status'] == $data['status']) $this->error('相同操作');

        Db::startTrans();
        try{
            $save = Db::table('cmf_company_post')->where(['id'=>$data['id']])->update($data);

            if($data['status'] == 1){//不同状态处理
                Db::table('cmf_user_relation')->where('user_id',$info['user_id'])->update(['role_id'=>7]);// 权限切换 “入驻公司”
                $message = '恭喜您，成功入驻芜湖人力资源！';
            }else{
                Db::table('cmf_user_relation')->where('user_id',$info['user_id'])->update(['role_id'=>3]);// 权限切换 “公司”
                $message = '对不起您的入驻申请被驳回啦，详情：'.$data['_msg'];
            }

            send_message(
                session('ADMIN_ID'),//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您的入驻申请有新进展！',//信息标题
                $message,//信息内容
                config('HR_PARK_MSG'),//redis前缀
                ''//路径
            );//消息提醒

            Db::commit();// 提交事务
        } catch (\Exception $e) {
            Db::rollback();// 回滚事务
            return $e->getMessage();
        }

        if($save){
            //wxSendMsg($message)//发送通知
            $this->success('审核成功');
        }else{
            $this->error('审核失败');
        }
        exit;
    }

}