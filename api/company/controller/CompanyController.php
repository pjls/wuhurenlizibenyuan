<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\company\controller;

use think\Db;
use think\Validate;
use api\common\controller\BaseController;
use api\company\model\Company;
use api\company\model\CompanyPost;
use api\company\model\Area;
use api\company\model\Type;


class CompanyController extends BaseController
{
    //公司详情获取
    public function info(Company $Company,$id = 0){
        $Type = new Type();
        try{
            $info = $Company
                ->field('id,user_id,logo,company,contact_name,contact_phone,description,status,company_tel,address,lat,lng,company_number,type_id,area_id')->find($id);
            $info['type_name'] = $Type->where('id',$info['type_id'])->value('name');
            $info['logo'] = cmf_get_image_preview_url($info['logo']);
        }catch(\Exception $e){
            $this->error('系统错误');
            exit;
        }
        $this->success('访问成功',$info);
    }

    //公司详情修改
    public function edit(Company $Company){
        if($this->request->isPost()){
                $user_id = request()->uid;
                $data = $this->request->only('id,address,lat,lng,company_number,description,company_tel,area_id,type_id,logo','post');
                if(empty($data['id'])) $this->error('id为空！');
                if(empty($data['logo'])) unset($data['logo']);
                $result = $this->validate($data, 'company.edit');
                if ($result !== true) $this->error($result);
                $save = $Company->where(['id'=>$data['id'],'user_id'=>$user_id])->update($data);
                $save?$this->success('修改成功'):$this->error('修改失败');
        }
        $this->error('访问失败');
    }

    //芜湖区域获取
    public function get_area(Area $Area){
        try{
            //芜湖市的下属区
            $data['list'] = $Area->where(['parent_id'=>340200,'status'=>1,'level'=>3])->field('id,fullname as name')->order('list_order asc')->select()->toArray();
        }catch (\Exception $e){
            return $e->getMessage();
        }
        $this->success('获取成功',$data);
        exit;
    }

    /**
     * 公司类型获取
     * @throws \think\exception\DbException
     * @throws \think\Exception
     */
    public function get_type(){
        $Type = new Type();
        $tree = $Type->where('Left(id,2) = 00')->where('id != 0')->field('id,name')->order('id asc')->select();
        $this->success('获取成功',['list'=>$tree]);
    }

    /**
     * 入驻公司申请
     * @throws \think\exception\DbException
     * @throws \think\Exception
     */
    public function post()
    {
        //if(!request()->user_relation['role_id'] != 3) $this->error('请您先完成公司实名认证');
        if ($this->request->isPost()) {

            $CompanyModel = new CompanyPost();
            $user_id = request()->uid;
            $info = $CompanyModel->where('user_id',$user_id)->field('id,status')->find();
            /*if(@$info['status'] === 0){
                $this->error('请勿重复申请！');
            }elseif(@$info['status'] == 1){
                $this->error('您的申请已经通过！');
            }*/

            $data = $this->request->only('company,legal_man,contact_name,contact_phone,description', 'post');
            $result = $this->validate($data, 'company_post');
            if ($result !== true) $this->error($result);

            //入驻申请内容
            $data['id'] = $info['id'];
            $data['user_id'] = request()->uid;
            $data['create_time'] = time();
            $CompanyModel->insert($data,true);
            $this->success('申请成功');

        }
        $this->error('访问失败');
    }

    //公司头像更换
    public function change_logo(Company $Company){
        $id = $this->request->post('company_id');
        $Asset = new \api\index\controller\AssetController;
        $result = $Asset->upload();
        if(isset($result['filepath'])){
            $Company->save(['logo'=>$result['filepath']],['id'=>$id]);
            $url = cmf_get_image_preview_url($result['filepath']);
            $this->success('更改成功',$url);
        }else{
            $this->error($result);
        }
    }

}