<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\article\controller;

use api\common\controller\BaseController;
use api\article\model\PortalArticleModel;
use \think\Db;

class IndexController extends BaseController
{
    //api 分类栏目
    public function article_category($id = 0){
        try{
            $category_arr = Db::table('cmf_portal_category')->where('parent_id',$id)->where(['status'=>1,'delete_time'=>0])->order('list_order asc,id desc')->select()->toArray();
        }catch (\Exception $e){
            $this->error('获取失败！'.$e->getMessage());
            exit;
        }
        $this->success('获取成功！',$category_arr);
    }

    // api 文章列表
    public function article_list(PortalArticleModel $PortalArticleModel,$page = 0,$limit = 10,$post_keywords = ''){
        $category_id = $this->request->param('id');
        $category_ids = $this->request->param('ids');
        $where = ['delete_time'=>0,'post_type'=>1,'post_status'=>1,'published_time'=>['lt',time()]];
        if(!empty($post_keywords)) $where['post_keywords|post_title'] = ['like','%'.$post_keywords.'%'];

        if(!empty($category_id)){
            $where['category_id'] = $category_id;
        }elseif(!empty($category_ids)){
            $where['category_id'] = ['in',$category_ids];
        }

        try{
            $data['count'] = $PortalArticleModel->where($where)->count();
            $data['list']  = $PortalArticleModel
                ->where($where)
                ->field('id,more,post_title,post_keywords,post_status,is_top,recommended,post_hits,post_favorites,post_like,comment_count,create_time,update_time,published_time,post_content')
                ->order('is_top desc,update_time desc')
                ->limit(($page*$limit),$limit)
                ->select()->append(['description'])->hidden(['post_content'])->toArray();
        }catch (\Exception $e) {
            $this->error('获取失败'.$e->getMessage());
            exit;
        }
        $data['now_page'] = $page;
        $data['limit'] = $limit;

        foreach($data['list'] as $k => $v){
            $data['list'][$k]['photos'] = [];
            $more = json_decode($v['more'],true);
            if(isset($more['photos'])){
                foreach($more['photos'] as $pk => $pv){
                    $data['list'][$k]['photos'][$pk]['url'] = cmf_get_image_preview_url($pv['url']);
                }
            }
            $data['list'][$k]['thumbnail'] = cmf_get_image_preview_url($more['thumbnail']);
            unset($data['list'][$k]['more']);
        }

        $this->success("获取成功", $data);
    }

    // api 文章详情
    public function article_page(PortalArticleModel $PortalArticleModel,$id = 0){
        if(empty($id)) $this->error('id不能为空');


        try{
            $PortalArticleModel->where('id',$id)->setInc('post_hits');
            $data = $PortalArticleModel
                ->where(['delete_time'=>0,'post_status'=>1,'id'=>$id])
                ->field('id,post_title,post_content,more,post_hits,post_favorites,post_like,comment_count,create_time,update_time,published_time')
                ->find()->toArray();
        }catch (\Exception $e) {
            $this->error('获取失败');
            exit;
        }
        $data['post_content'] = cmf_replace_content_file_url(htmlspecialchars_decode($data['post_content']));
        $data['post_content'] = str_replace("\n",'',$data['post_content']);
        $data['more'] = json_decode($data['more'],true);
        try{
            foreach($data['more']['photos'] as $k => $v){
                $data['more']['photos'][$k]['url'] = cmf_get_image_preview_url($v['url']);
            }
        }catch(\Exception $e){}
        if($data['more']['thumbnail']) $data['more']['thumbnail'] = cmf_get_image_preview_url($data['more']['thumbnail']);
        $this->success('获取成功',$data);
    }

    //文章点击阅读
    /*public function post_hits(PortalArticleModel $PortalArticleModel,$id = 0){
        try{
            $PortalArticleModel->where('id',$id)->setInc('post_hits');
        }catch (\Exception $e){
            $this->error('系统错误');
        }
        $this->success('自增成功');
    }*/

    
}