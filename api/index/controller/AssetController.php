<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\index\controller;

use api\common\controller\BaseController;
use cmf\lib\Upload;

class AssetController extends BaseController
{
    /**
     * webuploader 上传
     */
    public function uploader()
    {
        if ($this->request->isPost()) {
            $uploader = new Upload();
            $result = $uploader->upload();

            if ($result === false) {
                $this->error($uploader->getError());
            } else {
                $fileType = $this->request->post('filetype','image');
                $result['url'] = $fileType == 'image' ? cmf_get_image_preview_url($result['filepath']) : cmf_get_file_download_url($result['filepath']) ;
                $this->success("上传成功!", $result);
            }
        }
    }

    public function upload(){
        if ($this->request->isPost()) {
            $uploader = new Upload();
            $result = $uploader->upload();

            if ($result === false) {
                return $uploader->getError();
            } else {
                return $result;
            }
        }
        exit;
    }


    /*网络图片转为base64编码*/
    public function imgtobase64()
    {
        $url = $this->request->param('url','','trim');
        if(!$url) return error('缺少参数！！！');
        $imageInfo = getimagesize($url);
        header('Content-Type: '.$imageInfo['mime']);
        echo file_get_contents($url);
        exit;
    }
}