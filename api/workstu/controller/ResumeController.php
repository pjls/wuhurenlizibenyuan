<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\workstu\controller;

use think\Db;
use think\Validate;
use api\common\controller\BaseController;
use api\workstu\model\Resume;
use api\workstu\model\Order;
use api\common\model\User;


class ResumeController extends BaseController
{
    //获取简历详情
    public function info(Resume $Resume,$user_id = 0){
        try{
            if($Resume->where('user_id',$user_id)->count()<=0){
                $insertId = $Resume->insertGetId(['user_id'=>$user_id]);
                 Db::table('cmf_user_relation')->where(['user_id'=>$user_id])->update(['resume_id'=>$insertId]);
            }
            $resumeInfo = $Resume->alias('re')
                ->join('cmf_user_relation r','r.user_id = re.user_id','left')
                ->join('cmf_user u','re.user_id = u.id','left')
                ->join('cmf_school s','r.school_id = s.id','left')
                ->where('re.user_id',$user_id)->field('re.*,s.name as school_name,r.school_id,u.real_name,u.user_nickname,u.avatar')->find();
            $countArr = Db::table('cmf_work_student_order_comment')->where(['user_id'=>$user_id])->field('count(order_id) as order_count,sum(company_evaluation = 1) as nice_count,sum(company_evaluation = 2) as bad_count,avg(company_grade) as grade')->find();

            $resumeInfo['order_count'] = $countArr['order_count'];
            $resumeInfo['grade'] =  round($countArr['grade'],1);
            $resumeInfo['nice_count'] = ($countArr['nice_count'] + $countArr['bad_count'])==0?0:round($countArr['nice_count']/($countArr['nice_count'] + $countArr['bad_count'])*100);
            //订单量
        }catch (\Exception $e){
            $this->error('系统错误');
            exit;
        }
        unset($resumeInfo['header']);
        $this->success('访问成功',$resumeInfo);
    }

    //修改简历
    public function edit_post(Resume $Resume){
        if(!$this->request->isPost()) $this->error('访问失败');

        $data = $this->request->only('id,city,skilled,cert,school_honor,part_time,description','post');
        $user_id = request()->uid;

        try{
            $count = $Resume->where('id',$data['id'])->count();
        }catch (\Exception $e){
            $this->error('系统错误');
            exit;
        }

        if($data['id'] && $count > 0 ){
            $save = $Resume->save($data,['id'=>$data['id'],'user_id'=>$user_id]);
        }else{
            unset($data['id']);
            $data['user_id'] = $user_id;
            $save = $Resume->insert($data);
            $insertId = $Resume->getLastInsID();
            try{
                if($insertId) Db::table('cmf_user_relation')->where(['user_id'=>$user_id])->update(['resume_id'=>$insertId]);
            }catch (\Exception $e){}
        }

        $save?$this->success('修改成功'):$this->error('修改失败');
    }

}