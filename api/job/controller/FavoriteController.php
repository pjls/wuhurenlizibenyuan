<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\job\controller;

use think\Db;
use api\common\controller\BaseController;
use api\user\model\UserFavorite;


class FavoriteController extends BaseController
{
    private $CompanyTable = 'company';
    private $PositionTable = 'job_position';

    /**
     * 公司收藏
     * @throws \think\Exception
     */
    public function company_like(){
        $UserFavorite = new UserFavorite();
        $object_id = $this->request->param('id',0,'intval');
        $info = Db::table('cmf_company')->where('id',$object_id)->find();
        $user_id = request()->uid;
        $id = $UserFavorite->insertGetId([
            'user_id'=>$user_id,
            'title'=>$info['company'],
            /*'thumb'=>$info['logo'],
            'description'=>$info['description'],*/
            'table_name'=>$this->CompanyTable,
            'object_id'=>$object_id,
            'create_time'=>time(),
        ]);
        $id?$this->success('收藏成功',['id'=>$id]):$this->error('收藏失败');
    }

    /**
     * 取消公司收藏
     */
    public function company_unlike(){
        $UserFavorite = new UserFavorite();
        $object_id = $this->request->param('id', 0, 'intval');
        $user_id = request()->uid;
        $delete = $UserFavorite->where(['user_id' => $user_id , 'object_id' => $object_id , 'table_name' => $this->CompanyTable])->delete();
        $delete ? $this->success('取消成功') : $this->error('您还未收藏');
    }

    /**
     * 职位收藏
     * @throws \think\Exception
     */
    public function position_like(){
        $UserFavorite = new UserFavorite();
        $object_id = $this->request->param('id',0,'intval');
        $info = Db::table('cmf_job_position')->where('id',$object_id)->find();
        $user_id = request()->uid;
        $id = $UserFavorite->insertGetId([
            'user_id'=>$user_id,
            'title'=>$info['name'],
            /*'thumb'=>$info['logo'],
            'description'=>$info['description'],*/
            'table_name'=>$this->PositionTable,
            'object_id'=>$object_id,
            'create_time'=>time(),
        ]);
        $id?$this->success('收藏成功',['id'=>$id]):$this->error('收藏失败');
    }

    /**
     * 职位取消收藏
     */
    public function position_unlike(){
        $UserFavorite = new UserFavorite();
        $object_id = $this->request->param('id', 0, 'intval');
        $user_id = request()->uid;
        $delete = $UserFavorite->where(['user_id' => $user_id , 'object_id' => $object_id , 'table_name' => $this->PositionTable])->delete();
        $delete ? $this->success('取消成功') : $this->error('您还未收藏');
    }

    /**
     * 收藏的公司列表
     * @param int $page 页数
     * @param int $limit 列表个数
     * @throws \think\Exception
     */
    public function company_list($page = 0,$limit = 10){
        $UserFavorite = new UserFavorite();
        $user_id = request()->uid;
        $data['list'] = $UserFavorite->alias('f')
            ->join('cmf_company c','c.id = f.object_id AND f.table_name = "'.$this->CompanyTable.'"')
            ->join('cmf_job_type industry','industry.id = c.type_id','left')
            ->join('cmf_area a','c.area_id = a.id','left')
            ->where(['f.user_id'=>$user_id])
            ->field('c.id,c.type_id as industry_id,c.area_id,c.user_id,c.company_number,c.address,c.company,c.logo,a.name as area_name,industry.name as industry_name')
            ->limit($page*$limit,$limit)->select();
        $data['count'] = $UserFavorite->where('user_id',$user_id)->where('table_name',$this->CompanyTable)->count();
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
    }

    /**
     * 收藏的公司列表
     * @param int $page 页数
     * @param int $limit 列表个数
     * @throws \think\Exception
     */
    public function position_list($page = 0,$limit = 10){
        $UserFavorite = new UserFavorite();
        $user_id = request()->uid;
        $data['list'] = $UserFavorite->alias('f')
            ->join('cmf_job_position p','p.id = f.object_id AND f.table_name = "'.$this->PositionTable.'"')
            ->join('cmf_company c','c.id = p.company_id')
            ->join('cmf_area area','area.id = p.area_id','left')
            ->join('cmf_job_type t','t.id = p.type_id','left')
            ->join('cmf_job_experience e','e.id = p.experience_id','left')
            ->join('cmf_job_education education','education.id = p.education_id','left')
            ->where(['f.user_id'=>$user_id])
            ->group('p.id')
            ->field('p.*,education.name as education_name,e.name as experience_name,t.name as type_name,c.company as company_name,c.logo,area.name as area_name')
            ->limit($page*$limit,$limit)->select();
        $data['count'] = $UserFavorite->where('user_id',$user_id)->where('table_name',$this->PositionTable)->count();
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
    }


}