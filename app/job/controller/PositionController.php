<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls
// +----------------------------------------------------------------------
namespace app\job\controller;

use Exception;
use think\Db;
use cmf\controller\AdminBaseController;
use app\job\model\PositionModel;
use app\job\model\Resume;
use EasyWeChat\Factory;

class PositionController extends AdminBaseController
{

    /**
     * 获取子职业类型
     * @throws \think\Exception
     */
    public function get_type_child()
    {
        $parent_type = $this->request->param('parent_type',0,'intval');
        $res = [];

        if($parent_type != 0) $res = Db::table('cmf_job_type')->where('LEFT(LPAD(id,4,0),2) = LEFT(LPAD('.$parent_type.',4,0),2)')->field('id,name')->order('id asc')->select()->toArray();
        return json(['data'=>$res,'status'=>1,'code'=>200]);
    }

    public $recommendArray = array(
        0 => '不推荐',
        1 => '&nbsp;推 荐&nbsp;',
    );

    public $recommendClass = array(
        0 => 'danger',
        1 => 'success',
    );

    /**
     * 职位列表
     * @adminMenu(
     *     'name'   => '职位列表',
     *     'parent' => '',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => 'file',
     *     'remark' => '职位列表',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        $keyword = $this->request->param('keyword','','trim');
        $PositionModel  =  new PositionModel;
        $where['p.delete_time'] = ['egt',0];
        $where['p.status'] = 1;
        if($keyword) $where['p.name|c.company'] = ['like','%'.$keyword.'%'];
        try{
            $list = $PositionModel->alias('p')
                ->join('cmf_company c','c.id = p.company_id')
                ->join('cmf_user u','u.id = p.user_id')
                ->join('cmf_job_order_post o','o.position_id = p.id AND o.delete_time = 0','left')
                ->field('p.*,c.company,u.user_type,u.real_name,u.user_nickname,sum(o.status = 1) as nocheck_count')
                ->where($where)
                ->group('p.id')
                ->order('create_time desc')
                ->paginate(10);
        }catch (Exception $e) {
            return $e->getMessage();
        }

        $this->assign("list", $list);
        $this->assign("keyword", $keyword);
        $this->assign('page', $list->render());
        $this->assign('recommendArray',$this->recommendArray);
        $this->assign('recommendClass',$this->recommendClass);
        return $this->fetch();
    }

    //获取二维码
    public function qr_code(){
        $id = $this->request->param('id', 0, 'intval');
        if(!$id) $this->error('缺少重要参数！');
        $app = Factory::miniProgram(config('easy_wechat'));
        $img = $app->app_code->get('pages/login/index?redirect=/pages/job/detail/index&query={"id":'.$id.'}');
        header('Content-Type:image/jpeg');
        echo($img);
        exit;
    }

    //岗位海报图
    public function poster(){
        $PositionModel = new PositionModel;
        $id = $this->request->param('id', 0, 'intval');
        if(!$id) $this->error('缺少重要参数！');
        $info = $PositionModel->alias('p')
            ->join('cmf_company c','p.company_id = c.id','left')
            ->field('p.id,p.name,p.salary_lowest,p.salary_highest,p.address,c.company as company_name,c.logo as company_logo')
            ->where('p.id',$id)->find()->toArray();
        $this->assign('info', $info);
        return $this->fetch();
    }

    //企业推荐/不推荐
    public function recommend_change(){
        $data = $this->request->param();

        try{
            $info = Db::table('cmf_job_position')->find($data['id']);
        }catch (\Exception $e){
            return $e->getMessage();
        }
        if($info['is_recommend'] == $data['is_recommend']) $this->error('相同操作');

        Db::startTrans();
        try{
            $save = Db::table('cmf_job_position')->where(['id'=>$data['id']])->update($data);
            Db::commit();// 提交事务
        } catch (\Exception $e) {
            Db::rollback();// 回滚事务
            return $e->getMessage();
        }

        if($save){
            //wxSendMsg($message)//发送通知
            $this->success('更改成功');
        }else{
            $this->error('更改失败');
        }
        exit;
    }

    /**
     * 职位详情
     * @adminMenu(
     *     'name'   => '职位详情',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位详情',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function info()
    {
        $PositionModel = new PositionModel;
        $id = $this->request->param('id', 0, 'intval');

        $info = $PositionModel->alias('p')
            ->join('cmf_user u','p.user_id = u.id','left')
            ->join('cmf_company c','p.company_id = c.id','left')
            ->join('cmf_job_type industry','industry.id = c.type_id','left')
            ->join('cmf_job_type t','t.id = p.type_id','left')
            ->join('cmf_job_experience e','e.id = p.experience_id','left')
            ->join('cmf_job_education education','education.id = p.education_id','left')
            ->join('cmf_area a','a.id = p.area_id','left')
            ->field('p.*,u.real_name,education.name as education_name,e.name as experience_name,t.name as type_name,industry.name as industry_name,c.company,a.name as area')
            ->where('p.id',$id)->find();

        $this->assign('info', $info);
        return $this->fetch();
    }

    /**
     * 职位删除
     * @throws Exception
     */
    public function delete(){
        $PositionModel = new PositionModel;
        $id = $this->request->param('id',0,'intval');
        $save = $PositionModel->save(['delete_time'=>-time()],['id'=>$id]);
        $result = $PositionModel->where('id', $id)->find();
        if($save){
            Db::table('cmf_company')->where('id',$result['company_id'])->setDec('position_count');
            Db::name('recycleBin')->insert(
                [
                    'object_id'   => $result['id'],
                    'create_time' => time(),
                    'table_name'  => 'job_position',
                    'name'        => $result['name'],
                    'user_id'     => cmf_get_current_admin_id()
                ]
            );
        }
        $save?$this->success('删除成功'):$this->error('删除失败');
    }

    /**
     * 职位编辑
     * @adminMenu(
     *     'name'   => '职位编辑',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位编辑',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function edit()
    {
        $PositionModel = new PositionModel;
        $id = $this->request->param('id', 0, 'intval');

        $info = $PositionModel->alias('p')
            ->join('cmf_user u','p.user_id = u.id','left')
            ->join('cmf_company c','p.company_id = c.id','left')
            ->join('cmf_job_type industry','industry.id = c.type_id','left')
            ->join('cmf_job_type t','t.id = p.type_id','left')
            ->join('cmf_job_experience e','e.id = p.experience_id','left')
            ->join('cmf_job_education education','education.id = p.education_id','left')
            ->join('cmf_area a','a.id = p.area_id','left')
            ->field('p.*,u.user_type,u.real_name,u.user_nickname,education.name as education_name,e.name as experience_name,t.name as type_name,industry.name as industry_name,c.company,a.name as area')
            ->where('p.id',$id)->find();
        $info['parent_type'] = substr_replace($info['type_id'],'00',-2);

        $companyList = Db::table('cmf_company')->where('delete_time',0)->field('id,company')->select();
        $ParentTypeList = Db::table('cmf_job_type')->where('RIGHT(id,2) = 00')->field('id,name')->order('id asc')->select()->toArray();
        $TypeList = Db::table('cmf_job_type')->where('LEFT(LPAD(id,4,0),2) = LEFT(LPAD('.$info['type_id'].',4,0),2)')->field('id,name')->order('id asc')->select()->toArray();
        $experienceList = Db::table('cmf_job_experience')->where('status',1)->field('id,name')->select();
        $educationList = Db::table('cmf_job_education')->where('status',1)->field('id,name')->select();
        $this->assign('info', $info);
        $this->assign('companyList', $companyList);
        $this->assign('experienceList', $experienceList);
        $this->assign('educationList', $educationList);
        $this->assign('ParentTypeList', $ParentTypeList);
        $this->assign('TypeList', $TypeList);
        return $this->fetch();
    }

    //职位编辑提交
    public function edit_post(){
        $PositionModel = new PositionModel;
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,type_id,experience_id,education_id,name,work_number,contact,phone,salary_lowest,salary_highest,description,lat,lng,address,area_id,company_id,welfare','post');
        if($data['salary_highest'] != 0 && $data['salary_highest'] < $data['salary_lowest']) $this->error('最高月薪不得低于最低月薪！');

        $result = $this->validate($data, 'job_position');
        if ($result !== true) $this->error($result);

        $data['update_time'] = time();
        $data['status'] = 1;

        $save = $PositionModel->save($data,['id'=>$data['id']]);
        $save?$this->success('编辑成功'):$this->error('编辑失败');
    }

    /**
     * 职位添加
     * @adminMenu(
     *     'name'   => '职位添加',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位添加',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function add()
    {
        $companyList = Db::table('cmf_company')->where('delete_time',0)->field('id,company')->select();
        $ParentTypeList = Db::table('cmf_job_type')->where('RIGHT(id,2) = 00')->field('id,name')->order('id asc')->select()->toArray();
        $experienceList = Db::table('cmf_job_experience')->where('status',1)->field('id,name')->select();
        $educationList = Db::table('cmf_job_education')->where('status',1)->field('id,name')->select();
        $this->assign('companyList', $companyList);
        $this->assign('experienceList', $experienceList);
        $this->assign('educationList', $educationList);
        $this->assign('ParentTypeList', $ParentTypeList);
        return $this->fetch();
    }

    /**
     * 职位添加提交
     * @adminMenu(
     *     'name'   => '职位添加',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位添加',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function add_post(){
        $PositionModel = new PositionModel;
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('type_id,experience_id,education_id,name,work_number,contact,phone,salary_lowest,salary_highest,description,lat,lng,address,area_id,company_id,welfare','post');
        if($data['salary_highest'] != 0 && $data['salary_highest'] < $data['salary_lowest']) $this->error('最高月薪不得低于最低月薪！');
        $result = $this->validate($data, 'job_position');
        if ($result !== true) $this->error($result);

        $data['user_id'] = cmf_get_current_admin_id();

        $data['create_time'] = $data['update_time'] = time();
        $data['status'] = 1;

        $save = $PositionModel->insert($data);
        if($save){
            Db::table('cmf_company')->where('id',$data['company_id'])->setInc('position_count');
            $this->success('添加成功', url('index'));
        }else{
            $this->error('添加失败');
        }
    }

    /**
     * 职位申请列表
     * @adminMenu(
     *     'name'   => '职位申请列表',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位申请列表',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function order()
    {
        $PositionModel = new PositionModel;
        $status = $this->request->param('status','');
        $id = $this->request->param('id', 0, 'intval');
        $keyword = $this->request->param('keyword','','trim');

        $statusArr = [
            1 => [
                'class' => 'default',
                'text' => '未查看'
            ],
            2 => [
                'class' => 'info',
                'text' => '已查看'
            ],
            4 => [
                'class' => 'warning',
                'text' => '不合适'
            ],
            3 => [
                'class' => 'success',
                'text' => '有意向'
            ]
        ];

        $where['o.position_id'] = $id;
        if($status !== '') $where['o.status'] = intval($status);
        if($keyword) $where['u.real_name'] = ['like','%'.$keyword.'%'];

        $list = $PositionModel->alias('p')
            ->join('cmf_company c','c.id = p.company_id')
            ->join('cmf_job_order_post o','o.position_id = p.id','right')
            ->join('cmf_user u','u.id = o.user_id')
            ->where($where)
            ->field('p.company_id,p.name,o.id,o.user_id,o.position_id,o.status,o.resume_id,o.create_time,o.delete_time,c.company,u.real_name')
            ->order('create_time desc')
            ->paginate(10);

        $this->assign("id", $id);
        $this->assign("list", $list);
        $this->assign("keyword", $keyword);
        $this->assign("status",$status);
        $this->assign("statusArr", $statusArr);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    /**
     * 求职者简历信息获取
     * @throws \think\Exception
     */
    public function resume(){
        $Resume = new Resume();
        $order_id = $this->request->param('order_id',0,'intval');
        $order_info = Db::table('cmf_job_order_post')->where('id',$order_id)->find();
        $resume_id = @$order_info['resume_id'];
        $info = $Resume::get($resume_id);

        if(!$resume_id || !$order_id || !$order_info || !$info){
            $this->error('参数错误！');
        }elseif($order_info['user_id'] != $info['user_id']){//不是本人简历、直接删除求职记录
            $this->error('简历已销毁！');
        }elseif($order_info['status'] == 1){//求职状态 待查看 转变成 已查看
            Db::table('cmf_job_order_post')->where('id',$order_id)->update(['status'=>2]);
        }
        $info = $info->toArray();
        $info['order_id'] = $order_id;

        $info['area_name'] = $info['area_id'] ? Db::table('cmf_area')->where('id',$info['area_id'])->value('name'):'未选择';
        $info['experience_list'] = Db::table('cmf_job_resume_experience')->where('resume_id',$resume_id)->order('id desc')->select()->toArray();
        $info['education_list'] = Db::table('cmf_job_resume_education')->where('resume_id',$resume_id)->order('education_id desc,id desc')->select()->toArray();
        $info['project_list'] = Db::table('cmf_job_resume_project')->where('resume_id',$resume_id)->order('id desc')->select()->toArray();
        //dump($info);die;
        $educationList = Db::table('cmf_job_education')->where('status',1)->column('name','id');
        $this->assign('educationList', $educationList);
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * 求职申请审核
     * @throws Exception
     */
    public function status_change(){

        $id = $this->request->param('id',0,'intval');
        $status = $this->request->param('status',0,'intval');
        /*$data = $this->request->only('status,_msg','post');
        $status = $data['status'];*/
        $user_id = cmf_get_current_admin_id();

        if(!$info= Db::table('cmf_job_order_post')->where(['id'=>$id])->find()){
            $this->error('没有该申请！');
        }elseif($info['status'] == $status){
            $this->error('相同操作！');
        }
        if($status == 4){
            $message = '觉得您的简历不合适！';
        }elseif($status == 3){
            $message = '对您的申请有意向！';
        }else{
            $this->error('参数错误！');
            exit;
        }

        Db::startTrans();
        try{
            $comp = Db::table('cmf_company')->where('id',$info['company_id'])->value('company');
            $position = Db::table('cmf_job_position')->where('id',$info['position_id'])->value('name');
            $save = Db::table('cmf_job_order_post')->where(['id'=>$id])->update(['status'=>$status]);

            send_message(
                $user_id,//发送者ID
                $info['user_id'],//接收者ID
                'work_student_order_post',//对象表
                $info['id'],//对象ID
                '您投递的全职岗位“'.$position.'”有新进展！',//信息标题
                '@'.$comp.$message,//信息内容
                config('HR_PARK_MSG'),//redis前缀
                '/pages/part_time/info?id='.$id//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $save?$this->success('审核成功'):$this->error('审核失败');
        exit;
    }

}