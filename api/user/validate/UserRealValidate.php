<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace api\user\validate;

use think\Validate;

class UserRealValidate extends Validate
{
    protected $rule = [
        'id_card' => 'require|checkCard:thinkphp',
        'name' => 'require',
        'phone' => 'require|checkPhone:thinkphp',
        'school_name' => 'require',
        'card_before_image' => 'require',
        'card_after_image' => 'require',
        'company_name' => 'require',
        'business_image' => 'require',
    ];

    protected $message = [
        'id_card.require' => '请填写您的身份证号码',
        'name.require' => '请填写您的姓名',
        'phone.require' => '请填写您的联系方式',
        'school_name.require' => '请填写您的学校的名称',
        'card_before_image.require' => '请上传您的身份证正面照片',
        'card_after_image.require' => '请上传您的身份证反面照片',
        'company_name.require' => '请输入您的公司名称',
        'business_image.require' => '请上传您的营业执照',
    ];

    protected $scene = [
        'student'  => ['id_card','name','phone','school_name','card_before_image','card_before_image'],
        'society'  => ['id_card','name','phone','card_before_image','card_before_image'],
        'company'  => ['id_card','name','phone','company_name','card_before_image','card_before_image','business_image'],
    ];

    // 自定义验证手机
    protected function checkPhone($value,$rule,$data)
    {
        if(!preg_match('/^1\d{10}$/',$value)){
            return '联系电话格式错误';
        }else{
            return true;
        }
    }
    // 自定义验证身份证
    protected function checkCard($value,$rule,$data)
    {
        if(!preg_match('/(^\d(15)$)|((^\d{18}$))|(^\d{17}(\d|X|x)$)/',$value)){
            return '身份证格式错误';
        }else{
            return true;
        }
    }
}