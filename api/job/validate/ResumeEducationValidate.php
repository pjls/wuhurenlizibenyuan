<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace api\job\validate;

use think\Validate;

class ResumeEducationValidate extends Validate
{
    protected $rule = [
        'school_name'  => 'require',
        'subject'  => 'require',
        'education_id'  => 'require',
        'in_time'  => 'require',
        'out_time'  => 'require',
    ];

    protected $message = [
        'school_name.require'  =>  '请填写学校名称',
        'subject.require'  =>  '请填写专业名称',
        'education_id.require'  =>  '请选择您的学历',
        'in_time.require'  =>  '请选择开始时间',
        'out_time.require'  =>  '请选择结束时间',
    ];

    // protected $scene = [
    // ];

    // 自定义验证规则
    protected function checkPhone($value)
    {
        if(!preg_match('/^1\d{10}$/',$value)){
            return '联系电话格式错误';
        }else{
            return true;
        }
    }
}