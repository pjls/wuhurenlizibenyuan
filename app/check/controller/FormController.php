<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\check\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\form\model\DiyForm;

class FormController extends AdminBaseController
{

    public $checkedArray = array(0 => '未审核', 1 => '通过', 2 => '驳回');

    /**
     * 公用预约审核列表
     * @param string $name table_name
     * @throws \think\Exception
     * @return array|string
     */
    public function _empty($name = ''){
        $keyword = $this->request->param('keyword','','trim');
        $status = $this->request->param('status',-1,'intval');
        $DiyForm  = new DiyForm();
        if(!$name) $this->error('缺少form_id');
        $formInfo = $DiyForm::get($name);
        if(!$formInfo) $this->error('列表错误');
        $where['t.form_id'] = $name;
        if(!empty($keyword)) $where['u.real_name'] = ['like','%'.$keyword.'%'];

        //渲染处理
        $draw = json_decode($formInfo['draw'],true);
        $value_type = [];
        foreach($draw as $k => $v){
            if(!isset($v['name'])) continue;
            if($v['name'] == 'photos'){
                $value_type[$v['name']]['type'] = 'photos';
            }else{
                $value_type[$v['name']]['type'] = $v['el'];
            }
            $value_type[$v['name']]['title'] = $v['title'];
        }

        if($status != -1) $where['t.status'] = $status;
        $list = Db::table($formInfo['table_name'])->alias('t')
            ->join('cmf_user u','t.user_id = u.id')
            ->where($where)->field($formInfo['field'].',t.status,t.id,t.create_time,u.real_name')->order('t.create_time desc')->paginate(10);

        $this->assign('field',explode(',',$formInfo['field']));
        $this->assign('value_type',$value_type);
        $this->assign('list',$list);
        $this->assign('form_id',$name);
        $this->assign('page', $list->render());
        $this->assign('keyword', $keyword);
        $this->assign('status', (int) $status);
        $this->assign('checkedArray',$this->checkedArray);
        return $this->fetch('index');
    }

    /**
     * 公用预约删除
     * @throws \think\Exception
     */
    public function delete(){
        $id = $this->request->param('id',0,'intval');
        $form_id = $this->request->param('form_id',0,'intval');
        $DiyForm  = new DiyForm();
        if(!$form_id) $this->error('缺少form_id');
        $formInfo = $DiyForm::get($form_id);
        if(!$formInfo) $this->error('form_id错误');
        $delete = Db::table($formInfo['table_name'])->where('id',$id)->delete();
        $delete?$this->success('删除成功'):$this->error('删除失败');
    }

    /**
     * 公用预约审核状态变更
     * @throws \think\Exception
     * @return array|string
     */
    public function status_change(){
        if(!$this->request->isPost()) $this->error('访问失败');
        $DiyForm  = new DiyForm();
        $data = $this->request->only('id,status,_msg','post');
        $form_id = $this->request->param('form_id');
        if(!$form_id) $this->error('没有form_id');
        $data['update_time'] = time();
        $formInfo = $DiyForm->field('table_name,title')->find($form_id);
        $info = Db::table($formInfo['table_name'])->find($data['id']);

        if($info['status'] == $data['status']) $this->error('相同操作');

        Db::startTrans();
        try{
            Db::table($formInfo['table_name'])->where(['id'=>$data['id']])->update($data);

            //不同状态处理
            if($data['status'] == 1){
                Db::table('cmf_user_order')->where(['object_id'=>$data['id'],'table_name'=>$formInfo['table_name']])->update(['status'=>$data['status']]);

                $message = '恭喜您，成功通过了‘'.$formInfo['title'].'’申请！';
            }else{
                Db::table('cmf_user_order')->where(['object_id'=>$data['id'],'table_name'=>$formInfo['table_name']])->update(['status'=>$data['status']]);

                $message = '对不起您的‘'.$formInfo['title'].'’申请被驳回啦，详情：'.$data['_msg'];
            }

            send_message(
                session('ADMIN_ID'),    //发送者ID
                $info['user_id'],               //接收者ID
                $formInfo['table_name'],        //对象表
                $info['id'],                    //对象ID
                '您的工单有新进展！',           //信息标题
                $message,                       //信息内容
                config('HR_PARK_MSG'),  //redis前缀
                ''                              //路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $this->success('审核成功');
        exit;
    }
}