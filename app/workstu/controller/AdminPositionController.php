<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls
// +----------------------------------------------------------------------
namespace app\workstu\controller;

use think\Db;
use Exception;
use cmf\controller\AdminBaseController;
use app\workstu\model\PositionModel;
use app\job\model\Resume;

class AdminPositionController extends AdminBaseController
{

    //薪酬类别输出
    public $salaryExt = ['1'=>'小时','2'=>'天'];

    /**
     * 职位列表
     * @adminMenu(
     *     'name'   => '职位列表',
     *     'parent' => '',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => 'file',
     *     'remark' => '职位列表',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        $keyword = $this->request->param('keyword','','trim');
        $PositionModel  =  new PositionModel;
        $where['p.delete_time'] = ['egt',0];
        $where['p.status'] = 1;
        if($keyword) $where['p.name|c.company'] = ['like','%'.$keyword.'%'];
        try{
            $list = $PositionModel->alias('p')
                ->join('cmf_company c','c.id = p.company_id')
                ->join('cmf_work_student_order_post o','o.position_id = p.id AND o.delete_time = 0','left')
                ->field('p.*,c.company,sum(o.check_status = 0) as nocheck_count')
                ->where($where)
                ->order('p.create_time desc')
                ->group('p.id')
                ->paginate(10);
        }catch (Exception $e) {
            return $e;
        }

        $this->assign("list", $list);
        $this->assign("keyword", $keyword);
        $this->assign("salaryExt", $this->salaryExt);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    /**
     * 职位详情
     * @adminMenu(
     *     'name'   => '职位详情',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位详情',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function info()
    {
        $PositionModel = new PositionModel;
        $id = $this->request->param('id', 0, 'intval');
        $info = $PositionModel->alias('p')
            ->join('cmf_user u','u.id = p.user_id','left')
            ->join('cmf_work_student_close_type compute','p.compute_id = compute.id','left')
            ->join('cmf_work_student_type type','p.type_id = type.id','left')
            ->join('cmf_company c', 'c.id = p.company_id','left')
            ->join('cmf_area a','a.id = p.area_id','left')
            ->field('p.*,c.company,a.name as area,u.real_name,compute.name as compute,type.name as type')
            ->find($id);
        $this->assign('info', $info);
        $this->assign("salaryExt", $this->salaryExt);
        return $this->fetch();
    }

    /**
     * 职位删除
     * @throws Exception
     */
    public function delete(){
        $PositionModel = new PositionModel;
        $id = $this->request->param('id',0,'intval');
        $save = $PositionModel->save(['delete_time'=>-time()],['id'=>$id]);
        $result = $PositionModel->where('id', $id)->find();
        if($save) Db::name('recycleBin')->insert(
            [
                'object_id'   => $result['id'],
                'create_time' => time(),
                'table_name'  => 'work_student_position',
                'name'        => $result['name'],
                'user_id'     => cmf_get_current_admin_id()
            ]
        );
        $save?$this->success('删除成功'):$this->error('删除失败');
    }

    /**
     * 职位添加页
     * @adminMenu(
     *     'name'   => '职位添加页',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位添加页',
     *     'param'  => ''
     * )
     * @throws Exception
     */
    public function add(){

        $companyList = Db::table('cmf_company')->where('delete_time',0)->field('id,company')->select();
        $computeList = Db::table('cmf_work_student_close_type')->where('status',1)->field('id,name')->select();
        $typeList = Db::table('cmf_work_student_type')->where('status',1)->field('id,name')->select();

        $userInfo = Db::table('cmf_user')->where('id',cmf_get_current_admin_id())->find();
        $this->assign('user', $userInfo);
        $this->assign('typeList', $typeList);
        $this->assign('companyList', $companyList);
        $this->assign('computeList', $computeList);
        $this->assign("salaryExt", $this->salaryExt);
        return $this->fetch();
    }

    /**
     * 职位添加提交
     * @adminMenu(
     *     'name'   => '职位添加提交',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位添加提交',
     *     'param'  => ''
     * )
     * @throws Exception
     */
    public function add_post(){
        $PositionModel = new PositionModel;
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('company_id,compute_id,type_id,name,start_date,end_date,work_number,salary,salary_method,phone,description,contact,area_id,lat,lng,address','post');

        //已经有人在进行或者完成，不能删除
        //if(Db::table('cmf_work_student_order_post')->where(['position_id'=>$data['id'],'delete_time'=>0,'check_status'=>3])->value('count(id)')>0) $this->error('该兼职已经有人开展或者完成！');

        $result = $this->validate($data, 'work_student_position');
        if ($result !== true) $this->error($result);

        $data['user_id'] = cmf_get_current_admin_id();
        $data['start_date'] = strtotime($data['start_date']);
        $data['end_date'] = strtotime($data['end_date']);

        $data['create_time'] = $data['update_time'] = time();
        $data['status'] = 1;
        $add = $PositionModel->insertGetId($data);
        $add?$this->success('添加成功', url("index")):$this->error('添加失败');
    }

    /**
     * 职位修改页
     * @adminMenu(
     *     'name'   => '职位修改页',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位修改页',
     *     'param'  => ''
     * )
     * @throws Exception
     */
    public function edit(){
        $PositionModel = new PositionModel;
        $id = $this->request->param('id', 0, 'intval');
        $info = $PositionModel->alias('p')
            ->join('cmf_user u','u.id = p.user_id','left')
            ->field('p.*,u.real_name,u.user_nickname,u.user_type')
            ->find($id);

        $companyList = Db::table('cmf_company')->where('delete_time',0)->column('company','id');
        $computeList = Db::table('cmf_work_student_close_type')->where('status',1)->field('id,name')->select();
        $typeList = Db::table('cmf_work_student_type')->where('status',1)->field('id,name')->select();

        $this->assign('info', $info);
        $this->assign('typeList', $typeList);
        $this->assign('companyList', $companyList);
        $this->assign('computeList', $computeList);
        $this->assign("salaryExt", $this->salaryExt);
        return $this->fetch();
    }

    /**
     * 职位修改提交
     * @adminMenu(
     *     'name'   => '职位修改提交',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位修改提交',
     *     'param'  => ''
     * )
     * @throws Exception
     */
    public function edit_post(){
        $PositionModel = new PositionModel;
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,company_id,compute_id,type_id,name,start_date,end_date,work_number,salary,salary_method,phone,description,contact,area_id,lat,lng,address','post');

        //已经有人在进行或者完成，不能删除
        //if(Db::table('cmf_work_student_order_post')->where(['position_id'=>$data['id'],'delete_time'=>0,'check_status'=>3])->value('count(id)')>0) $this->error('该兼职已经有人开展或者完成！');

        $result = $this->validate($data, 'work_student_position');
        if ($result !== true) $this->error($result);

        $data['start_date'] = strtotime($data['start_date']);
        $data['end_date'] = strtotime($data['end_date']);

        $data['update_time'] = time();
        $data['status'] = 1;
        $add = $PositionModel->save($data,['id'=>$data['id']]);
        $add?$this->success('编辑成功'):$this->error('编辑失败');
    }

    /**
     * 职位申请列表
     * @adminMenu(
     *     'name'   => '职位申请列表',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位申请列表',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function order()
    {
        $PositionModel = new PositionModel;
        $id = $this->request->param('id', 0, 'intval');
        $keyword = $this->request->param('keyword','','trim');
        $check_status = $this->request->param('check_status','');

        $where['o.position_id'] = $id;
        if($keyword) $where['u.real_name'] = ['like','%'.$keyword.'%'];
        if($check_status !== '') $where['o.check_status'] = intval($check_status);

        $statusArr = [
            0 => [
                'class' => 'default',
                'text' => '未查看'
            ],
            1 => [
                'class' => 'info',
                'text' => '已查看'
            ],
            2 => [
                'class' => 'warning',
                'text' => '不合适'
            ],
            3 => [
                'class' => 'success',
                'text' => '有意向'
            ]
        ];

        $list = $PositionModel->alias('p')
            ->join('cmf_company c','c.id = p.company_id')
            ->join('cmf_work_student_order_post o','o.position_id = p.id','right')
            ->join('cmf_user u','u.id = o.user_id')
            ->where($where)
            ->field('p.company_id,p.name,o.id,o.user_id,o.position_id,o.check_status,o.order_status,o.create_time,o.delete_time,c.company,u.real_name')
            ->order('create_time desc')
            ->paginate(10);

        $this->assign("id", $id);
        $this->assign("list", $list);
        $this->assign("keyword", $keyword);
        $this->assign("statusArr", $statusArr);
        $this->assign("check_status",$check_status);
        $this->assign("salaryExt", $this->salaryExt);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    /**
     * 职位申请列表
     * @adminMenu(
     *     'name'   => '职位申请列表',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位申请列表',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function order_comment(){
        $id = $this->request->param('id', 0, 'intval');
        $info = Db::table('cmf_work_student_order_comment')->alias('c')
            ->join('cmf_work_student_order_post o','o.id = c.order_id')
            ->field('c.*,o.complete_money,o.complete_time,o.position_id')
            ->where('c.order_id',$id)->find();
        $this->assign('info', $info);
        $this->assign("salaryExt", $this->salaryExt);
        return $this->fetch();
    }

    /**
     * 求职者简历信息获取
     * @throws Exception
     */
    public function resume(){
        $Resume = new Resume();
        $order_id = $this->request->param('order_id',0,'intval');
        $order_info = Db::table('cmf_work_student_order_post')->where('id',$order_id)->find();
        $resume_id = @$order_info['resume_id'];
        $info = $Resume::get($resume_id);

        if(!$resume_id || !$order_id || !$order_info || !$info){
            $this->error('参数错误！');
        }elseif($order_info['user_id'] != $info['user_id']){//不是本人简历、直接删除求职记录
            Db::table('cmf_work_student_order_post')->where('id',$order_id)->update(['delete_time'=>time()]);
            Db::table('cmf_work_student_position')->where('id',$order_info['position_id'])->setDec('sign_count');
            $this->error('简历已销毁！');
        }elseif($order_info['check_status'] == 0){//求职状态 待查看 转变成 已查看
            Db::table('cmf_work_student_order_post')->where('id',$order_id)->update(['check_status'=>1]);
        }
        $info = $info->toArray();
        $info['order_id'] = $order_id;

        $info['area_name'] = $info['area_id'] ? Db::table('cmf_area')->where('id',$info['area_id'])->value('name'):'未选择';
        $info['experience_list'] = Db::table('cmf_job_resume_experience')->where('resume_id',$resume_id)->order('id desc')->select()->toArray();
        $info['education_list'] = Db::table('cmf_job_resume_education')->where('resume_id',$resume_id)->order('education_id desc,id desc')->select()->toArray();
        $info['project_list'] = Db::table('cmf_job_resume_project')->where('resume_id',$resume_id)->order('id desc')->select()->toArray();
        //dump($info);die;
        $educationList = Db::table('cmf_job_education')->where('status',1)->column('name','id');
        $this->assign('educationList', $educationList);
        $this->assign('order_info',$order_info);
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * 求职申请审核
     * @throws Exception
     */
    public function check_status_change(){

        $id = $this->request->param('id',0,'intval');
        $status = $this->request->param('status',0,'intval');
        /*$data = $this->request->only('status,_msg','post');
        $status = $data['status'];*/
        $user_id = cmf_get_current_admin_id();

        if(!$info= Db::table('cmf_work_student_order_post')->where(['id'=>$id])->find()){
            $this->error('没有该申请！');
        }elseif($info['check_status'] == $status){
            $this->error('相同操作！');
        }
        if($status == 2){
            $message = '觉得您的简历不合适！';
        }elseif($status == 3){
            $message = '对您的申请有意向！';
        }else{
            $this->error('参数错误！');
            exit;
        }

        Db::startTrans();
        try{
            $comp = Db::table('cmf_company')->where('id',$info['company_id'])->value('company');
            $position = Db::table('cmf_work_student_position')->where('id',$info['position_id'])->value('name');
            $save = Db::table('cmf_work_student_order_post')->where(['id'=>$id])->update(['check_status'=>$status]);

            send_message(
                $user_id,//发送者ID
                $info['user_id'],//接收者ID
                'work_student_order_post',//对象表
                $info['id'],//对象ID
                '您投递的兼职“'.$position.'”有新进展！',//信息标题
                '@'.$comp.$message,//信息内容
                config('HR_PARK_MSG'),//redis前缀
                '/pages/part_time/info?id='.$id//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $save?$this->success('审核成功'):$this->error('审核失败');
        exit;
    }

}