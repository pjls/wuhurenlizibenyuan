<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace api\workstu\validate;

use think\Validate;

class OrderValidate extends Validate
{
    protected $rule = [
        'company_grade' => 'require',
    ];

    protected $message = [
        'company_grade.require' => '请为此学生打个分',
    ];

    /*protected $scene = [
        'add'  => ['company,legal_man,contact_name,contact_phone'],
        'edit' => ['company,address,lat,lng,company_number,area_id'],
    ];*/
}