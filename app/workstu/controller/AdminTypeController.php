<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls
// +----------------------------------------------------------------------
namespace app\workstu\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\workstu\model\TypeModel;

class AdminTypeController extends AdminBaseController
{
    /**
     * 行业类型管理
     * @adminMenu(
     *     'name'   => '行业类型管理',
     *     'parent' => '',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => 'file',
     *     'remark' => '行业类型管理',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        $TypeModel  =  new TypeModel;
        try{
            $list = $TypeModel->paginate();
        }catch (\Exception $e) {
            return $e;
        }

        $this->assign("arrStatus", $TypeModel::$STATUS);
        $this->assign("list", $list);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    /**
     * 添加行业类型
     * @adminMenu(
     *     'name'   => '添加行业类型',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加行业类型',
     *     'param'  => ''
     * )
     */
    public function add()
    {
        $TypeModel  =  new TypeModel;
        $this->assign("arrStatus", $TypeModel::$STATUS);
        return $this->fetch();
    }

    /**
     * 添加行业类型提交
     * @adminMenu(
     *     'name'   => '添加行业类型提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加行业类型提交',
     *     'param'  => ''
     * )
     */
    public function addPost()
    {
        $TypeModel  =  new TypeModel;

        $arrData = $this->request->param();
        $TypeModel->isUpdate(false)->allowField(true)->save($arrData);

        $this->success('添加成功');

    }

    /**
     * 更新行业类型状态
     * @adminMenu(
     *     'name'   => '更新行业类型状态',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '更新行业类型状态',
     *     'param'  => ''
     * )
     */
    public function upStatus()
    {
        $TypeModel  =  new TypeModel;
        $intId     = $this->request->param("id");
        $intStatus = $this->request->param("status");
        $intStatus = $intStatus ? 1 : 0;
        if (empty($intId)) {
            $this->error('没有ID');
        }
        $TypeModel->isUpdate(true)->save(["status" => $intStatus], ["id" => $intId]);

        $this->success('操作成功');

    }

}