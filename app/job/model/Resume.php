<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls < 1512273960@qq.com>
// +----------------------------------------------------------------------
namespace app\job\model;

use think\Model;

class Resume extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'cmf_job_resume';

    public static   $LANG_LEVELS = array(
        1=>"初级（入门）",
        2=>"中级（日常会话）",
        3=>"中高级（商务会话）",
        4=>"高级（无障碍商务沟通）",
    );

    public static   $SEX = array(
        1=>"男",
        2=>"女",
    );

    //预编译
    public function getSkillAttr($value){
        return  explode(',',$value);
    }

    //预编译
    public function getSexAttr($value){
        $sex = $value?Resume::$SEX[$value]:'未知';
        return  $sex;
    }

    //预编译
    public function getCertificateAttr($value){
        return  explode(',',$value);
    }

    //预编译
    public function getLangAttr($value){
        return  json_decode($value,true);
    }

    //预编译
    public function getAwardAttr($value){
        return  json_decode($value,true);
    }

}