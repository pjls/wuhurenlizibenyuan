<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace api\job\validate;

use think\Validate;

class ResumeProjectValidate extends Validate
{
    protected $rule = [
        'title'  => 'require',
        'url'  => 'url',
        'begin_time'  => 'require',
        'end_time'  => 'require',
    ];

    protected $message = [
        'title.require'  =>  '请填写项目名称',
        'url.url'  =>  '链接格式错误',
        'begin_time.require'  =>  '请选择开始时间',
        'end_time.require'  =>  '请选择结束时间',
    ];

    // protected $scene = [
    // ];

    // 自定义验证规则
    protected function checkPhone($value)
    {
        if(!preg_match('/^1\d{10}$/',$value)){
            return '联系电话格式错误';
        }else{
            return true;
        }
    }
}