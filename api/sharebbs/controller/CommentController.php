<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\sharebbs\controller;

use think\Db;
use api\common\controller\BaseController;
use api\sharebbs\model\CommentModel;

class CommentController extends BaseController
{
    public  $PrefixComment = 'Comment_';
    protected  $table_name = 'bbs_comment';
    protected  $wx_url = '/pages/share';
    /**
     * 架构函数
     * @access public
     */
    public function __construct(){
        parent::__construct();
    }

    //測試新增最新消息
    public function test(){
        ignore_user_abort(true);
        set_time_limit(0);
        //被发送用户未读信息+1
        //$this->redis->incr($this->PrefixComment.'5');
        $url = 'https://apis.map.qq.com/ws/district/v1/list?key=ODWBZ-UJQ6V-OMYPM-UZTEJ-IJMQF-LFBFB';
        $res = \Curl::Get($url);
        $res = json_decode($res,true);
        if($res['status'] == 0 && !empty($res['result'][0])){
            foreach($res['result'][0] as $k => $v){
                dump($v['name']);
                $data['id'] = $v['id'];
                $data['parent_id'] = 0;
                $data['name'] = $v['name'];
                $data['fullname'] = $v['fullname'];
                $data['pinyin'] = implode('',$v['pinyin']);
                $data['level'] = 1;
                Db::table('cmf_area')->insert($data,true);

                    $url2 = 'https://apis.map.qq.com/ws/district/v1/getchildren?key=ODWBZ-UJQ6V-OMYPM-UZTEJ-IJMQF-LFBFB&id='.$v['id'];
                    sleep(2);
                    $res2 = \Curl::Get($url2);
                    $res2 = json_decode($res2,true);
                    if($res2['status'] == 0 && !empty($res2['result'][0])){
                        foreach($res2['result'][0] as $k2 => $v2){
                            //县级城市
                            if(substr($v2['id'],-2) !== '00'){
                                $data2['parent_id'] = $v['id'];
                                if(substr($v2['id'],-4,2) == '90'){
                                    $data2['id'] = substr_replace($v2['id'],'00',-2,2);
                                    $data2['name'] = '省直辖县级行政区划';
                                    $data2['fullname'] = '省直辖县级行政区划';
                                    $data2['pinyin'] = '';
                                }else{
                                    $data2['id'] = substr_replace($v2['id'],'0100',-4,4);
                                    $data2['name'] = $v['name'];
                                    $data2['fullname'] = $v['fullname'];
                                    $data2['pinyin'] = implode('',$v2['pinyin']);
                                }
                                $data2['level'] = 2;
                                Db::table('cmf_area')->insert($data2,true);
                                $data3['id'] = $v2['id'];
                                $data3['parent_id'] = $data2['id'];
                                $data3['name'] = $v2['name'];
                                $data3['fullname'] = $v2['fullname'];
                                $data3['pinyin'] = implode('',$v2['pinyin']);
                                $data3['level'] = 3;
                                Db::table('cmf_area')->insert($data3,true);
                                continue;
                            }

                            $data2['id'] = $v2['id'];
                            $data2['parent_id'] = $v['id'];
                            $data2['name'] = $v2['name'];
                            $data2['fullname'] = $v2['fullname'];
                            $data2['pinyin'] = implode('',$v2['pinyin']);
                            $data2['level'] = 2;
                            Db::table('cmf_area')->insert($data2,true);

                            $url3 = 'https://apis.map.qq.com/ws/district/v1/getchildren?key=ODWBZ-UJQ6V-OMYPM-UZTEJ-IJMQF-LFBFB&id='.$v2['id'];
                            sleep(2);
                            $res3 = \Curl::Get($url3);
                            $res3 = json_decode($res3,true);
                            if($res3['status'] == 0 && !empty($res3['result'][0])){
                                foreach($res3['result'][0] as $k3 => $v3){
                                    $data3['id'] = $v3['id'];
                                    $data3['parent_id'] = $v2['id'];
                                    $data3['name'] = $v3['fullname'];
                                    $data3['fullname'] = $v3['fullname'];
                                    //$data3['pinyin'] = implode('',$v3['pinyin']);
                                    $data3['level'] = 3;
                                    Db::table('cmf_area')->insert($data3,true);
                                }
                            }
                        }
                    }
            }
        }
    }

    //分表存储
    /*public function test(){
        ignore_user_abort(true);
        set_time_limit(0);
        //被发送用户未读信息+1
        //$this->redis->incr($this->PrefixComment.'5');
        $url = 'https://apis.map.qq.com/ws/district/v1/list?key=ODWBZ-UJQ6V-OMYPM-UZTEJ-IJMQF-LFBFB';
        $res = \Curl::Get($url);
        $res = json_decode($res,true);
        if($res['status'] == 0 && !empty($res['result'][0])){
            foreach($res['result'][0] as $k => $v){
                $data['id'] = $v['id'];
                $data['name'] = $v['name'];
                $data['fullname'] = $v['fullname'];
                $data['pinyin'] = implode('',$v['pinyin']);
                unset($data['parent_id']);
                Db::table('area_province')->insert($data,true);

                //直辖市
                if(strpos($v['fullname'],'市') !== false){
                    dump($v['fullname']);
                    $data['id'] = $v['id']+100;
                    $data['parent_id'] = $v['id'];
                    Db::table('area_city')->insert($data,true);
                    $url2 = 'https://apis.map.qq.com/ws/district/v1/getchildren?key=ODWBZ-UJQ6V-OMYPM-UZTEJ-IJMQF-LFBFB&id='.$v['id'];
                    sleep(2);
                    $res2 = \Curl::Get($url2);
                    $res2 = json_decode($res2,true);
                    if($res2['status'] == 0 && !empty($res2['result'][0])){
                        foreach($res2['result'][0] as $k2 => $v2){
                            $data2['id'] = $v2['id'];
                            $data2['parent_id'] = $v['id']+100;
                            $data2['name'] = $v2['name'];
                            $data2['fullname'] = $v2['fullname'];
                            $data2['pinyin'] = implode('',$v2['pinyin']);
                            $data2['parent_id'] = $v['id']+100;
                            Db::table('area')->insert($data2,true);
                        }
                    }
                }else{
                    dump($v['name']);
                    $url2 = 'https://apis.map.qq.com/ws/district/v1/getchildren?key=ODWBZ-UJQ6V-OMYPM-UZTEJ-IJMQF-LFBFB&id='.$v['id'];
                    sleep(2);
                    $res2 = \Curl::Get($url2);
                    $res2 = json_decode($res2,true);
                    if($res2['status'] == 0 && !empty($res2['result'][0])){
                        foreach($res2['result'][0] as $k2 => $v2){
                            $data2['id'] = $v2['id'];
                            $data2['parent_id'] = $v['id'];
                            $data2['name'] = $v2['name'];
                            $data2['fullname'] = $v2['fullname'];
                            $data2['pinyin'] = implode('',$v2['pinyin']);
                            Db::table('area_city')->insert($data2,true);

                            if(substr($v2['id'],-2) === '00'){
                                $data['id'] = $v2['id'];
                                Db::table('area_city')->insert($data2,true);
                                continue;
                            }

                            $url3 = 'https://apis.map.qq.com/ws/district/v1/getchildren?key=ODWBZ-UJQ6V-OMYPM-UZTEJ-IJMQF-LFBFB&id='.$v2['id'];
                            sleep(2);
                            $res3 = \Curl::Get($url3);
                            $res3 = json_decode($res3,true);
                            if($res3['status'] == 0 && !empty($res3['result'][0])){
                                foreach($res3['result'][0] as $k3 => $v3){
                                    $data3['id'] = $v3['id'];
                                    $data3['parent_id'] = $v['id'];
                                    $data3['name'] = $v3['name'];
                                    $data3['fullname'] = $v3['fullname'];
                                    $data3['pinyin'] = implode('',$v3['pinyin']);
                                    Db::table('area')->insert($data3,true);
                                }
                            }
                        }
                    }
                }
            }
        }
    }*/

    /**
     * 获取我的未读评论条数
     */
    public function my_new_msg_len(){
        $user_id = request()->uid;
        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);
        $count = $redis->get($this->PrefixComment.$user_id)?:0;
        $this->success('获取成功',['count'=>$count]);
    }

    /**
     * 清除未读评论条数
     */
    public function del_new_msg(){
        $user_id = request()->uid;
        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);
        $del = $redis->del($this->PrefixComment.$user_id);
        $del?$this->success('清除成功'):$this->error('清除失败');
    }

    /**
     * 获取我的评论消息
     * @throws \think\Exception
     */
    public function my_msg(){
        $user_id = request()->uid;

        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);
        //清除未读评论条数
        $redis->del($this->PrefixComment.$user_id);

        $page = $this->request->param('page', 0, 'intval');
        $limit = $this->request->param('limit', 10, 'intval');
        $data['list'] = Db::table('cmf_user_msg')->alias('m')
            ->join('cmf_bbs_comment c','c.id = m.object_id','left')
            ->join('cmf_user u','u.id = m.sender_id')
            ->field('m.id,m.sender_id,m.to_user_id,m.object_id,m.comment_id,m.object_title,m._msg,m.create_time,IF(c.delete_time = 0,0,1) as is_delete,u.id as user_id,u.user_nickname,u.avatar,m.is_read')
            ->where('m.to_user_id',$user_id)->where('m.table_name',$this->table_name)->limit($page*$limit,$limit)->select()->toArray();
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
    }

    /**
     * 评论列表
     * @throws \think\Exception
     */
    public function index(){
        $CommentModel = new CommentModel();
        $object_id = $this->request->param('id', 0, 'intval');
        $page = $this->request->param('page', 0, 'intval');
        $limit = $this->request->param('limit', 10, 'intval');
        //$user_id = request()->uid;

        $data['list'] = $CommentModel->alias('c')
            ->join('cmf_user u','u.id = c.user_id')
            ->field('c.object_id,c.id,c.user_id,c.create_time,c.content,c.to_user_id,c.to_user_name,u.user_nickname,u.avatar')
            ->limit(($page*$limit),$limit)
            ->order('create_time desc')
            ->where(['object_id'=>$object_id,'delete_time'=>0])->select()->toArray();
        $data['count'] = $CommentModel->where(['object_id'=>$object_id,'delete_time'=>0])->count();

        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
    }

    /**
     * 发表评论
     * @throws \think\Exception
     */
    public function add_post(){
        if(request()->user_relation['role_id'] == 5) $this->error('请您先完成实名认证');
        if(!$this->request->isPost()) $this->error('访问方式错误');

        $object_title = $this->request->param('object_title','','trim');
        $data = $this->request->only('object_id,content','post');
        $data['user_id'] = request()->uid;
        $to_user_id = Db::table('cmf_bbs_post')->where('id',$data['object_id'])->value('user_id');
        $data['floor'] = Db::table('cmf_bbs_comment')->where(['object_id'=>$data['object_id'],'parent_id'=>0])->count()+1;
        $data['create_time'] = time();
        //$data['nick_name'] = Db::table('cmf_user')->where('id',$data['user_id'])->value('user_nickname');

        Db::startTrans();
        try{
            Db::table('cmf_bbs_post')->where('id',$data['object_id'])->setInc('comment_count');
            $add = Db::table('cmf_bbs_comment')->insertGetId($data);

            send_message(
                $data['user_id'],//发送者ID
                $to_user_id,//接收者ID
                $this->table_name,//对象表
                $add,//对象ID
                $object_title,//信息标题
                $data['content'],//信息内容
                $this->PrefixComment,//redis前缀
                $this->wx_url.'?id='.$data['object_id']//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('系统错误');
            exit;
        }
        $this->success('评论成功',['id'=>$add]);
    }

    /**
     * 发表评论回复
     * @throws \think\Exception
     */
    public function add_reply(){
        if(request()->user_relation['role_id'] == 5) $this->error('请您先完成实名认证');
        if(!$this->request->isPost()) $this->error('访问方式错误');

        $data = $this->request->only('object_id,to_user_id,content','post');
        $object_title = $this->request->param('object_title','','trim');
        $data['user_id'] = request()->uid;
        $data['floor'] = Db::table('cmf_bbs_comment')->where(['object_id'=>$data['object_id'],'parent_id'=>0])->count()+1;
        $data['create_time'] = time();
        $data['to_user_name'] = Db::table('cmf_user')->where('id',$data['to_user_id'])->value('user_nickname');

        Db::startTrans();
        try{
            Db::table('cmf_bbs_post')->where('id',$data['object_id'])->setInc('comment_count');
            $add = Db::table('cmf_bbs_comment')->insertGetId($data);

            send_message(
                $data['user_id'],//发送者ID
                $data['to_user_id'],//接收者ID
                $this->table_name,//对象表
                $add,//对象ID
                $object_title,//信息标题
                $data['content'],//信息内容
                $this->PrefixComment,//redis前缀
                $this->wx_url.'?id='.$data['object_id']//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('系统错误');
            exit;
        }
        $this->success('评论成功',['id'=>$add]);
    }

    /**
     * 撤回评论/回复
     */
    public function delete(){
        $id = $this->request->param('id', 0, 'intval');
        $object_id = $this->request->param('object_id', 0, 'intval');

        Db::startTrans();
        try{
            Db::table('cmf_bbs_post')->where('id',$object_id)->setDec('comment_count');
            Db::table('cmf_bbs_comment')->where(['id'=>$id])->update(['delete_time'=>time()]);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('系统错误');
            exit;
        }
        $this->success('撤回成功');
    }

    /**
     * 举报评论
     * @throws \think\Exception
     */
    public function report(){
        if(request()->user_relation['role_id'] == 5) $this->error('请您先完成实名认证');
        if(!$this->request->isPost()) $this->error('访问方式错误');
        $data['user_id'] = request()->uid;
        $data['object_id'] = $this->request->param('id', 0, 'intval');
        $data['to_user_id'] = $this->request->param('to_user_id', 0, 'intval');
        $data['table_name'] = 'bbs_comment';

        $report = new \api\sharebbs\model\ReportModel();
        if($report->where($data)->count()>0) $this->error('您已经举报过了');

        $data['type'] = $this->request->param('type', 0, 'intval');
        if(!$data['object_id'] || !$data['to_user_id'] || !$data['type']) $this->error('参数错误');

        $data['create_time'] = time();
        $add = $report->insert($data);
        $add?$this->success('举报成功'):$this->success('举报失败');
    }

    /**
     * 点赞帖子
     * @throws \think\Exception
     */
    public function like(){
        if(request()->user_relation['role_id'] == 5) $this->error('请您先完成实名认证');
        $id = $this->request->param('id', 0, 'intval');
        $user_id = request()->uid;
        if(Db::table('cmf_user_like')->where(['user_id'=>$user_id,'object_id'=>$id,'table_name'=>'bbs_comment'])->count()>0) $this->error('您已经点过赞啦！');
        Db::startTrans();
        try{
            Db::table('cmf_bbs_comment')->where('id',$id)->setInc('like_count');
            $title = Db::table('cmf_bbs_comment')->where('id',$id)->value('content');
            Db::table('cmf_user_like')->insertGetId(['user_id'=>$user_id,'create_time'=>time(),'table_name'=>'bbs_comment','object_id'=>$id,'title'=>$title]);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('系统错误');
            exit;
        }
        $this->success('点赞成功');
    }

    /**
     * 取消点赞帖子
     */
    public function cancel_like(){
        $id = $this->request->param('id', 0, 'intval');
        $user_id = request()->uid;
        if(!$like_id = Db::table('cmf_user_like')->where(['user_id'=>$user_id,'object_id'=>$id,'table_name'=>'bbs_comment'])->value('id')) $this->error('您还没有点赞呢！');

        Db::startTrans();
        try{
            Db::table('cmf_bbs_comment')->where('id',$id)->setDec('like_count');
            Db::table('cmf_user_like')->delete($like_id);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('系统错误');
            exit;
        }

        $this->success('取消点赞成功');
    }

}