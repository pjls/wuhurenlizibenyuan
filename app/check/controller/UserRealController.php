<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\check\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\check\model\UserRealPostModel;

class UserRealController extends AdminBaseController
{

    public $table_name = 'user_real_post';
    public $wx_url = '/pages/real/order';
    public $checkedArray = array(0 => '未审核', 1 => '通过', 2 => '驳回');

    //学生认证审核列表
    public function student_list($keyword = '', $check_status = -1, UserRealPostModel $UserRealPostModel){

        $where = ['type'=>1];

        if(!empty($keyword)) $where['id_card|name|school_name'] = ['like','%'.$keyword.'%'];
        if($check_status != -1) $where['check_status'] = $check_status;
        try{
            $list = $UserRealPostModel->where($where)->order('check_status asc,create_time desc')->paginate(10);
        }catch (\Exception $e){
            return $e;
        }
        $this->assign('list',$list);
        $this->assign('page', $list->render());
        $this->assign('keyword', $keyword);
        $this->assign('check_status', (int) $check_status);
        $this->assign('checkedArray',$this->checkedArray);
        return $this->fetch();
    }

    //社会人员认证审核列表
    public function society_list($keyword = '', $check_status = -1, UserRealPostModel $UserRealPostModel){

        $where = ['type'=>2];

        if(!empty($keyword)) $where['id_card|name'] = ['like','%'.$keyword.'%'];
        if($check_status != -1) $where['check_status'] = $check_status;
        try{
            $list = $UserRealPostModel->where($where)->order('check_status asc,create_time desc')->paginate(10);
        }catch (\Exception $e){
            return $e;
        }
        $this->assign('list',$list);
        $this->assign('page', $list->render());
        $this->assign('keyword', $keyword);
        $this->assign('check_status', (int) $check_status);
        $this->assign('checkedArray',$this->checkedArray);
        return $this->fetch();
    }

    //公司认证审核列表
    public function company_list($keyword = '', $check_status = -1, UserRealPostModel $UserRealPostModel){

        $where = ['type'=>3];

        if(!empty($keyword)) $where['id_card|name|company_name'] = ['like','%'.$keyword.'%'];
        if($check_status != -1) $where['check_status'] = $check_status;
        try{
            $list = $UserRealPostModel->where($where)->order('check_status asc,create_time desc')->paginate(10);
        }catch (\Exception $e){
            return $e;
        }
        $this->assign('list',$list);
        $this->assign('page', $list->render());
        $this->assign('keyword', $keyword);
        $this->assign('check_status', (int) $check_status);
        $this->assign('checkedArray',$this->checkedArray);
        return $this->fetch();
    }

    //学生认证审核
    public function student_checked_change(UserRealPostModel $UserRealPostModel){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,check_status,back_message','post');
        $data['update_time'] = time();

        try{
            $info = $UserRealPostModel::get($data['id']);
        }catch (\Exception $e){
            return $e;
        }
        if($info['check_status'] == $data['check_status']) $this->error('相同操作');

        Db::startTrans();
        try{
            Db::table('cmf_user_real_post')->where(['id'=>$data['id']])->update($data);

            //不同状态处理
            if($data['check_status'] == 1){
                // 权限切换且增加公司和学校
                if($school_id = Db::table('cmf_school')->where('name',$info['school_name'])->value('id')){
                    Db::table('cmf_school')->where('id',$school_id)->setInc('number');
                }else{
                    $school_id = Db::table('cmf_school')->insertGetId(['name'=>$info['school_name']]);
                }
                Db::table('cmf_user_relation')->where('user_id',$info['user_id'])->update(['role_id'=>6,'school_id'=>$school_id]);

                //认证通过，身份证照片跟随
                $userMore = Db::table('cmf_user')->where('id',$info['user_id'])->value('more');
                $userMore = json_decode($userMore,true);
                $userMore['card_before_image'] = $info['card_before_image'];
                $userMore['card_after_image'] = $info['card_after_image'];
                $userMore = json_encode($userMore);

                Db::table('cmf_user')->where('id',$info['user_id'])->update(['real_name'=>$info['name'],'id_card'=>$info['id_card'],'more'=>$userMore]);

                $message = '恭喜您，成功通过了实名认证！';
            }else{
                if($info['check_status']==1 && $school_id = Db::table('cmf_user_relation')->where('user_id',$info['user_id'])->value('school_id')){
                    Db::table('cmf_school')->where('id',$school_id)->setDec('number');
                }
                Db::table('cmf_user_relation')->where('user_id',$info['user_id'])->update(['role_id'=>5]);

                $message = '对不起您的实名认证被驳回啦，详情：'.$data['back_message'];
            }

            send_message(
                session('ADMIN_ID'),//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您的实名认证有新进展！',//信息标题
                $message,//信息内容
                config('HR_PARK_MSG'),//redis前缀
                $this->wx_url//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $this->success('审核成功');
        exit;
    }

    //公司认证审核
    public function company_checked_change(UserRealPostModel $UserRealPostModel){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,check_status,back_message','post');
        $data['update_time'] = time();

        try{
            $info = $UserRealPostModel::get($data['id']);
        }catch (\Exception $e){
            return $e;
        }
        if($info['check_status'] == $data['check_status']) $this->error('相同操作');

        Db::startTrans();
        try{
            Db::table('cmf_user_real_post')->where(['id'=>$data['id']])->update($data);

            //不同状态处理
            if($data['check_status'] == 1){
                $userInfo = Db::table('cmf_user')->where('id',$info['user_id'])->find();
                // 权限切换且增加公司和学校
                if(!$company_id = Db::table('cmf_company')->where('user_id',$info['user_id'])->where('delete_time',0)->value('id')){
                    $company_id = Db::table('cmf_company')->insertGetId([
                        'type'=>2,
                        'user_id'=>$info['user_id'],
                        'contact_name'=>$info['name'],
                        'contact_phone'=>$userInfo['mobile'],
                        'create_time'=>time(),
                        'company'=>$info['company_name'],
                        'business_image'=>$info['business_image']
                    ]);
                }else{
                    Db::table('cmf_company')->where('id',$company_id)->update([
                        'type'=>2,
                        'create_time'=>time(),
                        'contact_name'=>$info['name'],
                        'contact_phone'=>$userInfo['mobile'],
                        'company'=>$info['company_name'],
                        'business_image'=>$info['business_image']
                    ]);
                }
                Db::table('cmf_user')->where('id',$info['user_id'])->update(['company'=>$info['company_name']]);
                Db::table('cmf_user_relation')->where('user_id',$info['user_id'])->update(['role_id'=>3,'company_id'=>$company_id]);

                //认证通过，身份证照片跟随
                $userMore = Db::table('cmf_user')->where('id',$info['user_id'])->value('more');
                $userMore = json_decode($userMore,true);
                $userMore['card_before_image'] = $info['card_before_image'];
                $userMore['card_after_image'] = $info['card_after_image'];
                $userMore = json_encode($userMore);

                Db::table('cmf_user')->where('id',$info['user_id'])->update(['real_name'=>$info['name'],'id_card'=>$info['id_card'],'more'=>$userMore]);

                $message = '恭喜您，成功通过了实名认证！';
            }else{
                Db::table('cmf_user_relation')->where('user_id',$info['user_id'])->update(['role_id'=>5]);

                $message = '对不起您的实名认证被驳回啦，详情：'.$data['back_message'];
            }

            send_message(
                session('ADMIN_ID'),//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您的实名认证有新进展！',//信息标题
                $message,//信息内容
                config('HR_PARK_MSG'),//redis前缀
                $this->wx_url//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $this->success('审核成功');
        exit;
    }

    //社会人员认证审核
    public function society_checked_change(UserRealPostModel $UserRealPostModel){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,check_status,back_message','post');
        $data['update_time'] = time();

        //排除相同操作
        try{
            $info = $UserRealPostModel::get($data['id']);
        }catch (\Exception $e){
            return $e;
        }
        if($info['check_status'] == $data['check_status']) $this->error('相同操作');

        Db::startTrans();
        try{
            Db::table('cmf_user_real_post')->where(['id'=>$data['id']])->update($data);

            //不同状态处理
            if($data['check_status'] == 1){
                Db::table('cmf_user_relation')->where('user_id',$info['user_id'])->update(['role_id'=>4]);

                //认证通过，身份证照片跟随
                $userMore = Db::table('cmf_user')->where('id',$info['user_id'])->value('more');
                $userMore = json_decode($userMore,true);
                $userMore['card_before_image'] = $info['card_before_image'];
                $userMore['card_after_image'] = $info['card_after_image'];
                $userMore = json_encode($userMore);

                Db::table('cmf_user')->where('id',$info['user_id'])->update(['real_name'=>$info['name'],'id_card'=>$info['id_card'],'more'=>$userMore]);

                $message = '恭喜您，成功通过了实名认证！';
            }else{
                Db::table('cmf_user_relation')->where('user_id',$info['user_id'])->update(['role_id'=>5]);

                $message = '对不起您的实名认证被驳回啦，详情：'.$data['back_message'];
            }

            send_message(
                session('ADMIN_ID'),//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您的实名认证有新进展！',//信息标题
                $message,//信息内容
                config('HR_PARK_MSG'),//redis前缀
                $this->wx_url//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $this->success('审核成功');
        exit;
    }

}