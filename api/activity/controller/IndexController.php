<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\activity\controller;

use think\Db;
use api\common\controller\BaseController;
use api\activity\model\ActivityModel;

class IndexController extends BaseController
{
    /**
     * 添加活动
     */
    public function add_post(){
        if(!$this->request->isPost()) $this->error('访问方式错误');

        $data = $this->request->only('title,begin_time,end_time,playing_time,cut_date,organizer,lat,lng,address,supporting,co_organizer,thumbnail,content','post');
        $result = $this->validate($data, 'activity');
        if ($result !== true) $this->error($result);

        $data['type'] = 1;
        $data['is_admin'] = 0;
        $data['user_id'] = request()->uid;
        $data['create_time'] = $data['update_time'] = time();
        $data['cut_date'] = strtotime($data['cut_date']);

        Db::startTrans();
        try{
            $addId = Db::table('cmf_activity')->insertGetId($data);
            //Db::table('cmf_activity_member')->insert(['activity_id'=>$addId,'user_id'=>$data['user_id'],'create_time'=>$data['create_time'],'update_time'=>$data['update_time'],]);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('审核错误');
            exit;
        }

        $this->success('添加成功',['id'=>$addId]);
    }

    /**
     * 活动详情
     * @throws \think\exception\DbException
     * @throws \think\Exception
     */
    public function info(){
        $id = $this->request->param('id', 0, 'intval');
        $ActivityModel = new ActivityModel();
        $info = $ActivityModel::get($id)->hidden(['create_time','is_admin','is_hot']);
        if(@$info['delete_time'] != 0 ) $this->error('对不起！该活动已被删除');
        if(@$info['status'] == 0 ) $this->error('对不起！该活动已被终止');

        $user_id = request()->uid;
        $info['is_sign'] = Db::table('cmf_activity_member')->where(['user_id'=>$user_id,'activity_id'=>$info['id'],'delete_time'=>0])->count()>0?1:0;
        $info['content'] = cmf_replace_content_file_url(htmlspecialchars_decode($info['content']));
        $info['content'] = str_replace("\n",'',$info['content']);

        $info?$this->success('获取成功',$info):$this->error('获取失败');
    }

    /**
     * 修改活动
     */
    public function edit_post(){
        if(!$this->request->isPost()) $this->error('访问方式错误');

        $data = $this->request->only('id,title,begin_time,end_time,playing_time,cut_date,organizer,lat,lng,address,supporting,co_organizer,thumbnail,content','post');
        $result = $this->validate($data, 'activity');
        if ($result !== true) $this->error($result);

        $data['update_time'] = time();
        $data['check_status'] = 0;
        $data['cut_date'] = strtotime($data['cut_date']);

        $ActivityModel = new ActivityModel();
        $add = $ActivityModel->save($data,['id'=>$data['id']]);
        $add?$this->success('修改成功'):$this->error('修改失败');
    }

    /**
     * 删除活动
     */
    public function delete(){
        $ActivityModel = new ActivityModel();
        $id = $this->request->param('id', 0, 'intval');
        $user_id = request()->uid;
        $save = $ActivityModel->where('id',$id)->where('user_id',$user_id)->update(['delete_time'=>time()]);

        if($save){
            //删除成功后，通知已报名的人活动被删了
            //msg_sign();

            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    /**
     * 我发布的活动
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function my(){
        $ActivityModel = new ActivityModel();
        $page = $this->request->param('page', 0, 'intval');
        $limit = $this->request->param('limit', 10, 'intval');
        $user_id = request()->uid;

        $where = ['user_id'=>$user_id,'delete_time'=>['elt',0]];

        $data['list']  = $ActivityModel->alias('a')
            ->where($where)
            ->order('create_time desc')
            ->limit(($page*$limit),$limit)
            ->field('*,IF(delete_time=0,"","被管理员删除了") as admin_delete')
            ->select()->hidden(['delete_time','supporting','co_organizer','content'])->toArray();
        $data['count'] = $ActivityModel->where($where)->count();
        $data['page'] = $page;
        $data['limit'] = $limit;

        $this->success('获取成功',$data);
    }

    /**
     * 活动列表
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function index(){
        $ActivityModel = new ActivityModel();
        $type = $this->request->param('type', 1, 'intval');
        $page = $this->request->param('page', 0, 'intval');
        $limit = $this->request->param('limit', 10, 'intval');

        $where = ['a.type'=>$type,'a.delete_time'=>0,'a.status'=>1];

        $data['list']  = $ActivityModel->alias('a')
            ->join('cmf_activity_member m','m.activity_id = a.id AND m.user_id = '.request()->uid,'left')
            ->where($where)
            ->where('a.is_admin = 1 OR a.check_status = 1')
            ->order('a.is_top desc,a.create_time desc')
            ->group('a.id')
            ->field('a.*,If(a.cut_date < unix_timestamp(now()),1,0) as is_cut,If(m.id,1,0) as is_sign,m.id as member_id')
            ->limit(($page*$limit),$limit)
            ->select()->hidden(['status','delete_time','supporting','co_organizer','content'])->toArray();
        $data['count'] = $ActivityModel->alias('a')->where($where)->where('a.is_admin = 1 OR a.check_status = 1')->count();
        $data['page'] = $page;
        $data['limit'] = $limit;

        $this->success('获取成功',$data);
    }

    /**
     * 热门活动列表
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function hot_index(){
        $ActivityModel = new ActivityModel();
        $page = $this->request->param('page', 0, 'intval');
        $limit = $this->request->param('limit', 10, 'intval');

        $where = ['a.type'=>1,'a.delete_time'=>0,'a.status'=>1,'a.is_hot'=>1];

        $data['list']  = $ActivityModel->alias('a')
            ->join('cmf_activity_member m','m.activity_id = a.id AND m.delete_time = 0 AND m.user_id = '.request()->uid,'left')
            ->where($where)
            ->order('a.is_top desc,a.create_time desc')
            ->group('a.id')
            ->field('a.*,If(a.cut_date < unix_timestamp(now()),1,0) as is_cut,If(m.id,1,0) as is_sign,m.id as member_id')
            ->limit(($page*$limit),$limit)
            ->select()->hidden(['status','delete_time','supporting','co_organizer','content'])->toArray();
        $data['count'] = $ActivityModel->alias('a')->where($where)->count();
        $data['page'] = $page;
        $data['limit'] = $limit;

        $this->success('获取成功',$data);
    }

}