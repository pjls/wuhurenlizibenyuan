<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls < 1512273960@qq.com>
// +----------------------------------------------------------------------
namespace api\job\model;

use think\Model;

class Order extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'cmf_job_order_post';

    public static   $STATUS = array(
        1=>"未查看",
        2=>"被查看",
        3=>"有意向",
        4=>"不合适",
    );

    //预编译
    public function getStatusAttr($value){
        return  @Order::$STATUS[$value]?:$value;
    }

    //预编译
    public function getStatusNameAttr($value,$data){
        return  @Order::$STATUS[$data['status']]?:$data['status'];
    }
}