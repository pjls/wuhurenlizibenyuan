<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\check\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\workstu\model\PositionModel;

class StudentPositionController extends AdminBaseController
{

    //薪酬类别输出
    public $salaryExt = ['1'=>'小时','2'=>'天'];
    public $checkedArray = array(0 => '未审核', 1 => '通过', 2 => '驳回');
    protected  $table_name = 'work_student_position';

    /*职位提交列表*/
    public function index($keyword = '', PositionModel $PositionModel){
        $status = $this->request->param('status',-1,'intval');
        $where =  ['p.delete_time'=>0];
        if($status != -1) $where['p.status'] = $status;
        if($keyword) $where['p.name|c.company'] = ['like','%'.$keyword.'%'];

        try{
            $list = $PositionModel->alias('p')
                ->join('cmf_company c','c.id = p.company_id')
                ->field('p.id,p.name,p.salary,p.update_time,p.company_id,p.salary_method,p.status,c.company')
                ->where($where)
                ->order('update_time desc')
                ->paginate(10);
        }catch (\Exception $e){
            return $e->getMessage();
        }

        $this->assign('list',$list);
        $this->assign('page', $list->render());
        $this->assign('keyword', $keyword);
        $this->assign('status', (int) $status);
        $this->assign("salaryExt", $this->salaryExt);
        $this->assign('checkedArray',$this->checkedArray);
        return $this->fetch();
    }

    /**
     * 职位详情
     * @adminMenu(
     *     'name'   => '职位详情',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位详情',
     *     'param'  => ''
     * )
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function info(){
        $PositionModel = new PositionModel();
        $id = $this->request->param('id', 0, 'intval');
        $info = $PositionModel->alias('p')
            ->join('cmf_user u','u.id = p.user_id','left')
            ->join('cmf_work_student_close_type compute','p.compute_id = compute.id','left')
            ->join('cmf_work_student_type type','p.type_id = type.id','left')
            ->join('cmf_company c', 'c.id = p.company_id','left')
            ->join('cmf_area a','a.id = p.area_id','left')
            ->field('p.*,c.company,a.name as area,u.real_name,compute.name as compute,type.name as type')
            ->find($id);
        $this->assign('info', $info);
        $this->assign("salaryExt", $this->salaryExt);
        $this->assign('checkedArray',$this->checkedArray);
        return $this->fetch();
    }

    //职位申请审核
    public function change_status(PositionModel $PositionModel){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,status,_msg','post');
        $data['update_time'] = time();

        try{
            $info = $PositionModel::get($data['id']);
        }catch (\Exception $e){
            return $e->getMessage();
        }
        if($info['status'] == $data['status']) $this->error('相同操作');

        Db::startTrans();
        try{
            $save = Db::name('work_student_position')->where(['id'=>$data['id']])->update($data);

            if($data['status'] == 1){//不同状态处理
                $message = '您发布的兼职“'.$info['name'].'”通过了！';
            }else{
                $message = '对不起，您发布的兼职“'.$info['name'].'”被驳回，详情：'.$data['_msg'];
            }

            send_message(
                session('ADMIN_ID'),//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您发布的兼职有新进展！',//信息标题
                $message,//信息内容
                config('HR_PARK_MSG'),//redis前缀
                ''//路径
            );//消息提醒

            Db::commit();// 提交事务
        } catch (\Exception $e) {
            Db::rollback();// 回滚事务
            return $e->getMessage();
        }

        if($save){
            //wxSendMsg($message)//发送通知
            $this->success('审核成功');
        }else{
            $this->error('审核失败');
        }
        exit;
    }

}