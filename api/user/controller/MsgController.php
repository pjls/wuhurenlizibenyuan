<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\user\controller;

use think\Db;
use api\common\controller\BaseController;
use api\user\model\UserMsg;

class MsgController extends BaseController
{
    /**
     * 未读消息条数
     */
    public function my_new_msg_len()
    {
        $user_id = request()->uid;
        $redis = new \Redis();
        $redis->connect('127.0.0.1', 6379);
        $Comment = new \api\sharebbs\controller\CommentController();
        $comment_count = $redis->get($Comment->PrefixComment . $user_id) ?: 0;
        $system_count = $redis->get(config('HR_PARK_MSG') . $user_id) ?: 0;
        $this->success('获取成功', [$Comment->PrefixComment . 'count' => $comment_count, config('HR_PARK_MSG') . 'count' => $system_count]);
    }

    public function test(){
        $redis = new \Redis();
        $redis->connect('127.0.0.1', 6379);
        $redis->set(config('HR_PARK_MSG') . '22','5');
    }

    /**
     * 清除未读评论条数
     */
    public function del_new_msg()
    {
        $prefix = $this->request->param('prefix', '', 'trim');
        $user_id = request()->uid;
        $redis = new \Redis();
        $redis->connect('127.0.0.1', 6379);
        $del = $redis->del($prefix . $user_id);
        $del ? $this->success('清除成功') : $this->error('清除失败');
    }

    /**
     * 消息列表
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function index()
    {
        $redis = new \Redis();
        $redis->connect('127.0.0.1', 6379);

        $UserMsg = new UserMsg();
        $type = $this->request->param('type', 1, 'intval');
        $page = $this->request->param('page', 0, 'intval');
        $limit = $this->request->param('limit', 10, 'intval');
        $user_id = request()->uid;

        $where = ['to_user_id' => $user_id];

        //暂时不属于评论消息的全归于系统消息
        if ($type == 1) {
            $prefix = config('HR_PARK_MSG');
            $redis->del($prefix . $user_id);

            $where['table_name'] = ['neq', 'bbs_comment'];
        } elseif ($type == 2) {
            $Comment = new \api\sharebbs\controller\CommentController();
            $prefix = $Comment->PrefixComment;
            $redis->del($prefix . $user_id);

            $where['table_name'] = 'bbs_comment';
        }

        $data['list'] = $UserMsg
            ->order('create_time desc')
            ->limit(($page * $limit), $limit)
            ->field('id,sender_id,object_id,object_title,_msg,url,create_time,is_read')
            ->where($where)->select()->toArray();
        $data['count'] = $UserMsg->where($where)->count();
        $data['page'] = $page;
        $data['limit'] = $limit;

        $this->success('获取成功', $data);
    }

    //消息已读
    public function read_msg(){
        if(!$this->request->isPost()) $this->error('访问方式错误');
        $id = $this->request->param('id', 0, 'intval');
        $user_id = request()->uid;
        $UserMsg = new UserMsg();
        $UserMsg->where('id',$id)->where('to_user_id',$user_id)->update(['is_read'=>1]);
        $this->success('变更成功');
    }

    //删除消息
    public function delete(){
        if(!$this->request->isPost()) $this->error('访问方式错误');
        $id = $this->request->param('id', 0, 'intval');
        $user_id = request()->uid;
        $UserMsg = new UserMsg();
        $UserMsg->where('id',$id)->where('to_user_id',$user_id)->delete();
        $this->success('删除成功');
    }

    //消息全部已读
    public function read_all_msg(){
        if(!$this->request->isPost()) $this->error('访问方式错误');
        $type = $this->request->param('type', 1, 'intval');
        $user_id = request()->uid;

        $where['to_user_id'] = $user_id;
        //暂时不属于评论消息的全归于系统消息
        if ($type == 1) {
            $where['table_name'] = ['neq', 'bbs_comment'];
        } elseif ($type == 2) {
            $where['table_name'] = 'bbs_comment';
        }

        $UserMsg = new UserMsg();
        $UserMsg->where($where)->update(['is_read'=>1]);
        $this->success('变更成功');
    }

    //清空全部消息
    public function delete_all_msg(){
        if(!$this->request->isPost()) $this->error('访问方式错误');
        $type = $this->request->param('type', 1, 'intval');
        $user_id = request()->uid;

        $where['to_user_id'] = $user_id;
        //暂时不属于评论消息的全归于系统消息
        if ($type == 1) {
            $where['table_name'] = ['neq', 'bbs_comment'];
        } elseif ($type == 2) {
            $where['table_name'] = 'bbs_comment';
        }

        $UserMsg = new UserMsg();
        $UserMsg->where($where)->delete();
        $this->success('清空成功');
    }
}