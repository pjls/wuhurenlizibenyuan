<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls
// +----------------------------------------------------------------------
namespace app\job\controller;

use cmf\controller\AdminBaseController;
use app\job\model\Education;

class EducationController extends AdminBaseController
{
    /**
     * 学历管理
     * @adminMenu(
     *     'name'   => '学历管理',
     *     'parent' => '',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => 'file',
     *     'remark' => '学历管理',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        $Education  =  new Education;
        try{
            $list = $Education->paginate();
        }catch (\Exception $e) {
            return $e;
        }

        $this->assign("arrStatus", $Education::$STATUS);
        $this->assign("list", $list);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    /**
     * 添加学历
     * @adminMenu(
     *     'name'   => '添加学历',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加学历',
     *     'param'  => ''
     * )
     */
    public function add()
    {
        $Education  =  new Education;
        $this->assign("arrStatus", $Education::$STATUS);
        return $this->fetch();
    }

    /**
     * 添加学历提交
     * @adminMenu(
     *     'name'   => '添加学历提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加学历提交',
     *     'param'  => ''
     * )
     */
    public function addPost()
    {
        $Education  =  new Education;

        $arrData = $this->request->param();
        $Education->isUpdate(false)->allowField(true)->save($arrData);

        $this->success('添加成功');

    }

    /**
     * 更新学历状态
     * @adminMenu(
     *     'name'   => '更新学历状态',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '更新学历状态',
     *     'param'  => ''
     * )
     */
    public function upStatus()
    {
        $Education  =  new Education;
        $intId     = $this->request->param("id");
        $intStatus = $this->request->param("status");
        $intStatus = $intStatus ? 1 : 0;
        if (empty($intId)) {
            $this->error('没有ID');
        }
        $Education->isUpdate(true)->save(["status" => $intStatus], ["id" => $intId]);

        $this->success('操作成功');

    }

}