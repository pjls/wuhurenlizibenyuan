<?php
// +----------------------------------------------------------------------
// | 文件说明：用户-幻灯片
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: wuwu <15093565100@163.com>
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Date: 2017-5-25
// +----------------------------------------------------------------------

namespace api\article\model;

use think\Model;

class PortalArticleModel extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'cmf_portal_post';

    /**
     * 关联标签表
     * @return \think\model\relation\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('PortalTagModel', 'portal_tag_post', 'tag_id', 'post_id');
    }

    //预编译
    public function getThumbnailAttr($value){
        return  cmf_get_image_preview_url($value);
    }
    //预编译
    public function getDescriptionAttr($value,$data){
        $html_string = htmlspecialchars_decode($data['post_content']);
        $content = str_replace(" ","",$html_string);
        $contents = strip_tags($content);
        $contents = str_replace("\n","",$contents);
        return  mb_strlen($contents,'utf-8') > 200 ? mb_substr($contents, 0, 200, "utf-8").'....' : mb_substr($contents, 0, 200, "utf-8");
    }
}

