<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls
// +----------------------------------------------------------------------
namespace app\job\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\job\model\Salary;

class SalaryController extends AdminBaseController
{
    /**
     * 薪酬管理
     * @adminMenu(
     *     'name'   => '薪酬管理',
     *     'parent' => '',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => 'file',
     *     'remark' => '薪酬管理',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        $Salary  =  new Salary;
        try{
            $list = $Salary->paginate();
        }catch (\Exception $e) {
            return $e;
        }

        $this->assign("arrStatus", $Salary::$STATUS);
        $this->assign("list", $list);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    /**
     * 添加薪酬
     * @adminMenu(
     *     'name'   => '添加薪酬',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加薪酬',
     *     'param'  => ''
     * )
     */
    public function add()
    {
        $Salary  =  new Salary;
        $this->assign("arrStatus", $Salary::$STATUS);
        return $this->fetch();
    }

    /**
     * 添加薪酬提交
     * @adminMenu(
     *     'name'   => '添加薪酬提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加薪酬提交',
     *     'param'  => ''
     * )
     */
    public function addPost()
    {
        $Salary  =  new Salary;

        $arrData = $this->request->param();
        $Salary->isUpdate(false)->allowField(true)->save($arrData);

        $this->success('添加成功');

    }

    /**
     * 更新薪酬状态
     * @adminMenu(
     *     'name'   => '更新薪酬状态',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '更新薪酬状态',
     *     'param'  => ''
     * )
     */
    public function upStatus()
    {
        $Salary  =  new Salary;
        $intId     = $this->request->param("id");
        $intStatus = $this->request->param("status");
        $intStatus = $intStatus ? 1 : 0;
        if (empty($intId)) {
            $this->error('没有ID');
        }
        $Salary->isUpdate(true)->save(["status" => $intStatus], ["id" => $intId]);

        $this->success('操作成功');

    }

}