<?php 
function getUserId()
{
  if ($sign = request()->header(SIGN_NAME)) {
    try{
        $decoded = \Firebase\JWT\JWT::decode($sign, config('PUBLIC_KEY'), array('RS256'));
    }catch(\UnexpectedValueException $e){
        // 捕捉异常，直接抛出报错
        json(['code'=>0,'msg'=>$e->getMessage(),'errcode'=>500])->send();
        exit();
    }
    request()->uid = $decoded->uid;
  }else {
      json(['code'=>0,'msg'=>'无权限访问','errcode'=>500])->send();
      exit();
  }
}
    function error($msg = 'failed', $more = [],$exit = true)
    {
        // 返回数组
        $return = [
            'code'=>0,
            'msg'=> $msg,
            
            
        ];
        if(!empty($more)){
            foreach( $more as $key => $value ){
                $return[$key] = $value;
            }
        }
        // 是否强制输出
        if( $exit ) {
            json($return)->send();
            exit();
        }
        return json($return);
    }
    function success($data=null,$msg='success',$more=[],$exit=true)
    {
        if (null === $data) {
            json(['status'=>1])->send();
            exit();
        }
        // 返回数组
        $return = [
            'code'=>1,
            'msg'=>$msg
        ];
        if($data){
            $return['data'] = $data;
        }
        // 更多信息
        if(!empty($more)){
            foreach( $more as $key => $value ){
                $return[$key] = $value;
            }
        }
        // 是否强制输出
        if( $exit ){
            json($return)->send();
            exit();
        }
        return json($return);
    }