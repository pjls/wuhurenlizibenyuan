<?php
/**
 * 公共函数
 */
function sendSms($text,$tel){
    $apikey = config("YUNPIAN.SMS_KEY");
    // 发送短信
    $text=urldecode($text);
    $data = array( 'text'=>$text , 'apikey' => $apikey , 'mobile' => $tel);
    $return = \Curl::Post('https://sms.yunpian.com/v2/sms/single_send.json',$data);
    return $return;
}



/* 模板接口发短信 */
function tpl_send_sms($apikey, $tpl_id, $tpl_value, $mobile){
    $url="http://sms.yunpian.com/v2/sms/tpl_single_send.json";
    $encoded_tpl_value = urlencode("$tpl_value");  //tpl_value需整体转义
    $mobile = urlencode("$mobile");
    $post_string="apikey=$apikey&tpl_id=$tpl_id&tpl_value=$encoded_tpl_value&mobile=$mobile";
    return sock_post($url, $post_string);
}

/**
 * @param  int $url 为服务的url地址
 * @param string $query 为请求串
 * @return string $data
 */
function sock_post($url,$query){

    $data = "";
    $info=parse_url($url);
    $fp=fsockopen($info["host"],80,$errno,$errstr,30);

    if(!$fp){
        return $data;
    }

    $head="POST ".$info['path']." HTTP/1.0\r\n";
    $head.="Host: ".$info['host']."\r\n";
    $head.="Referer: http://".$info['host'].$info['path']."\r\n";
    $head.="Content-type: application/x-www-form-urlencoded\r\n";
    $head.="Content-Length: ".strlen(trim($query))."\r\n";
    $head.="\r\n";
    $head.=trim($query);
    $write=fputs($fp,$head);
    $header = "";
    while ($str = trim(fgets($fp,4096))) {

        $header.=$str;
    }
    while (!feof($fp)) {
        $data .= fgets($fp,4096);
    }
    return $data;
}

function send_code($smsCode,$mobile){
    $apikey = config("YUNPIAN.SMS_KEY");
    $tpl_value = urlencode('#code#').'='.urlencode($smsCode);
    $result = tpl_send_sms($apikey,3086502,$tpl_value,$mobile);
    return $result;
}

/* 地区分表缓存 */
function getAreaList(){
    $list = cache('CityList');
    if($list) return $list;

    try{
        $list = \think\Db::table('cmf_area')->where('parent_id',0)->where('level',1)->field('id,name')->order('list_order asc,id asc')->select()->toArray();
        foreach($list as $k => $v){
            $cityList = \think\Db::table('cmf_area')->where('parent_id',$v['id'])->where('level',2)->where('RIGHT(id,4) != 9000')->field('id,name')->order('list_order asc,id asc')->select()->toArray();
            $cityList2 = \think\Db::table('cmf_area')->where('parent_id',substr_replace($v['id'],'9000',-4,4))->where('level',3)->field('id,fullname as name')->order('list_order asc,id asc')->select()->toArray();
            $list[$k]['list'] = array_merge($cityList,$cityList2);
        }
    }catch (\Exception $e){
        return $e->getMessage();
    }

    cache('AreaList',$list);
    return $list;
}


/**
 * 消息提醒被操作者
 * @param int $sender_id 发送者ID
 * @param int $to_user_id 被发送者ID
 * @param string $table_name 消息对应数据表
 * @param int $object_id 消息对应数据表里面的数据ID
 * @param string $title 消息标题
 * @param string $message 消息内容
 * @param string $PrefixComment redis消息命名
 * @param string $url 对应小程序的跳转路径
 * @param int $comment_id 对象内容下的评论ID
 */
function send_message($sender_id,$to_user_id,$table_name,$object_id,$title,$message='',$PrefixComment = '',$url = '',$comment_id = 0){
    \think\Db::table('cmf_user_msg')->insert([
        'sender_id'=>$sender_id,
        'to_user_id'=>$to_user_id,
        'table_name'=>$table_name,
        'object_id'=>$object_id,
        'comment_id'=>$comment_id,
        'object_title'=>$title,
        '_msg'=>$message,
        'create_time'=>time(),
        'url'=>$url
    ]);
    $redis = new \Redis();
    $redis->connect('127.0.0.1',6379);
    $redis->incr($PrefixComment.$to_user_id);//被发送用户未读信息+1
}