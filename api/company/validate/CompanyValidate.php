<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace api\company\validate;

use think\Validate;

class CompanyValidate extends Validate
{
    protected $rule = [
        'address' => 'require',
        'lat' => 'require',
        'lng' => 'require',
        'company_number' => 'require',
        'area_id' => 'require',
        'type_id' => 'require',
    ];

    protected $message = [
        'address.require'  => '请填写地址',
        'lat.require'  => '请选择地址',
        'lng.require'  => '请选择地址',
        'company_number.require'  => '请填写公司规模',
        'area_id.require'  => '请选择地址',
        'type_id.require'  => '请选择行业',
    ];

    /*protected $scene = [
        'edit' => ['company,address,lat,lng,company_number,area_id'],
    ];*/

    // 自定义验证规则
    /*protected function checkPhone($value,$rule,$data)
    {
        if(!preg_match('/^1\d{10}$/',$value)){
            return '联系电话格式错误';
        }else{
            return true;
        }
    }*/
}