<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\job\controller;

use think\Db;
use api\common\controller\BaseController;
use api\job\model\ResumeEducation;
use api\job\model\Resume;


class EducationController extends BaseController
{
    /**
     * 学历选项
     * @throws \think\Exception
     */
    public function education_option(){
        $data['list'] = Db::table('cmf_job_education')->field('id,name')->select()->toArray();
        $this->success('获取成功',$data);
    }

    /**
     * 教育经历添加提交2
     */
    public function add_post(){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('school_name,subject,education_id,in_time,out_time,description','post');
        $result = $this->validate($data, 'resume_education');
        if ($result !== true) $this->error($result);

        $user_id  = request()->uid;
        $Resume = new Resume();

        if(!$resume_id = $Resume->where('user_id',$user_id)->value('id')){
            $resume_id = $Resume->insertGetId(['user_id'=>$user_id,'create_time'=>time(),'update_time'=>time()]);
        }

        $data['resume_id'] = $resume_id;
        $data['user_id'] = $user_id;
        $ResumeEducation = new ResumeEducation();
        $id = $ResumeEducation->insertGetId($data);
        $education_id = $ResumeEducation->where('resume_id',$resume_id)->value('max(education_id)');
        if($id) $Resume->where('id',$resume_id)->update(['update_time'=>time(),'education_id'=>$education_id]);//简历修改时间更新
        $id?$this->success('添加成功',['id'=>$id]):$this->error('添加失败');
    }

    /**
     * 教育经历修改提交2
     */
    public function edit_post(){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,school_name,subject,education_id,in_time,out_time,description','post');
        $result = $this->validate($data, 'resume_education');
        if ($result !== true) $this->error($result);

        $ResumeEducation = new ResumeEducation();
        $user_id  = request()->uid;
        if($user_id != $ResumeEducation->where('id',$data['id'])->value('user_id')){
            $this->error('不是您的教育经历！');
        }

        $save = $ResumeEducation->save($data,['id'=>$data['id']]);

        //简历修改时间
        if($save){
            $Resume = new Resume();
            $resume_id = $ResumeEducation->where('id',$data['id'])->value('resume_id');
            $education_id = $ResumeEducation->where('resume_id',$resume_id)->value('max(education_id)');
            $Resume->where('id',$resume_id)->update(['update_time'=>time(),'education_id'=>$education_id]);
        }
        $save?$this->success('修改成功'):$this->error('修改失败');
    }

    /**
     * 教育经历详情获取
     * @throws \think\Exception
     */
    public function info(){
        $id = $this->request->param('id');
        $ResumeEducation = new ResumeEducation();
        $info = $ResumeEducation::get($id);
        $info['education_name'] = Db::table('cmf_job_education')->where('id',$info['education_id'])->value('name');
        $info?$this->success('获取成功',$info):$this->error('获取失败');
    }

    /**
     * 教育经历删除
     */
    public function delete(){
        $id = $this->request->param('id');
        $ResumeEducation = new ResumeEducation();
        $user_id  = request()->uid;
        if($user_id != $ResumeEducation->where('id',$id)->value('user_id')){
            $this->error('不是您的教育经历！');
        }

        $delete = $ResumeEducation->where('id',$id)->delete();

        //简历修改时间
        if($delete){
            $Resume = new Resume();
            $resume_id = $ResumeEducation->where('id',$id)->value('id');
            $Resume->where('id',$resume_id)->update(['update_time'=>time()]);
        }
        $delete?$this->success('删除成功'):$this->error('删除失败');
    }

}