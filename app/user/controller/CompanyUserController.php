<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Powerless < wzxaini9@gmail.com>
// +----------------------------------------------------------------------
namespace app\user\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use think\db\Query;

class CompanyUserController extends AdminBaseController
{

    /**
     * 企业用户
     * @adminMenu(
     *     'name'   => '企业用户',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '企业用户',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        try{
            $list = Db::name('user')->alias('u')
                ->join('cmf_company c','c.user_id = u.id','right')
                ->where(function (Query $query) {
                    $data = $this->request->param();
                    $query->where('u.user_type',2);
                    if (!empty($data['keyword'])) {
                        $keyword = $data['keyword'];
                        $query->where('u.real_name|u.user_nickname|u.mobile|c.company', 'like', "%$keyword%");
                    }
                })
                ->field('u.*,c.company,c.id as company_id')
                ->order("u.create_time DESC")
                ->group('u.id')
                ->paginate(10);
        }catch (\Exception $e){
            return $e;
        }
        // 获取分页显示
        $page = $list->render();
        $this->assign('list', $list);
        $this->assign('page', $page);
        return $this->fetch();
    }

    /**
     * 企业详情
     * @adminMenu(
     *     'name'   => '企业详情',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '企业详情',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function company(){
        $id = $this->request->param('id',0,'intval');
        $info = Db::table('cmf_company')->alias('c')
            ->join('cmf_area a','a.id = c.area_id','left')
            ->field('c.*,a.name as area')
            ->find($id);
        $this->assign('info',$info);
        return $this->fetch();
    }
}
