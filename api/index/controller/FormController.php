<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\index\controller;

use think\Db;
use api\common\controller\BaseController;
use api\index\model\SlideItemModel;

class FormController extends BaseController
{
    /**
     * 提交工单
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function add_post(){
        if(!$this->request->isPost()) $this->error('访问方式错误');
        $form_id = $this->request->param('form_id','','trim');
        $table_setting = Db::table('diy_form')->where('id',$form_id)->find();
        $data = $this->request->only($table_setting['field'],'post');

        $result = $this->validate($data,json_decode($table_setting['validate'],true));
        if(true !== $result){
            // 验证失败 输出错误信息
            $this->error($result);
        }

        //添加系统字段参数
        $data['form_id'] = $form_id;
        $data['user_id'] = request()->uid;
        $data['update_time'] = $data['create_time'] = time();

        Db::startTrans();
        try{
            $addId =  Db::table($table_setting['table_name'])->insertGetId($data);
            //工单提交
            Db::table('cmf_user_order')->insert(['title'=>$table_setting['title'],'user_id'=>$data['user_id'],'object_id'=>$addId,'table_name'=>$table_setting['table_name'],'apply_time'=>$data['create_time']]);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('系统错误');
            exit;
        }
    }
}
