<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\index\controller;

use think\Db;
use \Firebase\JWT\JWT;
use cmf\controller\RestBaseController;
use api\common\model\User;
use EasyWeChat\Factory;

class LoginController extends RestBaseController
{
    // 获取小程序openid和unionid
    const code2Session = 'https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code';
    
    protected $session_key;

    public function login($code='',User $UserModel){
        $openid = $this->getOpenId($code);
        if(!$openid) $this->error('获取openid失败');

            //jwt内容
        $token = [
            "iat" => $_SERVER['REQUEST_TIME'], //签发时间
            //"nbf" => 1200, //在什么时候jwt开始生效  （这里表示生成100秒后才生效）
            "exp" => $_SERVER['REQUEST_TIME'] + 86400, //token 过期时间 1天
        ];

        //返回信息
        $returnJson = ['code'=>1,'status'=>1,"msg"=>'登录成功！','phone'=>''];
        try{
            $user = $UserModel->where(['wxpopenid'=>$openid,'user_type'=>2])->field('id,user_status,mobile')->find();
        }catch (\Exception $e) {}

        if(isset($user)){
            $user_id = $user['id'];
            $returnJson['phone'] = $user['mobile'];
            $UserModel->save(['last_login_time'=>time()],['id'=>$user_id]);
            if(empty($user['mobile'])){
                $returnJson['code'] = 0;
                $returnJson['status'] = 0;
            }
            //禁用
            if($user['user_status']==2) $returnJson = ['code'=>0,'status'=>0,"msg"=>'用户被禁用，请联系管理员！','phone'=>''];
        }else{
            $UserModel->insert(['wxpopenid'=>$openid,'user_type'=>2,'create_time'=>time()]);
            $user_id = $UserModel->getLastInsID();
            Db::table('cmf_user_relation')->insert(['user_id'=>$user_id]);
            $returnJson['code'] = 0;
            $returnJson['status'] = 0;
            /*return json(['code'=>0,'status'=>0,'msg'=>'请先登录！','phone'=>''])->header([config('JWT_SESSION_NAME')=>$this->session_key]);*/
        }
        $token['uid'] = $user_id;


        $jwt = JWT::encode($token,config('PRIVATE_KEY'),"RS256"); //根据参数生成了 token
        return json($returnJson)->header([config('JWT_SIGN_NAME')=>$jwt,config('JWT_SESSION_NAME')=>$this->session_key])->send();
    }
    

	//获取openid获取unionid
    private function getOpenId($code){
        $app = Factory::miniProgram(config('easy_wechat'));
        try{
            $res = $app->auth->session($code);
        }catch (\Exception $e) {}


        //dump($res);die;
        // 提取openid
        $openid = @$res['openid'];
        $this->session_key = @$res['session_key'];

        // 没有openid的情况
        if(!$openid){
            return false;
        }
        
        //获取openid成功返回
        return $openid;
    }

    //获取头部
    public function get_header($uid = 0){
        return JWT::encode(['uid'=>$uid], config('PRIVATE_KEY'), 'RS256');
    }

    //获取二维码
    public function qr_code(){
        $url = $this->request->param('url','','urldecode');

        $app = Factory::miniProgram(config('easy_wechat'));
        $img = $app->app_code->get($url);
        header('Content-Type:image/jpeg');
        echo($img);
        exit;
    }
}
