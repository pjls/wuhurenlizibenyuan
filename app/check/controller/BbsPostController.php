<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\check\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\sharebbs\model\BbsPostModel;

class BbsPostController extends AdminBaseController
{

    public $checkedArray = array(0 => '未审核', 1 => '通过', 2 => '驳回');
    protected  $table_name = 'bbs_post';

    /*职位提交列表*/
    public function index($keyword = '', BbsPostModel $BbsPostModel){
        $status = $this->request->param('status',-1,'intval');
        $where =  ['b.delete_time'=>0];
        if($status != -1) $where['b.status'] = $status;
        if(!empty($keyword)) $where['u.user_nickname|u.real_name|b.title|t.name'] = ['like','%'.$keyword.'%'];

        try{
            $list = $BbsPostModel->alias('b')
                ->join('cmf_user u','u.id = b.user_id')
                ->join('cmf_tag t','t.id = b.tag_id')
                ->where($where)->field('b.*,t.name as tag_name,u.user_nickname,u.real_name,u.user_type')->order('b.create_time desc')->paginate(10);
        }catch (\Exception $e){
            return $e;
        }

        $this->assign('list',$list);
        $this->assign('page', $list->render());
        $this->assign('keyword', $keyword);
        $this->assign('status', (int) $status);
        $this->assign('checkedArray',$this->checkedArray);
        return $this->fetch();
    }

    /**
     * 职位详情
     * @adminMenu(
     *     'name'   => '职位详情',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '职位详情',
     *     'param'  => ''
     * )
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function info(){
        $BbsPostModel = new BbsPostModel();
        $id = $this->request->param('id', 0, 'intval');
        $info = $BbsPostModel->alias('b')
            ->join('cmf_user u','u.id = b.user_id')
            ->join('cmf_tag t','t.id = b.tag_id')
            ->field('b.*,t.name as tag_name,u.user_nickname,u.real_name')->where('b.id',$id)->find();
        $this->assign('info',$info);
        $this->assign('checkedArray',$this->checkedArray);
        return $this->fetch();
    }

    //职位申请审核
    public function change_status(BbsPostModel $BbsPostModel){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,status,_msg','post');

        try{
            $info = $BbsPostModel::get($data['id']);
        }catch (\Exception $e){
            return $e->getMessage();
        }
        if($info['status'] == $data['status']) $this->error('相同操作');

        Db::startTrans();
        try{
            $save = Db::table('cmf_bbs_post')->where(['id'=>$data['id']])->update($data);

            if($data['status'] == 1){//不同状态处理
                $message = '您发布的帖子“'.$info['title'].'”通过了！';
            }else{
                $message = '对不起，您发布的帖子“'.$info['title'].'”被驳回，详情：'.$data['_msg'];
            }

            send_message(
                session('ADMIN_ID'),//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您的帖子发布有新进展！',//信息标题
                $message,//信息内容
                config('HR_PARK_MSG'),//redis前缀
                ''//路径
            );//消息提醒

            Db::commit();// 提交事务
        } catch (\Exception $e) {
            Db::rollback();// 回滚事务
            return $e->getMessage();
        }

        if($save){
            //wxSendMsg($message)//发送通知
            $this->success('审核成功');
        }else{
            $this->error('审核失败');
        }
        exit;
    }

}