<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\sharebbs\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\sharebbs\model\BbsPostModel;
use app\sharebbs\model\BbsCommentModel;

class InvitationController extends AdminBaseController
{

    /**
     * 论坛列表
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function index(){
        $BbsPostModel = new BbsPostModel();
        $keyword = $this->request->param('keyword','','trim');
        $where = ['b.delete_time'=>['egt',0],'b.status'=>1];
        if(!empty($keyword)) $where['u.user_nickname|u.real_name|b.title|t.name'] = ['like','%'.$keyword.'%'];
        $list = $BbsPostModel->alias('b')
            ->join('cmf_user u','u.id = b.user_id')
            ->join('cmf_tag t','t.id = b.tag_id')
            ->where($where)->field('b.*,t.name as tag_name,u.user_nickname,u.real_name,u.user_type')->order('b.create_time desc')->paginate(10);
        $this->assign('list',$list);
        $this->assign('page', $list->render());
        $this->assign('keyword', $keyword);
        return $this->fetch();
    }

    /**
     * 添加论坛帖子页面
     * @adminMenu(
     *     'name'   => '添加论坛帖子页面',
     *     'parent' => 'activity/Park/index',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加论坛帖子页面',
     *     'param'  => ''
     * )
     */
    public function add(){
        $tagList = Db::table('cmf_tag')->where(['type'=>1,'status'=>1])->column('name','id');
        $this->assign('tagList',$tagList);
        return $this->fetch();
    }
    /**
     * 添加论坛帖子提交
     * @adminMenu(
     *     'name'   => '添加论坛帖子提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加论坛帖子提交',
     *     'param'  => ''
     * )
     */
    public function add_post(){
        $BbsPostModel = new BbsPostModel();
        if (!$this->request->isPost()) $this->error('访问失败');

        $data = $this->request->post();
        if(!empty($data['photos'])) $data['photos'] = implode(',',$data['photos']);
        $result = $this->validate($data, 'bbs');
        if ($result !== true) $this->error($result);

        $data['user_id'] = cmf_get_current_admin_id();
        $data['user_nickname'] = Db::table('cmf_user')->where('id',$data['user_id'])->value('user_nickname');
        $data['create_time'] = time();

        $add = $BbsPostModel->insertGetId($data);

        $add?$this->success('添加成功',url('index')):$this->error('添加失败');
    }

    /**
     * 论坛帖子編輯
     * @adminMenu(
     *     'name'   => '论坛帖子編輯',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '论坛帖子編輯',
     *     'param'  => ''
     * )
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function edit(){
        $BbsPostModel = new BbsPostModel();
        $id = $this->request->param('id', 0, 'intval');
        $info = $BbsPostModel->where('id',$id)->find();
        if(!empty($info['photos'])) $info['photos'] = explode(',',$info['photos']);

        $tagList = Db::table('cmf_tag')->where(['type'=>1,'status'=>1])->column('name','id');
        $this->assign('tagList',$tagList);
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * 编辑论坛帖子提交
     * @adminMenu(
     *     'name'   => '编辑论坛帖子提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '编辑论坛帖子提交',
     *     'param'  => ''
     * )
     */
    public function edit_post(){
        $BbsPostModel = new BbsPostModel();
        if (!$this->request->isPost()) $this->error('访问方式错误');

        $data = $this->request->post();
        if(!empty($data['photos'])) $data['photos'] = implode(',',$data['photos']);
        $result = $this->validate($data, 'bbs');
        if ($result !== true) $this->error($result);

        $save = $BbsPostModel->save($data,['id'=>$data['id']]);
        $save?$this->success('编辑成功'):$this->error('编辑失败');
    }

    /**
     * 论坛详情
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function info(){
        $BbsPostModel = new BbsPostModel();
        $id = $this->request->param('id',0,'intval');
        $where = ['b.id'=>$id];

        $info = $BbsPostModel->alias('b')
            ->join('cmf_user u','u.id = b.user_id')
            ->join('cmf_tag t','t.id = b.tag_id')
            ->field('b.*,t.name as tag_name,u.user_nickname,u.real_name')->where($where)->find();
        //dump($info);die;
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * 评论列表
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function comment(){
        $CommentModel = new BbsCommentModel();
        $id = $this->request->param('id',0,'intval');
        $keyword = $this->request->param('keyword','','trim');
        $where = ['c.delete_time'=>['gt',0]];
        if(!empty($keyword)) $where['u.to_user_name|u.real_name|b.title|t.name'] = ['like','%'.$keyword.'%'];

        $list = $CommentModel->alias('c')
            ->join('cmf_user u','u.id = c.user_id')
            ->field('c.object_id,c.id,c.user_id,c.create_time,c.content,c.to_user_id,IF(c.to_user_name="","未設置",c.to_user_name) as to_user_name,c.delete_time,u.user_nickname,u.real_name,u.avatar,u.user_type')
            ->order('create_time desc')
            ->where(['object_id'=>$id,'delete_time'=>0])->paginate(10);

        $this->assign('id',$id);
        $this->assign('list',$list);
        $this->assign('page', $list->render());
        $this->assign('keyword', $keyword);
        return $this->fetch();
    }

    //评论
    protected  $PrefixComment = 'comment_';
    protected  $table_name = 'bbs_comment';
    protected  $wx_url = '/pages/share';

    /**
     * 评论帖子提交
     * @adminMenu(
     *     'name'   => '评论帖子提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '评论帖子提交',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function comment_post(){
        $BbsPostModel = new BbsPostModel();
        if (!$this->request->isPost()) $this->error('访问方式错误');
        $data = $this->request->only('object_id,content','post');
        $object_title = $BbsPostModel->where('id',$data['object_id'])->value('title');
        $data['user_id'] = cmf_get_current_admin_id();
        $to_user_id = Db::table('cmf_bbs_post')->where('id',$data['object_id'])->value('user_id');
        $data['floor'] = Db::table('cmf_bbs_comment')->where(['object_id'=>$data['object_id'],'parent_id'=>0])->count()+1;
        $data['create_time'] = time();

        Db::startTrans();
        try{
            Db::table('cmf_bbs_post')->where('id',$data['object_id'])->setInc('comment_count');
            $add = Db::table('cmf_bbs_comment')->insertGetId($data);

            send_message(
                $data['user_id'],//发送者ID
                $to_user_id,//接收者ID
                $this->table_name,//对象表
                $add,//对象ID
                $object_title,//信息标题
                $data['content'],//信息内容
                $this->PrefixComment,//redis前缀
                $this->wx_url.'?id='.$data['object_id']//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('系统错误');
            exit;
        }
        $this->success('评论成功');
    }

    /**
     * 评论回复提交
     * @adminMenu(
     *     'name'   => '评论回复提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '评论回复提交',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function replay_post(){
        $BbsPostModel = new BbsPostModel();
        if (!$this->request->isPost()) $this->error('访问方式错误');

        $data = $this->request->only('object_id,to_user_id,content','post');
        $object_title = $BbsPostModel->where('id',$data['object_id'])->value('title');
        $data['user_id'] = cmf_get_current_admin_id();
        $data['floor'] = Db::table('cmf_bbs_comment')->where(['object_id'=>$data['object_id'],'parent_id'=>0])->count()+1;
        $data['create_time'] = time();
        $data['to_user_name'] = Db::table('cmf_user')->where('id',$data['to_user_id'])->value('user_nickname');

        Db::startTrans();
        try{
            Db::table('cmf_bbs_post')->where('id',$data['object_id'])->setInc('comment_count');
            $add = Db::table('cmf_bbs_comment')->insertGetId($data);

            send_message(
                $data['user_id'],//发送者ID
                $data['to_user_id'],//接收者ID
                $this->table_name,//对象表
                $add,//对象ID
                $object_title,//信息标题
                $data['content'],//信息内容
                $this->PrefixComment,//redis前缀
                $this->wx_url.'?id='.$data['object_id']//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('系统错误');
            exit;
        }
        $this->success('评论成功');
    }

    /**
     * 帖子删除
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function delete(){
        $BbsPostModel = new BbsPostModel();
        $id = $this->request->param('id',0,'intval');
        $save = $BbsPostModel->save(['delete_time'=>-time()],['id'=>$id]);
        $result = $BbsPostModel->where('id', $id)->find();
        if($save) Db::name('recycleBin')->insert(
            [
                'object_id'   => $result['id'],
                'create_time' => time(),
                'table_name'  => 'bbs_post',
                'name'        => $result['title'],
                'user_id'     => cmf_get_current_admin_id()
            ]
        );
        $save?$this->success('删除成功'):$this->error('删除失败');
    }

    /**
     * 帖子强制删除
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function del(){
        $BbsPostModel = new BbsPostModel();
        $CommentModel = new BbsCommentModel();
        $id = $this->request->param('id',0,'intval');
        $save = $BbsPostModel->where('id',$id)->delete();
        $CommentModel->where('object_id',$id)->delete();
        $save?$this->success('删除成功'):$this->error('删除失败');
    }

    /**
     * 评论删除
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function delete_comment(){
        $CommentModel = new BbsCommentModel();
        $id = $this->request->param('id',0,'intval');
        $save = $CommentModel->save(['delete_time'=>-time()],['id'=>$id]);
        $result = $CommentModel->where('id', $id)->find();
        if($save) Db::name('recycleBin')->insert(
            [
                'object_id'   => $result['id'],
                'create_time' => time(),
                'table_name'  => 'bbs_comment',
                'name'        => $result['title'],
                'user_id'     => cmf_get_current_admin_id()
            ]
        );
        $save?$this->success('删除成功'):$this->error('删除失败');
    }

    /**
     * 评论强制删除
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function del_comment(){
        $CommentModel = new BbsCommentModel();
        $id = $this->request->param('id',0,'intval');
        $save = $CommentModel->where('id',$id)->delete();
        $save?$this->success('删除成功'):$this->error('删除失败');
    }

}