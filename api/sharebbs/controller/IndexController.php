<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\sharebbs\controller;

use think\Db;
use api\common\controller\BaseController;
use api\sharebbs\model\BbsModel;

class IndexController extends BaseController
{
    /**
     * 举报帖子
     * @param  int $id 用户ID
     * @param  int $page 页码
     * @param  int $limit 每页条数
     * @throws \think\Exception
     * @return array
     */
	public function person($id = 0,$page = 0,$limit = 10) {
      $list = Db::table('cmf_bbs_post')
        ->field('bbs.*,tag.name as tag_name')
        ->alias('bbs')
        ->where(['user_id'=>$id, 'delete_time'=>0])
        ->join('cmf_tag tag', 'tag.id = bbs.tag_id', 'LEFT')
        ->limit($page*$limit,$limit)
        ->order('id desc')
        ->select()->toArray();
      return json(['code'=>1,'data'=>['list'=>$list]]);
    }
    /**
     * 启用的标签获取
     */
    public function tag(){
        $data['list'] = Db::table('cmf_tag')->where(['type'=>1,'status'=>1])->column('name','id');

        $this->success('获取成功',$data);
    }

    /**
     * 举报选项
     */
    public function report_option(){
        $report = new \api\sharebbs\model\ReportModel();
        $type = $report::$TYPE;
        $this->success('获取成功',$type);
    }

    /**
     * 举报帖子
     * @throws \think\Exception
     */
    public function report(){
        if(!$this->request->isPost()) $this->error('访问方式错误');
        $data['user_id'] = request()->uid;
        $data['object_id'] = $this->request->param('id', 0, 'intval');
        $data['to_user_id'] = $this->request->param('to_user_id', 0, 'intval');
        $data['table_name'] = 'bbs_post';

        $report = new \api\sharebbs\model\ReportModel();
        if($report->where($data)->count()>0) $this->error('您已经举报过了');

        $data['type'] = $this->request->param('type', 0, 'intval');
        if(!$data['object_id'] || !$data['to_user_id'] || !$data['type']) $this->error('参数错误');

        $data['create_time'] = time();
        $add = $report->insert($data);
        $add?$this->success('举报成功'):$this->success('举报失败');
    }

    /**
     * 发布帖子
     */
    public function add_post(){
        if(!$this->request->isPost()) $this->error('访问方式错误');

        $data = $this->request->only('title,tag_id,content,photos','post');
        $result = $this->validate($data, 'bbs');
        if ($result !== true) $this->error($result);

        $data['user_id'] = request()->uid;
        if(request()->user_relation['role_id'] == 5) $this->error('请您先完成实名认证');
        $data['user_nickname'] = Db::table('cmf_user')->where('id',$data['user_id'])->value('user_nickname');
        $data['create_time'] = time();

        $BbsModel = new BbsModel();
        $add = $BbsModel->insertGetId($data);

        $add?$this->success('添加成功',['id'=>$add]):$this->error('添加失败');
    }

    /**
     * 帖子详情
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function info(){
        $id = $this->request->param('id', 0, 'intval');
        $BbsModel = new BbsModel();
        $info = $BbsModel::get($id)->toArray();
        $info['photos'] = explode(',',$info['photos']);
        $info['tag'] = Db::table('cmf_tag')->where('id',$info['tag_id'])->value('name');

        //点赞记录获取
        $user_id = request()->uid;
        $info['is_like'] = Db::table('cmf_user_like')->where(['object_id'=>$info['id'],'user_id'=>$user_id,'table_name'=>'bbs_post'])->value('id')?1:0;

        $userInfo = Db::table('cmf_user')->where('id',$info['user_id'])->field('avatar,user_nickname')->find();
        $info = array_replace_recursive($userInfo,$info);

        if(@$info['delete_time'] != 0 ) $this->error('对不起！该帖子已被删除');
        $info?$this->success('获取成功',$info):$this->error('获取失败');
    }

    /**
     * 我发布的帖子
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function my(){
        $BbsModel = new BbsModel();
        $page = $this->request->param('page', 0, 'intval');
        $limit = $this->request->param('limit', 10, 'intval');
        $user_id = request()->uid;

        $where = ['user_id'=>$user_id,'delete_time'=>0];

        $data['list']  = $BbsModel->alias('b')
            ->join('cmf_tag t','t.id = b.tag_id')
            ->where($where)
            ->order('create_time desc')
            ->limit(($page*$limit),$limit)
            ->field('b.*,t.name as tag_name')
            ->select()->hidden(['photos','content'])->toArray();
        $data['count'] = $BbsModel->where($where)->count();
        $data['page'] = $page;
        $data['limit'] = $limit;

        $this->success('获取成功',$data);
    }

    /**
     * 删除帖子
     */
    public function delete(){
        $BbsModel = new BbsModel();
        $id = $this->request->param('id', 0, 'intval');
        $user_id = request()->uid;
        $save = $BbsModel->where('id',$id)->where('user_id',$user_id)->update(['delete_time'=>time()]);

        if($save){
            //删除成功后，通知评论的人帖子被删了
            //msg_sign();

            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    /**
     * 论坛列表
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function index(){
        $BbsModel = new BbsModel();
        $tag_id = $this->request->param('tag_id', 0, 'intval');
        $page = $this->request->param('page', 0, 'intval');
        $limit = $this->request->param('limit', 10, 'intval');
        $keyword = $this->request->param('keyword', '', 'trim');
        $user_id = request()->uid;

        $where = ['b.delete_time'=>0,'b.status'=>1];
        if($tag_id) $where['b.tag_id'] = $tag_id;
        if($keyword) $where['b.title|t.name'] = ['like','%'.$keyword.'%'];

        $data['list']  = $BbsModel->alias('b')
            ->join('cmf_user u','u.id = b.user_id')
            ->join('cmf_tag t','b.tag_id = t.id','left')
            ->join('cmf_user_like l','l.object_id = b.id AND l.table_name = "bbs_post" AND l.user_id ='.$user_id,'left')
            ->where($where)
            ->order('b.create_time desc')
            ->group('b.id')
            ->field('b.*,u.user_nickname,u.avatar,t.name as tag_name,IF(l.id,1,0) as is_like')
            ->limit(($page*$limit),$limit)
            ->select()->toArray();
        $data['count'] = $BbsModel->alias('b')->join('cmf_tag t','b.tag_id = t.id','left')->where($where)->count();
        $data['page'] = $page;
        $data['limit'] = $limit;

        $this->success('获取成功',$data);
    }

    /**
     * 查看帖子
     * @throws \think\Exception
     */
    public function hit(){
        $BbsModel = new BbsModel();
        $id = $this->request->param('id', 0, 'intval');
        $save = $BbsModel->where('id',$id)->setInc('hit_count');

        if($save){
            $this->success('点击成功');
        }else{
            $this->error('点击失败');
        }
    }

    /**
     * 点赞帖子
     * @throws \think\Exception
     */
    public function like(){
        $id = $this->request->param('id', 0, 'intval');
        $user_id = request()->uid;
        if(Db::table('cmf_user_like')->where(['user_id'=>$user_id,'object_id'=>$id,'table_name'=>'bbs_post'])->count()>0) $this->error('您已经点过赞啦！');
        Db::startTrans();
        try{
            Db::table('cmf_bbs_post')->where('id',$id)->setInc('like_count');
            $title = Db::table('cmf_bbs_post')->where('id',$id)->value('title');
            Db::table('cmf_user_like')->insertGetId(['user_id'=>$user_id,'create_time'=>time(),'table_name'=>'bbs_post','object_id'=>$id,'title'=>$title]);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('系统错误');
            exit;
        }
        $this->success('点赞成功');
    }

    /**
     * 取消点赞帖子
     */
    public function cancel_like(){
        $id = $this->request->param('id', 0, 'intval');
        $user_id = request()->uid;
        if(!$like_id = Db::table('cmf_user_like')->where(['user_id'=>$user_id,'object_id'=>$id,'table_name'=>'bbs_post'])->value('id')) $this->error('您还没有点赞呢！');

        Db::startTrans();
        try{
            Db::table('cmf_bbs_post')->where('id',$id)->setDec('like_count');
            Db::table('cmf_user_like')->delete($like_id);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('系统错误');
            exit;
        }

        $this->success('取消点赞成功');
    }

    /**
     * 分享帖子
     * @throws \think\Exception
     */
    public function share(){
        $BbsModel = new BbsModel();
        $id = $this->request->param('id', 0, 'intval');
        $save = $BbsModel->where('id',$id)->setInc('share_count');

        if($save){
            $this->success('分享成功');
        }else{
            $this->error('分享失败');
        }
    }

}