<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
return [
    'ServiceName'                 => [// 在后台插件配置表单中的键名 ,会是config[text]
        'title'   => '又拍云服务名称', // 表单的label标题
        'type'    => 'text',// 表单的类型：text,password,textarea,checkbox,radio,select等
        'value'   => '',// 表单的默认值
        "rule"    => [
            "require" => true
        ],
        "message" => [
            "require" => 'ServiceName不能为空'
        ],
        'tip'     => '您的服务名' //表单的帮助提示
    ],
    'OperatorName'                 => [// 在后台插件配置表单中的键名 ,会是config[text]
        'title'   => '操作员账号',
        'type'    => 'text',
        'value'   => '',
        "rule"    => [
            "require" => true
        ],
        "message" => [
            "require" => 'OperatorName不能为空'
        ],
        'tip'     => '您的操作员账号'
    ],
    'OperatorPwd'                 => [// 在后台插件配置表单中的键名 ,会是config[password]
        'title'   => '操作员密码',
        'type'    => 'password',
        'value'   => '',
        "rule"    => [
            "require" => true
        ],
        "message" => [
            "require" => 'OperatorPwd不能为空'
        ],
        'tip'     => '您的操作员密码'
    ],
    'protocol'                  => [// 在后台插件配置表单中的键名 ,会是config[select]
        'title'   => '域名协议',
        'type'    => 'select',
        'options' => [//select 和radio,checkbox的子选项
            'http'  => 'http',// 值=>显示
            'https' => 'https',
        ],
        'value'   => 'http',
        "rule"    => [
            "require" => true
        ],
        "message" => [
            "require" => '域名协议不能为空'
        ],
        'tip'     => ''
    ],
    'domain'                    => [
        'title'   => '空间域名',
        'type'    => 'text',
        'value'   => '',
        "rule"    => [
            "require" => true
        ],
        "message" => [
            "require" => '空间域名不能为空'
        ],
        'tip'     => ''
    ],
    /*'bucket'                    => [
        'title'   => '空间名称',
        'type'    => 'text',
        'value'   => '',
        "rule"    => [
            "require" => true
        ],
        "message" => [
            "require" => '空间名称不能为空'
        ],
        'tip'     => ''
    ],*/
    /*'zone'                      => [// 在后台插件配置表单中的键名 ,会是config[select]
        'title'   => '存储区域',
        'type'    => 'select',
        'options' => [//select 和radio,checkbox的子选项
            'z0'  => '华东',// 值=>显示
            'z1'  => '华北',
            'z2'  => '华南',
            'na0' => '北美',
            'as0' => '东南亚',
        ],
        'value'   => 'http',
        "rule"    => [
            "require" => true
        ],
        "message" => [
            "require" => '存储区域不能为空'
        ],
        'tip'     => ''
    ],*/
    'style_separator'           => [
        'title'   => '样式分隔符',
        'type'    => 'text',
        'value'   => '!',
        'tip'     => ''
    ],
    /*'styles_watermark'          => [
        'title'   => '样式-水印',
        'type'    => 'explain',
        'value'   => 'watermark',
        'tip'     => '请到七牛存储空间->图片样式：添加此样式名称，并进行相应设置 处理接口<span style="color: red;">（请注意修改水印文字）</span>:<br>imageMogr2/auto-orient/thumbnail/1080x1080>/blur/1x0/quality/75|watermark/2/text/VGhpbmtDTUY=/font/5b6u6L2v6ZuF6buR/fontsize/500/fill/I0ZGRkZGRg==/dissolve/100/gravity/SouthEast/dx/10/dy/10'
    ],
    'styles_avatar'             => [
        'title'   => '样式-头像',
        'type'    => 'explain',
        'value'   => 'avatar',
        'tip'     => '请到七牛存储空间->图片样式：添加此样式名称，并进行相应设置 处理接口:<br>imageMogr2/auto-orient/thumbnail/!100x100r/gravity/Center/crop/100x100/quality/100/interlace/0'
    ],
    'styles_thumbnail120x120'   => [
        'title'   => '样式-缩略图120x120',
        'type'    => 'explain',
        'value'   => 'thumbnail120x120',
        'tip'     => '请到七牛存储空间->图片样式：添加此样式名称，并进行相应设置<br>处理接口:<br>imageMogr2/auto-orient/thumbnail/!120x120r/gravity/Center/crop/120x120/quality/100/interlace/0'
    ],
    'styles_thumbnail300x300'   => [
        'title'   => '样式-缩略图300x300',
        'type'    => 'explain',
        'value'   => 'thumbnail300x300',
        'tip'     => '请到七牛存储空间->图片样式：添加此样式名称，并进行相应设置<br>处理接口:<br>imageMogr2/auto-orient/thumbnail/!300x300r/gravity/Center/crop/300x300/quality/100/interlace/0'
    ],
    'styles_thumbnail640x640'   => [
        'title'   => '样式-缩略图640x640',
        'type'    => 'explain',
        'value'   => 'thumbnail640x640',
        'tip'     => '请到七牛存储空间->图片样式：添加此样式名称，并进行相应设置<br>处理接口:<br>imageMogr2/auto-orient/thumbnail/!640x640r/gravity/Center/crop/640x640/quality/100/interlace/0'
    ],
    'styles_thumbnail1080x1080' => [
        'title'   => '样式-缩略图1080x1080',
        'type'    => 'explain',
        'value'   => 'thumbnail1080x1080',
        'tip'     => '请到七牛存储空间->图片样式：添加此样式名称，并进行相应设置<br>处理接口:<br>imageMogr2/auto-orient/thumbnail/!1080x1080r/gravity/Center/crop/1080x1080/quality/100/interlace/0'
    ],*/
];
					