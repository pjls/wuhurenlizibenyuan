<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace api\company\validate;

use think\Validate;

class CompanyPostValidate extends Validate
{
    protected $rule = [
        'company' => 'require',
        'legal_man' => 'require',
        'contact_name' => 'require',
        'contact_phone' => 'require|checkPhone:thinkphp',
    ];

    protected $message = [
        'company.require' => '请填写公司名称',
        'legal_man.require'  => '请填写法人名称',
        'contact_name.require'  => '请填写联系人',
        'contact_phone.require'  => '请填写联系电话',
    ];

    // 自定义验证规则
    protected function checkPhone($value,$rule,$data)
    {
        if(!preg_match('/^1\d{10}$/',$value)){
            return '联系电话格式错误';
        }else{
            return true;
        }
    }
}