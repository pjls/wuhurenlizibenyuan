<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\workstu\controller;

use Exception;
use think\Db;
use api\common\controller\BaseController;
use api\workstu\model\Order;


class OrderController extends BaseController
{
    protected  $redis;
    protected  $PrefixComment;
    protected  $table_name = 'work_student_order_post';
    protected  $wx_company_order_url = '/pages/part_time/order_list';
    protected  $wx_user_order_url = '/pages/part_time/info';

    /**
     * 架构函数
     * @access public
     */
    public function __construct(){
        parent::__construct();
        if(request()->user_relation['role_id'] == 5){
            return json(['code'=>0,'status'=>0,'msg'=>'请您先完成实名认证','data'=>[],'errcode'=>40001,'errRedirect'=>'/pages/me/verify/index'])->send();
        }elseif(request()->user_relation['role_id'] == 3){
            return json(['code'=>0,'status'=>0,'msg'=>'企业认证者不得投递简历！','data'=>[],'errcode'=>40001,'errRedirect'=>'']);
        }
    }

    //获取兼职电话
    public function get_mobile(Order $Order,$order_id = 0){
        $mobile = $Order->alias('o')
            ->join('cmf_user u','o.user_id = u.id')
            ->where('o.id',$order_id)->value('mobile');
        $this->success('获取成功',['mobile'=>$mobile]);
    }

    //未查阅申请变更条数 (公司/个人)
    /*public function unread_message(Order $Order,User $User){
        $user_id = request()->uid;
        $wx_role = $User::$wx_role;
        try{
            $userInfo = Db::table('cmf_user_relation')->where('user_id',$user_id)->field('company_id,role_id')->find();
            //企业
            if($userInfo['role_id'] == 3){
                $tipsCount = $Order->where('company_id',$userInfo['company_id'])->where('delete_time',0)->value('sum(company_tip = 1)');
            //个人
            }else{
                $tipsCount = $Order->where('company_id',$userInfo['company_id'])->where('delete_time',0)->value('sum(student_tip = 1)');
            }
        }catch (Exception $e){
            $this->error('系统错误');
            exit;
        }
        $this->success('获取成功',['user_id'=>$user_id,'tipsCount'=>$tipsCount,'role'=>$wx_role[$userInfo['role_id']]]);
    }*/

    /**
     * 申请职位
     * @throws \think\Exception
     */
    public function position_post(){
        $Order = new Order();
        if(!$this->request->isPost()) $this->error('访问失败！');
        $id = $this->request->post('id');
        if(!$id) $this->error('参数错误！');
        $user_id = request()->uid;

        $count = $Order->where(['user_id'=>$user_id,'position_id'=>$id,'delete_time'=>0])->count();
        $position = Db::table('cmf_work_student_position')->where('id',$id)->field('name,user_id,company_id,phone,end_date')->find();
        $resume = Db::table('cmf_job_resume')->where('user_id',$user_id)->find();

        if($count > 0){
            $this->error('您已经申请过了！');
        }elseif($position['end_date']<time()){
            $this->error('该职位过期了！');
        }elseif(empty($resume['mobile'])||empty($resume['name'])){
            return json(['code'=>0,'status'=>0,'msg'=>'请完善您简历的基础信息！','data'=>[],'errcode'=>40003,'errRedirect'=>'/pages/me/resume/basic/index'])->send();
        }

        $data['position_id'] = $id;
        $data['user_id'] = $user_id;
        $data['company_id'] = $position['company_id'];
        $data['resume_id'] = $resume['id'];
        $data['create_time'] = time();
        $data['check_status'] = 0;
        $data['order_status'] = -1;

        Db::startTrans();
        try{
            Db::table('cmf_work_student_position')->where('id',$id)->setInc('sign_count');
            $add = Db::table('cmf_work_student_order_post')->insertGetId($data);


            $to_user_id = $position['user_id'];
            $real_name = Db::table('cmf_user')->where('id',$user_id)->value('real_name');
            $msg = $real_name.'申请兼职“'.$position['name'].'”';
            $url = $this->wx_company_order_url.'?id='.$id;
            send_message(
                $user_id,//发送者ID
                $to_user_id,//接收者ID
                $this->table_name,//对象表
                $add,//对象ID
                '有新的申请报名',//信息标题
                $msg,//信息内容
                config('HR_PARK_MSG'),//redis前缀
                $url//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        if($add){
            $this->success('申请成功',['phone'=>$position['phone']]);
        }else{
            $this->error('申请失败');
        }
        exit;
    }

    //学生正在申请中的职位
    public function checking_order(Order $Order,$page = 0,$limit = 10){
        $user_id = request()->uid;

        //提示信息条数被阅读
        $Order->save(['student_tip'=>0],['user_id'=>$user_id,'delete_time'=>0]);

        $where['o.user_id'] = $user_id;
        $where['o.delete_time'] = 0;
        $where['o.check_status'] = ['in','0,1'];
        $where['o.order_status'] = -1;
        $where['p.delete_time'] = 0;

        //,IF(p.delete_time=0,0,1) as position_delete
        $field = [
            'o.id',
            'o.position_id',
            'o.company_id',
            'o.check_status',
            'o.order_status',
            'p.*',
            'IF(p.delete_time=0,0,1) as position_delete',
            'FROM_UNIXTIME(p.create_time,"%Y-%m-%d %H:%i:%s") as create_time',
            'IF(p.salary_method=1,"小时","天") as salary_method',
            'compute.name as compute',
            'c.company as company_name',
            'c.logo',
            'c.description company_desc'
        ];

        $list = $Order->get_order($where,$field,$page,$limit);
        try{
            $data['count'] = $Order->get_count($where);
        }catch (Exception $e){}
        $data['list'] = $list;
        $data['page'] = $page;
        $data['limit'] = $limit;
        $list?$this->success('获取成功',$data):$this->error('获取失败');
    }

    //学生正在进行中的职位
    public function working_order(Order $Order,$page = 0,$limit = 10){
        $user_id = request()->uid;

        $where['o.user_id'] = $user_id;
        $where['o.delete_time'] = 0;
        $where['o.check_status'] = 3;
        $where['o.order_status'] = 0;
        $where['p.delete_time'] = 0;

        $field = 'o.id,o.position_id,o.company_id,o.check_status,p.*,IF(p.delete_time=0,0,1) as position_delete,FROM_UNIXTIME(p.create_time,"%Y-%m-%d %H:%i:%s") as create_time,IF(p.salary_method=1,"小时","天") as salary_method,compute.name as compute,c.company as company_name,c.logo,c.description company_desc';

        $list = $Order->get_order($where,$field,$page,$limit);
        try{
            $data['count'] = $Order->get_count($where);
        }catch (Exception $e){}
        $data['list'] = $list;
        $data['page'] = $page;
        $data['limit'] = $limit;
        $list?$this->success('获取成功',$data):$this->error('获取失败');
    }

    //学生已完成的职位
    public function ending_order(Order $Order,$page = 0,$limit = 10){
        $user_id = request()->uid;

        $where['o.user_id'] = $user_id;
        $where['o.delete_time'] = 0;
        $where['o.check_status'] = ['in','2,3'];
        $where['o.order_status'] = ['in','1,2'];
        //$where['p.delete_time'] = 0;

        $field = 'o.id,o.position_id,o.company_id,o.check_status,o.order_status,o.complete_money,p.*,IF(p.delete_time=0,0,1) as position_delete,FROM_UNIXTIME(p.create_time,"%Y-%m-%d %H:%i:%s") as create_time,p.salary,IF(p.salary_method=1,"小时","天") as salary_method,compute.name as compute,c.company as company_name,c.logo,c.description company_desc';

        $list = $Order->get_order($where,$field,$page,$limit);
        try{
            $data['count'] = $Order->get_count($where);
        }catch (Exception $e){}
        $data['list'] = $list;
        $data['page'] = $page;
        $data['limit'] = $limit;
        $list?$this->success('获取成功',$data):$this->error('获取失败');
    }

    //职位申请的详情
    public function order_info(Order $Order,$id = 0){
        try{
            $data['info'] = $Order->alias('o')
                ->join('cmf_work_student_position p','p.id = o.position_id')
                ->join('cmf_work_student_order_comment c','o.id = c.order_id')
                ->field('o.id,o.position_id,o.user_id,o.company_id,o.resume_id,c.company_grade,c.company_evaluation,c.company_comment,o.complete_money,o.check_status,o.order_status,FROM_UNIXTIME(o.create_time,"%Y-%m-%d %H:%i:%s") as create_time,p.name as position_name')
                ->where('o.id',$id)->find();
        }catch (Exception $e){
            $this->error('系统错误');
            exit;
        }
        if($data['info']['delete_time']) $this->error('该申请已被撤销');
        $this->success('获取成功',$data);
    }

    /**
     * 学生撤销职位申请
     * @throws \think\Exception
     */
    public function delete(){
        $Order = new Order();
        $id = $this->request->param('id',0,'intval');
        $user_id = request()->uid;
        $delete = $Order->save(['delete_time'=>time()],['id'=>$id,'user_id'=>$user_id]);
        if($delete){
            Db::table('cmf_work_student_position')->where('id',$id)->setDec('sign_count');
            $this->success('撤销成功');
        }else{
            $this->error('撤销失败');
        }
    }

    //公司所有申请列表
    public function company_order(Order $Order,$status = 0,$page = 0,$limit = 10){
        //$user_id = request()->uid;
        $company_id = request()->user_relation['company_id'];

        //提示信息条数被阅读
        $Order->save(['company_tip'=>0,'delete_time'=>0],['company_id'=>$company_id]);

        $where['o.company_id'] = $company_id;
        $where['o.delete_time'] = 0;
        if($status == 1){
            $where['o.check_status'] = ['in','0,1'];
        }elseif($status == 2){
            $where['o.check_status'] = ['in','2,3'];
        }
        //$where['o.order_status'] = 0;
        $where['p.delete_time'] = 0;

        $field = [
            'o.id',
            'o.position_id',
            'o.user_id',
            'o.check_status',
            'o.resume_id',
            'p.name as position_name',
            'FROM_UNIXTIME(o.create_time,"%Y-%m-%d %H:%i:%s") as create_time',
            'p.salary',
            'IF(p.salary_method=1,"小时","天") as salary_method',
            'compute.name as compute_name',
            'u.user_nickname',
            'r.name',
            'r.avatar',
        ];

        $list = $Order->get_order($where,$field,$page,$limit);
        try{
            unset($where['p.delete_time']);
            $data['count'] = $Order->alias('o')->where($where)->count();
        }catch (Exception $e){}
        $data['list'] = $list;
        $data['page'] = $page;
        $data['limit'] = $limit;
        $list?$this->success('获取成功',$data):$this->error('获取失败');
    }

    //公司所有申请列表
    public function company_complete_order(Order $Order,$status = 1,$page = 0,$limit = 10){
        //$user_id = request()->uid;
        $company_id = request()->user_relation['company_id'];

        //提示信息条数被阅读
        $Order->save(['company_tip'=>0,'delete_time'=>0],['company_id'=>$company_id]);

        $where['o.company_id'] = $company_id;
        $where['o.delete_time'] = 0;
        $where['o.check_status'] = 3;
        $where['o.order_status'] = $status;
        $where['p.delete_time'] = 0;

        $field = 'o.id,o.position_id,o.user_id,o.check_status,o.order_status,p.name as position_name,FROM_UNIXTIME(o.create_time,"%Y-%m-%d %H:%i:%s") as create_time,p.salary,IF(p.salary_method=1,"小时","天") as salary_method,compute.name as compute_name,u.real_name,u.user_nickname,u.avatar';

        $list = $Order->get_order($where,$field,$page,$limit);
        try{
            unset($where['p.delete_time']);
            $data['count'] = $Order->alias('o')->where($where)->count();
        }catch (Exception $e){}
        $data['list'] = $list;
        $data['page'] = $page;
        $data['limit'] = $limit;
        $list?$this->success('获取成功',$data):$this->error('获取失败');
    }

    /*//公司未操作的申请列表
    public function company_uncheck_order(Order $Order,$page = 0,$limit = 10){
        //$user_id = request()->uid;
        $company_id = request()->user_relation['company_id'];

        //提示信息条数被阅读
        $Order->save(['company_tip'=>0,'delete_time'=>0],['company_id'=>$company_id]);

        $where['o.company_id'] = $company_id;
        $where['o.delete_time'] = 0;
        $where['o.check_status'] = ['in','0,1'];
        //$where['o.order_status'] = 0;
        $where['p.delete_time'] = 0;

        $field = 'o.id,o.position_id,o.user_id,o.check_status,o.order_status,p.name as position_name,FROM_UNIXTIME(o.create_time,"%Y-%m-%d %H:%i:%s") as create_time,p.salary,IF(p.salary_method=1,"小时","天") as salary_method,compute.name as compute_name,u.real_name,u.user_nickname,u.avatar';

        $list = $Order->get_order($where,$field,$page,$limit);
        try{
            unset($where['p.delete_time']);
            $data['count'] = $Order->alias('o')->where($where)->count();
        }catch (Exception $e){}
        $data['list'] = $list;
        $data['page'] = $page;
        $data['limit'] = $limit;
        $list?$this->success('获取成功',$data):$this->error('获取失败');
    }

    //公司已操作的申请列表
    public function company_check_order(Order $Order,$page = 0,$limit = 10){
        //$user_id = request()->uid;
        $company_id = request()->user_relation['company_id'];

        //提示信息条数被阅读
        $Order->save(['company_tip'=>0,'delete_time'=>0],['company_id'=>$company_id]);

        $where['o.company_id'] = $company_id;
        $where['o.delete_time'] = 0;
        $where['o.check_status'] = ['not in','0,1'];
        //$where['o.order_status'] = 0;
        $where['p.delete_time'] = 0;

        $field = 'o.id,o.position_id,o.user_id,o.check_status,o.order_status,p.name as position_name,FROM_UNIXTIME(o.create_time,"%Y-%m-%d %H:%i:%s") as create_time,p.salary,IF(p.salary_method=1,"小时","天") as salary_method,compute.name as compute_name,u.real_name,u.user_nickname,u.avatar';

        $list = $Order->get_order($where,$field,$page,$limit);
        try{
            unset($where['p.delete_time']);
            $data['count'] = $Order->alias('o')->where($where)->count();
        }catch (Exception $e){}
        $data['list'] = $list;
        $data['page'] = $page;
        $data['limit'] = $limit;
        $list?$this->success('获取成功',$data):$this->error('获取失败');
    }*/

    //企业兼职正在进行的人
    public function position_working(Order $Order,$position_id = 0,$page = 0,$limit = 10){
        $where = ['o.position_id'=>$position_id,'o.delete_time'=>0,'o.order_status'=>0,'o.check_status'=>3];
        try{
            $data['count'] = $Order->alias('o')->join('cmf_user u','o.user_id = u.id')->where($where)->count();
            $data['list'] = $Order->alias('o')
                ->join('cmf_user u','o.user_id = u.id')
                ->field('o.id as order_id,o.user_id,o.position_id,u.user_nickname,u.real_name,u.avatar')
                ->where($where)->limit(((int) $page * (int) $limit), $limit)->select()->toArray();
        }catch (Exception $e){
            $this->error('系统错误');
        }
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
    }

    /**
     * 公司已查看简历
     * @throws \think\Exception
     */
    public function order_check(){
        $id = $this->request->param('id',0,'intval');
        $user_id = request()->uid;

        if(!$info= Db::table('cmf_work_student_order_post')->where(['id'=>$id])->find()) $this->error('没有该申请');
        Db::startTrans();
        try{
            $comp = Db::table('cmf_company')->where('id',$info['company_id'])->value('company');
            $position = Db::table('cmf_work_student_position')->where('id',$info['position_id'])->value('name');
            Db::table('cmf_work_student_order_post')->where(['id'=>$id])->update(['check_status'=>1]);

            send_message(
                $user_id,//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您投递的兼职“'.$position.'”有新进展！',//信息标题
                '@'.$comp.'查看了您的简历！',//信息内容
                config('HR_PARK_MSG'),//redis前缀
                $this->wx_user_order_url.'?id='.$id//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $this->success('更改成功');
        exit;
    }

    /**
     * 公司同意申请
     * @throws \think\Exception
     */
    public function order_agree(){
        $id = $this->request->param('id',0,'intval');
        $user_id = request()->uid;

        if(!$info= Db::table('cmf_work_student_order_post')->where(['id'=>$id])->find()) $this->error('没有该申请');
        Db::startTrans();
        try{
            $comp = Db::table('cmf_company')->where('id',$info['company_id'])->value('company');
            $position = Db::table('cmf_work_student_position')->where('id',$info['position_id'])->value('name');
            Db::table('cmf_work_student_order_post')->where(['id'=>$id])->update(['check_status'=>3,'order_status'=>0]);

            send_message(
                $user_id,//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您投递的兼职“'.$position.'”有新进展！',//信息标题
                '@'.$comp.'对您的申请有意向！请等待人资联系！',//信息内容
                config('HR_PARK_MSG'),//redis前缀
                $this->wx_user_order_url.'?id='.$id//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $this->success('更改成功');
        exit;
    }

    /**
     * 公司拒绝申请
     * @throws \think\Exception
     */
    public function order_refuse(){
        $id = $this->request->param('id',0,'intval');
        $user_id = request()->uid;

        if(!$info= Db::table('cmf_work_student_order_post')->where(['id'=>$id])->find()) $this->error('没有该申请');
        Db::startTrans();
        try{
            $comp = Db::table('cmf_company')->where('id',$info['company_id'])->value('company');
            $position = Db::table('cmf_work_student_position')->where('id',$info['position_id'])->value('name');
            Db::table('cmf_work_student_order_post')->where(['id'=>$id])->update(['check_status'=>2,'order_status'=>-1]);

            send_message(
                $user_id,//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您投递的兼职“'.$position.'”有新进展！',//信息标题
                '@'.$comp.'对您的申请不感兴趣！',//信息内容
                config('HR_PARK_MSG'),//redis前缀
                $this->wx_user_order_url.'?id='.$id//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $this->success('更改成功');
        exit;
    }

    /**
     * 学生确认兼职完成
     * @throws \think\Exception
     */
    public function order_complete(){
        $id = $this->request->param('id',0,'intval');
        $user_id = request()->uid;

        if(!$info= Db::table('cmf_work_student_order_post')->where(['id'=>$id])->find()) $this->error('没有该申请！');
        if($info['user_id'] != $user_id) $this->error('不属于您的申请！');
        Db::startTrans();
        try{
            $user_name = Db::table('cmf_user')->where('id',$info['user_id'])->value('real_name');
            $position = Db::table('cmf_work_student_position')->where('id',$info['position_id'])->value('name');
            Db::table('cmf_work_student_order_post')->where(['id'=>$id])->update(['check_status'=>3,'order_status'=>1]);

            send_message(
                $user_id,//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '兼职“'.$position.'”有新进展！',//信息标题
                '@'.$user_name.'提交完成，请尽快确认！',//信息内容
                config('HR_PARK_MSG'),//redis前缀
                $this->wx_company_order_url.'?id='.$id//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $this->success('更改成功');
        exit;
    }

    /**
     * 公司确认兼职已完成
     * @throws \think\Exception
     */
    public function order_complete_sure(){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('company_grade,company_comment','post');
        $id = $this->request->param('id',0,'intval');
        $user_id = request()->uid;

        $result = $this->validate($data, 'order');
        if ($result !== true) $this->error($result);

        if(!$info= Db::table('cmf_work_student_order_post')->where(['id'=>$id])->find()) $this->error('没有该申请！');
        if($info['user_id'] != $user_id) $this->error('不属于您的申请！');
        Db::startTrans();
        try{
            $comp = Db::table('cmf_company')->where('id',$info['company_id'])->value('company');
            $position = Db::table('cmf_work_student_position')->where('id',$info['position_id'])->value('name');
            Db::table('cmf_work_student_order_post')->where(['id'=>$id])->update(['check_status'=>3,'order_status'=>2]);

            $data['user_id'] = $info['user_id'];
            $data['company_id'] = $info['company_id'];
            $data['order_id'] = $id;
            if(Db::table('cmf_work_student_order_comment')->where('order_id',$data['order_id'])->count()>0){
                Db::table('cmf_work_student_order_comment')->where(['order_id'=>$data['order_id']])->update($data);
            }else{
                Db::table('cmf_work_student_order_comment')->insert($data);
            }

            send_message(
                $user_id,//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您的兼职“'.$position.'”有新进展！',//信息标题
                '@'.$comp.'确认了您的兼职已完成！',//信息内容
                config('HR_PARK_MSG'),//redis前缀
                $this->wx_user_order_url.'?id='.$id//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $this->success('更改成功');
        exit;
    }
}