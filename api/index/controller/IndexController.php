<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\index\controller;

use think\Db;
use api\common\controller\BaseController;
use api\common\model\User;
use api\common\model\FormId;
use api\user\model\VerificationCodeModel;

class IndexController extends BaseController
{
    //获取formId
    public function save_form(FormId $FormIdModel,User $UserModel)
	{
        $form_id = input('formId');
        if(!$form_id) $this->error('formId为空');
        $uid = request()->uid;
        $openid = $UserModel->where('id',$uid)->value('wxpopenid');
        $FormIdModel->isUpdate(false)->save(['wxpopenid'=>$openid,'form_id'=>$form_id,'create_time'=>time()]);
        $this->success('存储成功');
    }

    //获取验证码
    public function get_sms_code(VerificationCodeModel $VerificationCodeModel){
        if($this->request->isPost()){
            $mobile = $this->request->post('mobile');
            if(!preg_match('/^1\d{10}$/',$mobile) || empty($mobile)) $this->error('手机格式错误');

            $smsCode = rand(1000,9999);
            $result = send_code($smsCode,$mobile);
            if($result){
                $now = time();
                //有效期验证码刷新
                $VerificationCodeModel->insert(['account'=>$mobile,'code'=>$smsCode,'send_time'=>$now,'expire_time'=>$now+300]);
                $this->success('下发成功',['code'=>'']);
            }else{
                $this->error('下发失败',$result);
            }
        }else{
            $this->error('访问失败');
        }
    }

    //网站设置变量获取
    public function site_var(){
        $var = cmf_get_option('site_var');
        $this->success('获取成功',$var);
    }

    //地区列表
    public function area_list(){
        $data['list'] = getAreaList();
        $this->success('获取成功',$data);
    }

    public function service_menu(){
        $titleArray = ['1'=>'企业服务','2'=>'园区服务'];
        $menu = Db::table('div_form_title')->group('type')->field('type')->select()->toArray();
        foreach($menu as $k=>$v){
            $menu[$k]['title'] = $titleArray[$v['type']];
            $menu[$k]['list'] = Db::table('div_form_title')->where(['status'=>1,'type'=>$v['type']])->select()->toArray();
        }
        $menu?$this->success('获取成功',['menu'=>$menu]):$this->error('获取失败');
    }


    const APP_ID = '17066505';
    const APP_KEY = 'WhftDit2X8MXZ0jIUvODMghC';
    const SECRET_KEY = 'YRm8OT5MTu8GVCNBZDbD3ysFYYDpSnvl';
    public function getBaiDuToken()
    {
        $url = sprintf('https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=%s&client_secret=%s&', self::APP_KEY, self::SECRET_KEY);
        $res = file_get_contents($url);
        $session = json_decode($res, true);
        $access_token = $session['access_token'];
        $this->success('获取成功',['access_token' => $access_token, 'expires' => (time() + $session['expires_in']) * 1000]);
    }
}