<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\job\controller;

use think\Db;
use Exception;
use api\common\controller\BaseController;
use api\job\model\Position as Position;
use api\common\model\User;


class PositionController extends BaseController
{
    public  $orderList = [['id'=>'1','name'=>'按发布时间排序'],['id'=>'2','name'=>'按热度排序']];//排序字段

    public  $salaryList = [
        1  => ['id'=>'1' ,'name'=>'2千以下','salary_lowest'=>0,'salary_highest'=>2],
        2  => ['id'=>'2' ,'name'=>'2-3千','salary_lowest'=>2,'salary_highest'=>3],
        3  => ['id'=>'3' ,'name'=>'3-4.5千','salary_lowest'=>3,'salary_highest'=>4.5],
        4  => ['id'=>'4' ,'name'=>'4.5-6千','salary_lowest'=>4.5,'salary_highest'=>6],
        5  => ['id'=>'5' ,'name'=>'6-8千','salary_lowest'=>6,'salary_highest'=>8],
        6  => ['id'=>'6' ,'name'=>'0.8-1万','salary_lowest'=>8,'salary_highest'=>10],
        7  => ['id'=>'7' ,'name'=>'1-1.5万','salary_lowest'=>10,'salary_highest'=>15],
        8  => ['id'=>'8' ,'name'=>'1.5-2万','salary_lowest'=>15,'salary_highest'=>20],
        9  => ['id'=>'9' ,'name'=>'2-3万','salary_lowest'=>20,'salary_highest'=>30],
        10 => ['id'=>'10','name'=>'3-4万','salary_lowest'=>30,'salary_highest'=>40],
        11 => ['id'=>'11','name'=>'4-5万','salary_lowest'=>40,'salary_highest'=>50],
        12 => ['id'=>'12','name'=>'5万以上','salary_lowest'=>50,'salary_highest'=>0]
    ];//排序字段

    //选项接口
    public function get_type(){
        try{
            $data['educationList'] = Db::table('cmf_job_education')->where('status',1)->where('id','neq',-1)->order('list_order asc,id asc')->field('name,id')->select()->toArray();
            $data['experienceList'] = Db::table('cmf_job_experience')->where('status',1)->order('list_order asc,id asc')->field('name,id')->select()->toArray();
        }catch (Exception $e) {
            $this->error('获取失败');
            exit;
        }
        $this->success('获取成功',$data);
    }

    public function type_list(){
        try{
            $data['list'] = Db::table('cmf_job_type')->where('RIGHT(id,2) = 00')->field('id,name')->order('id asc')->select()->toArray();
            foreach($data['list'] as $k => $v){
                if($v['id'] == 0 ){
                    $data['list'][$k]['child'] = [];
                    continue;
                }
                $data['list'][$k]['child'] = Db::table('cmf_job_type')->where('LEFT(LPAD(id,4,0),2) = LEFT(LPAD('.$v['id'].',4,0),2)')->field('id,name')->select()->toArray();
            }
        }catch (Exception $e) {
            $this->error('获取失败');
            exit;
        }
        $this->success('获取成功',$data);
    }

    //岗位列表筛选项获取
    public function search_type(){
        try{
            $data['industryList'] = Db::table('cmf_job_type')->where('Left(id,2) = 00')->where('id != 0')->field('id,name')->order('id asc')->select();
            $data['educationList'] = Db::table('cmf_job_education')->where('status',1)->where('id','neq',-1)->order('list_order asc,id asc')->field('name,id')->select()->toArray();
            $data['experienceList'] = Db::table('cmf_job_experience')->where('status',1)->order('list_order asc,id asc')->field('name,id')->select()->toArray();
        }catch (Exception $e) {
            $this->error('获取失败');
            exit;
        }
        $data['salaryList'] = $this->salaryList;
        $data['orderList'] = $this->orderList;
        $this->success('获取成功',$data);
    }


    /**
     * 企业发布职位提交
     */
    public function add_post(){

        $Position = new Position();
        $User = new User();
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('type_id,experience_id,education_id,name,work_number,contact,phone,salary_lowest,salary_highest,description,lat,lng,address,area_id,welfare','post');

        $result = $this->validate($data, 'job_position');
        if ($result !== true) $this->error($result);

        $data['user_id'] = request()->uid;

        try{
            $userInfo = $User->where('id',$data['user_id'])->field('real_name as name,mobile')->find();
            if(@empty($data['phone'])) $data['phone'] = $userInfo['mobile'];
            if(@empty($data['name'])) $data['contact'] = $userInfo['name'];
        }catch(Exception $e){}

        $data['company_id'] = request()->user_relation['company_id'];
        $data['create_time'] = $data['update_time'] = time();
        $add = $Position->insertGetId($data);
        $add?$this->success('发布成功',['id'=>$add]):$this->error('发布失败');
    }

    //企业编辑职位页面
    public function edit($id = 0,Position $Position){
        try{
            $data = $Position->field('id,user_id,company_id,type_id,experience_id,education_id,area_id,name,work_number,contact,phone,salary_lowest,salary_highest,description,lat,lng,address,delete_time')->find($id);
        }catch (Exception $e) {
            $this->error('获取失败');
            exit;
        }
        if($data['delete_time'] < 0 ) $this->error('您的职位被管理员删除了！');
        if($data['delete_time'] > 0 ) $this->error('您的职位已经删除了！');
        $this->success('获取成功',$data);
    }

    //企业编辑职位提交
    public function edit_post(){
        $Position = new Position();
        $User = new User();
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,type_id,experience_id,education_id,name,work_number,contact,phone,salary_lowest,salary_highest,description,lat,lng,address,area_id,welfare','post');

        $result = $this->validate($data, 'job_position');
        if ($result !== true) $this->error($result);

        $data['user_id'] = request()->uid;

        try{
            $userInfo = $User->where('id',$data['user_id'])->field('real_name as name,mobile')->find();
            if(@empty($data['phone'])) $data['phone'] = $userInfo['mobile'];
            if(@empty($data['name'])) $data['contact'] = $userInfo['name'];
        }catch(Exception $e){}

        $data['company_id'] = request()->user_relation['company_id'];
        $data['update_time'] = time();
        $data['status'] = 0;

        $save = $Position->save($data,['id'=>$data['id']]);
        $save?$this->success('编辑成功'):$this->error('编辑失败');
    }

    /**
     * 企业删除职位
     * @throws \think\Exception
     */
    public  function delete(){
        $Position = new Position();
        $id = $this->request->param('id',0,'intval');
        $delete = $Position->save(['delete_time'=>time()],['id'=>$id]);
        if($delete) Db::table('cmf_company')->where('id',request()->user_relation['company_id'])->setInc('position_count');
        $delete?$this->success('删除成功'):$this->error('删除失败');
    }

    //全部岗位列表
    public function position_list($page = 0,$limit = 10,$keyword = '',$type_id = 0,$education_id = '',$experience_id = '',$area_id = 0,$order_id = 1,$salary_id = 0,Position $Position){
        /*$education_ids = json_decode($education_ids,true);*/
        /*$experience_ids = json_decode($experience_ids,true);*/
        $lat = $this->request->get('lat');
        $lng = $this->request->get('lng');
        $juli = $this->request->get('juli');
        $where = ['c.status'=>1,'c.delete_time'=>0];
        $whereString = 'p.delete_time=0 AND p.status=1';

        $Sql = $Position
        ->alias('p')
        ->join('cmf_company c','c.id = p.company_id')
        ->join('cmf_job_type industry','industry.id = c.type_id','left')
        ->join('cmf_job_type t','t.id = p.type_id','left')
        ->join('cmf_job_experience e','e.id = p.experience_id','left')
        ->join('cmf_job_education education','education.id = p.education_id','left')
        ->join('cmf_area area','area.id = p.area_id','left')
        ->group('p.id');

        //排序方式
        if($order_id == 1){
            $Sql = $Sql->order('p.create_time desc,p.id desc');
        }elseif($order_id == 2){
            $Sql = $Sql->order('p.sign_count desc,p.id desc');
        }

        $field = 'p.*,area.name as area_name,education.name as education_name,e.name as experience_name,t.name as type_name,industry.name as industry_name,c.company as company_name,c.logo';

        //是否为推荐职位
        if($is_recommend = $this->request->get('is_recommend')){
            $where['p.is_recommend'] = 1;
        }

        //关键词
        if($keyword){
            $where['p.name|c.company|t.name|p.address'] = ['like','%'.$keyword.'%'];
        }

        //薪资范畴
        if($salary_id){
            $salary_lowest = $this->salaryList[$salary_id]['salary_lowest'];
            $salary_highest = $this->salaryList[$salary_id]['salary_highest'];
            if($salary_highest == 0){
                $where['p.salary_lowest|p.salary_highest'] = ['egt',$salary_lowest];
            }else{
                $where['p.salary_lowest|p.salary_highest'] = ['between',[$salary_lowest,$salary_highest]];
            }

        }

        //工作类型
        if($type_id){
            $where['p.type_id'] = $type_id;
        }

        //经验筛选
        /*if($experience_ids){
            $where['p.experience_id'] = ['in',$experience_ids];
        }*/
        if($experience_id){
            $where['p.experience_id'] = $experience_id;
        }

        //教育经历
        /*if($education_ids){
            $where['p.education_id'] = ['in',$education_ids];
        }*/
        if($education_id && $education_id != -2){
            $where['p.education_id'] = $education_id;
        }

        //区域筛选
        if($area_id && substr($area_id,-2) == '00'){
            $whereString .= ' AND LEFT(p.area_id,4) = '.substr($area_id,0,4);
        }elseif($area_id){
            $where['p.area_id'] = $area_id;
        }

        //距离之内
        if($lat && $lng && $juli){
            $whereString .= ' AND ROUND(6378.138*2*ASIN(SQRT(POW(SIN(('.$lat.'*PI()/180 - p.lat*PI()/180)/2),2)+COS('.$lat.'*PI()/180)*COS(p.lat*PI()/180)*POW(SIN(('.$lng.'*PI()/180-p.lng*PI()/180)/2),2)))*1000) < '.$juli;
         }
        try{
            $data['list'] = $Sql->field($field)->where($where)->where($whereString)->limit(($page*$limit),$limit)->select();
            $data['count'] = Db::table('cmf_job_position')
                ->alias('p')
                ->join('cmf_company c','c.id = p.company_id')
                ->join('cmf_job_type industry','industry.id = c.type_id','left')
                ->join('cmf_job_type t','t.id = p.type_id','left')
                ->where($where)->where($whereString)->count();
        }catch (Exception $e) {
            return $e->getMessage();
        }
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
        exit;
    }

    private $PositionTable = 'job_position';

    //职位详情
    public function position_info(Position $Position,$id = 0){
        $user_id = request()->uid;
        try{
            $info = $Position->alias('p')
                ->join('cmf_company c','p.company_id = c.id','left')
                ->join('cmf_area area','area.id = p.area_id','left')
                ->join('cmf_job_type industry','industry.id = c.type_id','left')
                ->join('cmf_job_type t','t.id = p.type_id','left')
                ->join('cmf_job_experience e','e.id = p.experience_id','left')
                ->join('cmf_job_education education','education.id = p.education_id','left')
                ->field('p.*,area.name as area_name,education.name as education_name,e.name as experience_name,t.name as type_name,industry.name as industry_name,c.company as company_name,c.logo as company_logo')
                ->where('p.id',$id)->find();
            if(Db::table('cmf_job_order_post')->where(['position_id'=>$id,'delete_time'=>0,'user_id'=>$user_id])->count() > 0){
                $info['is_post'] = 1;
            }else{
                $info['is_post'] = 0;
            }
            if(@$info['company_logo']) $info['company_logo'] = cmf_get_image_preview_url($info['company_logo']);
            $info['is_like'] = Db::table('cmf_user_favorite')->where(['user_id'=>$user_id,'table_name'=>$this->PositionTable,'object_id'=>$info['id']])->value('count(id)');
        }catch (Exception $e){
            return $e->getMessage();
        }

        if($info['delete_time'] != 0) $this->error('该职位已被删除！');
        if($info['status'] != 1) $this->error('该职位尚未通过审核！');

        $this->success('获取成功',$info);
        exit;
    }

    //发布的职位（学生查看）
    public function company_position(Position $Position,$company_id = 0,$page = 0,$limit = 10){

        $Sql = $Position
            ->alias('p')
            ->join('cmf_company c','c.id = p.company_id')
            ->join('cmf_area area','area.id = p.area_id','left')
            ->join('cmf_job_type industry','industry.id = c.type_id','left')
            ->join('cmf_job_type t','t.id = p.type_id','left')
            ->join('cmf_job_experience e','e.id = p.experience_id','left')
            ->join('cmf_job_education education','education.id = p.education_id','left')
            ->where(['p.delete_time'=>0,'p.status'=>1,'company_id'=>$company_id])
            ->group('p.id')
            ->limit(((int) $page * (int) $limit), $limit);

        $field = 'p.*,education.name as education_name,e.name as experience_name,t.name as type_name,industry.name as industry_name,c.company as company_name,c.logo,area.name as area_name';

        try{
            $data['list'] = $Sql->field($field)->select();
            $data['count'] = $Position->where(['delete_time'=>0,'company_id'=>$company_id])->count();
        }catch (Exception $e) {
            return $e->getMessage();
        }
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
        exit;
    }

    //发布的职位（公司内部）
    public function publish_position(Position $Position,$page = 0,$limit = 10){
        $company_id = request()->user_relation['company_id'];
        $Sql = $Position->alias('p')
            ->join('cmf_company c','c.id = p.company_id')
            ->join('cmf_job_type industry','industry.id = c.type_id','left')
            ->join('cmf_job_type t','t.id = p.type_id','left')
            ->join('cmf_job_experience e','e.id = p.experience_id','left')
            ->join('cmf_job_education education','education.id = p.education_id','left')
            ->where(['p.delete_time'=>0,'p.company_id'=>$company_id])
            ->group('p.id')
            ->limit(((int) $page * (int) $limit), $limit);

        $field = [
            'p.*',
            'IF(p.delete_time<0,"管理员删除了","正常") as admin_delete',
            'education.name as education_name',
            'e.name as experience_name',
            't.name as type_name',
            'industry.name as industry_name',
            'c.company as company_name',
            'c.logo'
        ];

        try{
            $data['list'] = $Sql->field($field)->select()->append(['status_name']);
            $data['count'] = $Position->where(['delete_time'=>0,'company_id'=>$company_id])->count();
        }catch (Exception $e) {
            return $e->getMessage();
        }
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
        exit;
    }
}