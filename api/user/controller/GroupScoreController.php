<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\user\controller;

use think\Db;
use api\common\controller\BaseController;
use api\user\model\UserGroupScoreLog;

class GroupScoreController extends BaseController
{
    /**
     * 积分日志
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function log(){
        $UserScoreLog = new UserGroupScoreLog();
        $page = $this->request->param('page', 0, 'intval');
        $limit = $this->request->param('limit', 10, 'intval');
        $user_id = request()->uid;

        $where = ['user_id'=>$user_id];

        $data['list']  = $UserScoreLog
            ->where($where)
            ->order('create_time desc')
            ->limit(($page*$limit),$limit)
            ->select()->toArray();
        $data['count'] = $UserScoreLog->where($where)->count();
        $data['page'] = $page;
        $data['limit'] = $limit;

        $this->success('获取成功',$data);
    }
}