<?php
namespace api\index\validate;
use	think\Validate;
class Login extends Validate {				
    protected $rule =	[
      'mobile|手机号' => 'require|regex:^1{1}[3-9]{1}[0-9]{9}',
      'code|验证码' => 'require|min:4'
    ];
  	
  	protected $message = [
        'mobile.regex'     =>  '手机号不正确'
    ];
    // 场景
    protected $scene = [
        // 提出需求
        'login'		     =>  [
          'mobile',
          'code'
        ],
    ];		
}