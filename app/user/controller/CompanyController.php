<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Powerless < wzxaini9@gmail.com>
// +----------------------------------------------------------------------
namespace app\user\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use think\db\Query;

class CompanyController extends AdminBaseController
{


    public $statusArray = array(
        0 => '禁用',
        1 => '启用',
    );

    public $statusClass = array(
        0 => 'danger',
        1 => 'success',
    );
    public $recommendArray = array(
        0 => '不推荐',
        1 => '&nbsp;推 荐&nbsp;',
    );

    public $recommendClass = array(
        0 => 'danger',
        1 => 'success',
    );

    /**
     * 企业列表
     * @adminMenu(
     *     'name'   => '企业列表',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '企业列表',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        $keyword = $this->request->param('keyword','','trim');
        $where['delete_time'] = 0;
        if($keyword) $where['company|contact_name|contact_phone'] = ['like','%'.$keyword.'%'];
        try{
            $list = Db::name('company')->where($where)->order("create_time DESC")
                ->paginate(10,false,['query'=>['keyword'=>$keyword]]);
        }catch (\Exception $e){
            return $e;
        }
        // 获取分页显示
        $page = $list->render();
        $this->assign('list', $list);
        $this->assign('page', $page);
        $this->assign('keyword', $keyword);
        $this->assign('statusArray',$this->statusArray);
        $this->assign('statusClass',$this->statusClass);
        $this->assign('recommendArray',$this->recommendArray);
        $this->assign('recommendClass',$this->recommendClass);
        return $this->fetch();
    }

    public $table_name = 'company';

    //企业启用/禁用
    public function status_change(){
        $data = $this->request->param();

        try{
            $info = Db::table('cmf_company')->find($data['id']);
        }catch (\Exception $e){
            return $e->getMessage();
        }
        if($info['status'] == $data['status']) $this->error('相同操作');

        Db::startTrans();
        try{
            $save = Db::table('cmf_company')->where(['id'=>$data['id']])->update($data);

            if($data['status'] == 1){//不同状态处理
                $message = '您的公司“'.$info['company'].'”被启用了！';
            }else{
                $message = '对不起，您的公司“'.$info['company'].'”被禁用了，详情请联系管理员';
            }

            if($info['user_id']){
                send_message(
                    session('ADMIN_ID'),//发送者ID
                    $info['user_id'],//接收者ID
                    $this->table_name,//对象表
                    $info['id'],//对象ID
                    '您的公司状态被改变了！',//信息标题
                    $message,//信息内容
                    config('HR_PARK_MSG'),//redis前缀
                    ''//路径
                );//消息提醒
            }

            Db::commit();// 提交事务
        } catch (\Exception $e) {
            Db::rollback();// 回滚事务
            return $e->getMessage();
        }

        if($save){
            //wxSendMsg($message)//发送通知
            $this->success('更改成功');
        }else{
            $this->error('更改失败');
        }
        exit;
    }

    //企业推荐/不推荐
    public function recommend_change(){
        $data = $this->request->param();

        try{
            $info = Db::table('cmf_company')->find($data['id']);
        }catch (\Exception $e){
            return $e->getMessage();
        }
        if($info['is_recommend'] == $data['is_recommend']) $this->error('相同操作');

        Db::startTrans();
        try{
            $save = Db::table('cmf_company')->where(['id'=>$data['id']])->update($data);
            Db::commit();// 提交事务
        } catch (\Exception $e) {
            Db::rollback();// 回滚事务
            return $e->getMessage();
        }

        if($save){
            //wxSendMsg($message)//发送通知
            $this->success('更改成功');
        }else{
            $this->error('更改失败');
        }
        exit;
    }

    //企业删除
    public function delete(){
        $id = $this->request->param('id',0,'intval');

        try{
            $info = Db::table('cmf_company')->find($id);
        }catch (\Exception $e){
            return $e->getMessage();
        }

        Db::startTrans();
        try{
            $save = Db::table('cmf_company')->where(['id'=>$id])->update(['delete_time'=>-time()]);

            if($info['user_id']){
                send_message(
                    session('ADMIN_ID'),//发送者ID
                    $info['user_id'],//接收者ID
                    $this->table_name,//对象表
                    $info['id'],//对象ID
                    '您的企业被删除了！',//信息标题
                    '详情请联系管理员！',//信息内容
                    config('HR_PARK_MSG'),//redis前缀
                    ''//路径
                );//消息提醒
            }

            Db::commit();// 提交事务
        } catch (\Exception $e) {
            Db::rollback();// 回滚事务
            return $e->getMessage();
        }

        if($save){
            //wxSendMsg($message)//发送通知
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
        exit;
    }

    /**
     * 添加企业
     * @adminMenu(
     *     'name'   => '添加企业',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加企业',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function add(){
        $typeList = Db::table('cmf_job_type')->where('Left(id,2) = 00')->where('id != 0')->field('id,name')->order('id asc')->select();
        $this->assign('typeList',$typeList);
        return $this->fetch();
    }

    /**
     * 添加企业提交
     * @adminMenu(
     *     'name'   => '添加企业提交',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加企业提交',
     *     'param'  => ''
     * )
     */
    public function add_post(){
        if (!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->post();

        $result = $this->validate($data, 'Company');
        if ($result !== true) {
            $this->error($result);
        }
        $data['create_time'] = time();

        $add = Db::table('cmf_company')->insert($data);

        $add?$this->success('添加成功!', url('index')):$this->error('添加失败!');
    }

    /**
     * 企业修改
     * @adminMenu(
     *     'name'   => '企业修改',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '企业修改',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function edit(){
        $id = $this->request->param('id',0,'intval');
        $info = Db::table('cmf_company')->alias('c')
            ->join('cmf_area a','a.id = c.area_id','left')
            ->field('c.*,a.name as area')
            ->find($id);
        $typeList = Db::table('cmf_job_type')->where('Left(id,2) = 00')->where('id != 0')->field('id,name')->order('id asc')->select();
        $this->assign('info',$info);
        $this->assign('typeList',$typeList);
        return $this->fetch();
    }

    /**
     * 企业修改提交
     * @adminMenu(
     *     'name'   => '企业修改提交',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '企业修改提交',
     *     'param'  => ''
     * )
     * @throws \think\Exception
     */
    public function edit_post(){
        if (!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->post();

        $result = $this->validate($data, 'Company');
        if ($result !== true) {
            $this->error($result);
        }

        $save = Db::table('cmf_company')->where('id',$data['id'])->update($data);

        $save?$this->success('保存成功!'):$this->error('保存失败!');
    }
}
