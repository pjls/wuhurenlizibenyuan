<?php
// +----------------------------------------------------------------------
// | 文件说明：用户-幻灯片
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: wuwu <15093565100@163.com>
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Date: 2017-5-25
// +----------------------------------------------------------------------

namespace api\activity\model;

use think\Model;

class ActivityModel extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'cmf_activity';

    //预编译缩略图
    public function getThumbnailAttr($value){
        return  cmf_get_image_preview_url($value);
    }

    //预编译缩略图
    public function getCutDateAttr($value){
        return  date('Y-m-d',$value);
    }

    //预编译更新时间
    /*public function getUpdateTimeAttr($value){
        return  date('Y-m-d H:i',$value);
    }*/

    //预编译创建时间
    /*public function getCreateTimeAttr($value){
        return  date('Y-m-d H:i',$value);
    }*/
}

