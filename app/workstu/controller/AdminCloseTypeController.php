<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls
// +----------------------------------------------------------------------
namespace app\workstu\controller;

use think\Db;
use cmf\controller\AdminBaseController;
use app\workstu\model\CloseTypeModel;

class AdminCloseTypeController extends AdminBaseController
{
    /**
     * 结算方式管理
     * @adminMenu(
     *     'name'   => '结算方式管理',
     *     'parent' => '',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => 'file',
     *     'remark' => '结算方式管理',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        $CloseTypeModel  =  new CloseTypeModel;
        try{
            $list = $CloseTypeModel->paginate();
        }catch (\Exception $e) {
            return $e;
        }

        $this->assign("arrStatus", $CloseTypeModel::$STATUS);
        $this->assign("list", $list);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    /**
     * 添加结算方式
     * @adminMenu(
     *     'name'   => '添加结算方式',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加结算方式',
     *     'param'  => ''
     * )
     */
    public function add()
    {
        $CloseTypeModel  =  new CloseTypeModel;
        $this->assign("arrStatus", $CloseTypeModel::$STATUS);
        return $this->fetch();
    }

    /**
     * 添加结算方式提交
     * @adminMenu(
     *     'name'   => '添加结算方式提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加结算方式提交',
     *     'param'  => ''
     * )
     */
    public function addPost()
    {
        $CloseTypeModel  =  new CloseTypeModel;

        $arrData = $this->request->param();
        $CloseTypeModel->isUpdate(false)->allowField(true)->save($arrData);

        $this->success('添加成功');

    }

    /**
     * 更新结算方式状态
     * @adminMenu(
     *     'name'   => '更新结算方式状态',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '更新结算方式状态',
     *     'param'  => ''
     * )
     */
    public function upStatus()
    {
        $CloseTypeModel  =  new CloseTypeModel;
        $intId     = $this->request->param("id");
        $intStatus = $this->request->param("status");
        $intStatus = $intStatus ? 1 : 0;
        if (empty($intId)) {
            $this->error('没有ID');
        }
        $CloseTypeModel->isUpdate(true)->save(["status" => $intStatus], ["id" => $intId]);

        $this->success('操作成功');

    }

}