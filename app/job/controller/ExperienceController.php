<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls
// +----------------------------------------------------------------------
namespace app\job\controller;

use cmf\controller\AdminBaseController;
use app\job\model\Experience;

class ExperienceController extends AdminBaseController
{
    /**
     * 工作经验管理
     * @adminMenu(
     *     'name'   => '工作经验管理',
     *     'parent' => '',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => 'file',
     *     'remark' => '工作经验管理',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        $Experience  =  new Experience;
        try{
            $list = $Experience->paginate();
        }catch (\Exception $e) {
            return $e;
        }

        $this->assign("arrStatus", $Experience::$STATUS);
        $this->assign("list", $list);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    /**
     * 添加工作经验
     * @adminMenu(
     *     'name'   => '添加工作经验',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加工作经验',
     *     'param'  => ''
     * )
     */
    public function add()
    {
        $Experience  =  new Experience;
        $this->assign("arrStatus", $Experience::$STATUS);
        return $this->fetch();
    }

    /**
     * 添加工作经验提交
     * @adminMenu(
     *     'name'   => '添加工作经验提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加工作经验提交',
     *     'param'  => ''
     * )
     */
    public function addPost()
    {
        $Experience  =  new Experience;

        $arrData = $this->request->param();
        $Experience->isUpdate(false)->allowField(true)->save($arrData);

        $this->success('添加成功');

    }

    /**
     * 更新工作经验状态
     * @adminMenu(
     *     'name'   => '更新工作经验状态',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '更新工作经验状态',
     *     'param'  => ''
     * )
     */
    public function upStatus()
    {
        $Experience  =  new Experience;
        $intId     = $this->request->param("id");
        $intStatus = $this->request->param("status");
        $intStatus = $intStatus ? 1 : 0;
        if (empty($intId)) {
            $this->error('没有ID');
        }
        $Experience->isUpdate(true)->save(["status" => $intStatus], ["id" => $intId]);

        $this->success('操作成功');

    }

}