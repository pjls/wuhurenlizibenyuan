<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\index\controller;

use think\Db;
use think\Validate;
use api\common\controller\BaseController;
use api\index\model\SlideItemModel;

class SlideController extends BaseController
{
    public function slide_item(SlideItemModel $SlideItemModel,$code = ''){
        $join = [
            ['cmf_slide s', 'i.slide_id = s.id']
        ];
        $where['i.status'] = 1;
        $where['s.delete_time'] = 0;
        if(!empty($code)){
            $where['s.code'] = $code;
        }else{
            $this->error('code不能为空');
        }
        try{
            $slideList = $SlideItemModel->alias('i')
                ->join($join)->where($where)->select()->toArray();
        }catch (\Exception $e) {}
        isset($slideList)?$this->success('访问成功',$slideList):$this->success('访问失败');

    }
}
