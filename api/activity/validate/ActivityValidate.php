<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace api\activity\validate;

use think\Validate;

class ActivityValidate extends Validate
{
    protected $rule = [
        'title' => 'require',
        'organizer' => 'require',
        'begin_time' => 'require',
        'end_time' => 'require',
        'cut_date' => 'require',
        'playing_time' => 'require',
        'address' => 'require',
        'lat' => 'require',
        'lng' => 'require',
    ];
    protected $message = [
        'title.require' => '请填写活动名称！',
        'organizer.require' => '请填写主办单位！',
        'begin_time.require' => '请选择活动开始日期！',
        'end_time.require' => '请选择活动结束日期！',
        'cut_date.require' => '请选择报名截止日期！',
        'playing_time.require' => '请填写活动的时间！',
        'address.require' => '请选择活动地点！',
        'lat.require' => '请重新选择活动地点！',
        'lng.require' => '请重新选择活动地点！',
    ];

    protected $scene = [
//        'add'  => ['user_login,user_pass,user_email'],
//        'edit' => ['user_login,user_email'],
    ];
}