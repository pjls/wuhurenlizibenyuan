<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\activity\validate;

use think\Validate;

class ActivityValidate extends Validate
{
    protected $rule = [
        'title' => 'require',
        'organizer' => 'require',
        'begin_time' => 'require',
        'address' => 'require',
        'end_time' => 'require',
    ];
    protected $message = [
        'title.require' => '请填写活动名称！',
        'organizer.require' => '请填写主办单位！',
        'begin_time.require' => '请选择活动时间！',
        'address.require' => '请选择活动地点！',
        'end_time.require' => '请选择截止时间！',
    ];

    protected $scene = [
//        'add'  => ['user_login,user_pass,user_email'],
//        'edit' => ['user_login,user_email'],
    ];
}