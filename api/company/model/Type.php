<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls < 1512273960@qq.com>
// +----------------------------------------------------------------------
namespace api\company\model;

use think\Model;

class Type extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'cmf_job_type';

    public function typeTree()
    {
        return $this->hasMany('Type','LEFT(LPAD(id,4,0),2)','LEFT(LPAD(id,4,0),2)');
    }

}