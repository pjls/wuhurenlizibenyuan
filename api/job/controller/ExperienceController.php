<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\job\controller;

use think\Db;
use api\common\controller\BaseController;
use api\job\model\ResumeExperience;
use api\job\model\Resume;


class ExperienceController extends BaseController
{

    /**
     * 工作经验添加提交2
     */
    public function add_post(){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('company_name,position,in_time,out_time,description,ban_company','post');
        $result = $this->validate($data, 'resume_experience');
        if ($result !== true) $this->error($result);

        $user_id  = request()->uid;
        $Resume = new Resume();

        if(!$resume_id = $Resume->where('user_id',$user_id)->value('id')){
            $resume_id = $Resume->insertGetId(['user_id'=>$user_id,'create_time'=>time(),'update_time'=>time()]);
        }

        $data['resume_id'] = $resume_id;
        $data['user_id'] = $user_id;
        $ResumeExperience = new ResumeExperience();
        $id = $ResumeExperience->insertGetId($data);
        if($id) $Resume->where('id',$resume_id)->update(['update_time'=>time()]);//简历修改时间更新
        $id?$this->success('添加成功',['id'=>$id]):$this->error('添加失败');
    }

    /**
     * 工作经验修改提交2
     */
    public function edit_post(){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,company_name,position,in_time,out_time,description,ban_company','post');
        $result = $this->validate($data, 'resume_experience');
        if ($result !== true) $this->error($result);

        $ResumeExperience = new ResumeExperience();
        $user_id  = request()->uid;
        if($user_id != $ResumeExperience->where('id',$data['id'])->value('user_id')){
            $this->error('不是您的简历！');
        }

        $save = $ResumeExperience->save($data,['id'=>$data['id']]);

        //简历修改时间
        if($save){
            $Resume = new Resume();
            $resume_id = $ResumeExperience->where('id',$data['id'])->value('id');
            $Resume->where('id',$resume_id)->update(['update_time'=>time()]);
        }
        $save?$this->success('修改成功'):$this->error('修改失败');
    }

    /**
     * 工作经验详情获取
     * @throws \think\Exception
     */
    public function info(){
        $id = $this->request->param('id');
        $ResumeExperience = new ResumeExperience();
        $info = $ResumeExperience::get($id);
        $info?$this->success('获取成功',$info):$this->error('获取失败');
    }

    /**
     * 工作经验删除
     */
    public function delete(){
        $id = $this->request->param('id');
        $ResumeExperience = new ResumeExperience();
        $user_id  = request()->uid;
        if($user_id != $ResumeExperience->where('id',$id)->value('user_id')){
            $this->error('不是您的简历！');
        }

        $delete = $ResumeExperience->where('id',$id)->delete();

        //简历修改时间
        if($delete){
            $Resume = new Resume();
            $resume_id = $ResumeExperience->where('id',$id)->value('id');
            $Resume->where('id',$resume_id)->update(['update_time'=>time()]);
        }
        $delete?$this->success('删除成功'):$this->error('删除失败');
    }

}