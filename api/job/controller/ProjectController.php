<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\job\controller;

use think\Db;
use api\common\controller\BaseController;
use api\job\model\ResumeProject;
use api\job\model\Resume;


class ProjectController extends BaseController
{
    /**
     * 项目经验添加提交2
     */
    public function add_post(){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('title,duty,url,begin_time,end_time,description','post');
        $result = $this->validate($data, 'resume_project');
        if ($result !== true) $this->error($result);

        $user_id  = request()->uid;
        $Resume = new Resume();

        if(!$resume_id = $Resume->where('user_id',$user_id)->value('id')){
            $resume_id = $Resume->insertGetId(['user_id'=>$user_id,'create_time'=>time(),'update_time'=>time()]);
        }

        $data['resume_id'] = $resume_id;
        $data['user_id'] = $user_id;
        $ResumeProject = new ResumeProject();
        $id = $ResumeProject->insertGetId($data);
        if($id) $Resume->where('id',$resume_id)->update(['update_time'=>time()]);//简历修改时间更新
        $id?$this->success('添加成功',['id'=>$id]):$this->error('添加失败');
    }

    /**
     * 项目经验修改提交2
     */
    public function edit_post(){
        if(!$this->request->isPost()) $this->error('访问失败');
        $data = $this->request->only('id,title,duty,url,begin_time,end_time,description','post');
        $result = $this->validate($data, 'resume_project');
        if ($result !== true) $this->error($result);

        $ResumeProject = new ResumeProject();
        $user_id  = request()->uid;
        if($user_id != $ResumeProject->where('id',$data['id'])->value('user_id')){
            $this->error('不是您的项目经验！');
        }

        $save = $ResumeProject->save($data,['id'=>$data['id']]);

        //简历修改时间
        if($save){
            $Resume = new Resume();
            $resume_id = $ResumeProject->where('id',$data['id'])->value('id');
            $Resume->where('id',$resume_id)->update(['update_time'=>time()]);
        }

        $save?$this->success('修改成功'):$this->error('修改失败');
    }

    /**
     * 项目经验详情获取
     * @throws \think\Exception
     */
    public function info(){
        $id = $this->request->param('id');
        $ResumeProject = new ResumeProject();
        $info = $ResumeProject::get($id);
        $info?$this->success('获取成功',$info):$this->error('获取失败');
    }

    /**
     * 项目经验删除
     */
    public function delete(){
        $id = $this->request->param('id');
        $ResumeProject = new ResumeProject();
        $user_id  = request()->uid;
        if($user_id != $ResumeProject->where('id',$id)->value('user_id')){
            $this->error('不是您的简历！');
        }

        $delete = $ResumeProject->where('id',$id)->delete();

        //简历修改时间
        if($delete){
            $Resume = new Resume();
            $resume_id = $ResumeProject->where('id',$id)->value('id');
            $Resume->where('id',$resume_id)->update(['update_time'=>time()]);
        }
        $delete?$this->success('删除成功'):$this->error('删除失败');
    }

}