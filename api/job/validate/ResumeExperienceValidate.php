<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace api\job\validate;

use think\Validate;

class ResumeExperienceValidate extends Validate
{
    protected $rule = [
        'company_name'  => 'require',
        'position'  => 'require',
        'in_time'  => 'require',
        'out_time'  => 'require',
    ];

    protected $message = [
        'company_name.require'  =>  '请填写公司名称',
        'position.require'  =>  '请填写职位名称',
        'in_time.require'  =>  '请选择开始时间',
        'out_time.require'  =>  '请选择结束时间',
    ];

    // protected $scene = [
    // ];

    // 自定义验证规则
    protected function checkPhone($value)
    {
        if(!preg_match('/^1\d{10}$/',$value)){
            return '联系电话格式错误';
        }else{
            return true;
        }
    }
}