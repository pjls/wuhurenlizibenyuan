<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\user\controller;

use think\Db;
use api\common\controller\BaseController;
use api\user\model\UserOrder;

class OrderController extends BaseController
{
    /**
     * 工单列表
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function index(){
        $UserOrder = new UserOrder();
        $page = $this->request->param('page', 0, 'intval');
        $limit = $this->request->param('limit', 10, 'intval');
        $status = $this->request->param('status');
        $user_id = request()->uid;

        $where = ['user_id'=>$user_id];
        if($status === "0"){
            $where['status'] = 0;
        }elseif($status === '1'){
            $where['status'] = ['neq',0];
        }

        $data['list']  = $UserOrder->alias('o')
            ->where($where)
            ->order('apply_time desc')
            ->limit(($page*$limit),$limit)
            ->field('o.id,o.object_id,o.title,o.status,o.apply_time')
            ->select()->toArray();
        $data['count'] = $UserOrder->where($where)->count();
        $data['page'] = $page;
        $data['limit'] = $limit;

        $this->success('获取成功',$data);
    }

    /**
     * 撤回工单
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function delete(){
        $id = $this->request->param('id', 0, 'intval');
        $user_id = request()->uid;
        $order = Db::table('cmf_user_order')->where('id',$id)->field('user_id,object_id,table_name')->find();
        if($user_id != $order['user_id']) $this->error('不是您的工单！');
        if($order['status'] > 0) $this->error('只能删除未处理的工单！');

        Db::startTrans();
        try{
            Db::table('cmf_user_order')->delete($id);
            //记录报名数
            Db::table($order['table_name'])->delete($order['object_id']);
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->error('撤销失败');
            exit;
        }
        $this->success('撤销成功');
    }

    /**
     * 工单详情
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\Exception
     * @throws \think\Collection
     * @throws false|\PDOStatement|string|\think\Collection
     */
    public function info(){
        $id = $this->request->param('id', 0, 'intval');
        $user_id = request()->uid;
        $order = Db::table('cmf_user_order')->where('id',$id)->field('user_id,object_id,table_name')->find();
        if($user_id != $order['user_id']) $this->error('不是您的工单！');
        $info = Db::table($order['table_name'])->find($order['object_id']);

        /*$statusArray = Db::table('cmf_user_order_status')->column('name','id');
        if($info['step']){
            $info['step'] = json_decode($info['step'],true);
            foreach($info['step'] as $k => $v){
                $info['step'][$k]['status_name'] = $statusArray[$v['status']];
            }
        }*/
        $info?$this->success('获取成功',$info):$this->error('获取失败');
    }
}