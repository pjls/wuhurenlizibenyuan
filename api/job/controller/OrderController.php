<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\job\controller;

use think\Db;
use Exception;
use api\common\controller\BaseController;
use api\job\model\Order;


class OrderController extends BaseController
{
    protected  $PrefixComment;
    protected  $table_name = 'job_order_post';
    protected  $wx_company_order_url = '/pages/job/order_list';
    protected  $wx_user_order_url = '/pages/job/detail/index';

    /**
     * 架构函数
     * @access public
     */
    public function __construct(){
        parent::__construct();
        if(request()->user_relation['role_id'] == 5){
            return json(['code'=>0,'status'=>0,'msg'=>'请您先完成实名认证！','data'=>[],'errcode'=>40001,'errRedirect'=>'/pages/me/verify/index'])->send();
        }elseif(request()->user_relation['role_id'] == 3){
            return json(['code'=>0,'status'=>0,'msg'=>'企业认证者不得投递简历！','data'=>[],'errcode'=>40001,'errRedirect'=>'']);
        }

        //设置系统消息阅读数
        $this->PrefixComment = config('HR_PARK_MSG');
    }

    /**
     * 申请职位
     * @throws \think\Exception
     */
    public function position_post(){
        $Order = new Order();
        if(!$this->request->isPost()) $this->error('访问失败');
        $id = $this->request->post('id');
        $user_id = request()->uid;

        $count = $Order->where(['user_id'=>$user_id,'position_id'=>$id,'delete_time'=>0])->count();
        $position = Db::table('cmf_job_position')->where('id',$id)->field('name,user_id,company_id,phone')->find();
        $resume = Db::table('cmf_job_resume')->where('user_id',$user_id)->find();

        if($count > 0){
            $this->error('您已经申请过了');
        }elseif(empty($resume['position'])){
            return json(['code'=>0,'status'=>0,'msg'=>'请完善您简历的期望职位！','data'=>[],'errcode'=>40002,'errRedirect'=>'/pages/me/resume/expect/index'])->send();
        }elseif(empty($resume['mobile'])||empty($resume['name'])){
            return json(['code'=>0,'status'=>0,'msg'=>'请完善您简历的基础信息！','data'=>[],'errcode'=>40003,'errRedirect'=>'/pages/me/resume/basic/index'])->send();
        }elseif(empty($resume['education_id'])){
            return json(['code'=>0,'status'=>0,'msg'=>'请完善您简历的教育经历！','data'=>[],'errcode'=>40004,'errRedirect'=>'/pages/me/resume/education/index'])->send();
        }

        $data['position_id'] = $id;
        $data['user_id'] = $user_id;
        $data['company_id'] = $position['company_id'];
        $data['resume_id'] = $resume['id'];
        $data['create_time'] = time();
        $data['status'] = 1;
        $to_user_id = $position['user_id'];

        Db::startTrans();
        try{
            Db::table('cmf_job_position')->where('id',$id)->setInc('sign_count');
            $add = Db::table('cmf_job_order_post')->insertGetId($data);

            $real_name = Db::table('cmf_user')->where('id',$user_id)->value('real_name');
            $msg = $real_name.'申请全职岗位“'.$position['name'].'”';
            send_message(
                $user_id,//发送者ID
                $to_user_id,//接收者ID
                $this->table_name,//对象表
                $add,//对象ID
                '有新的申请报名！',//信息标题
                $msg,//信息内容
                config('HR_PARK_MSG'),//redis前缀
                $this->wx_company_order_url.'?id='.$id//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        //$add?$this->success('申请成功',['phone'=>$position['phone']]):$this->error('申请失败');
        $add?$this->success('申请成功'):$this->error('申请失败');
        exit;
    }

    //求职者正在申请中的职位
    public function user_order(Order $Order,$status = 0,$page = 0,$limit = 10){
        $user_id = request()->uid;

        $where['o.user_id'] = $user_id;
        $where['o.delete_time'] = 0;
        if($status == 1){
            $where['o.status'] = ['in','0,1'];//未查看、已查看
        }elseif($status == 2){
            $where['o.status'] = ['in','2,3'];//有意向、不合适
        }
        //$where['p.delete_time'] = 0;

        $field = 'o.*,o.status as check_status,IF(p.delete_time=0,0,1) as position_delete,p.name,p.salary_lowest,p.salary_highest,c.company as company_name,c.logo';

        try{
            $list = $Order->alias('o')
                ->join('cmf_company c','c.id = o.company_id')
                ->join('cmf_job_position p','p.id = o.position_id')
                ->where($where)->limit($page*$limit,$limit)->order('o.create_time desc')->field($field)->select()->append(['status_name']);
            $data['count'] = $Order->alias('o')->where($where)->count();
            $data['list'] = $list;
        }catch (Exception $e){
            return $e->getMessage();
        }
        //$data['statusList'] = $Order::$STATUS;
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
        exit;
    }

    //求职者撤销职位申请
    public function delete(Order $Order,$id = 0){
        $user_id = request()->uid;
        $delete = $Order->save(['delete_time'=>time()],['id'=>$id,'user_id'=>$user_id]);
        $delete?$this->success('撤销成功'):$this->error('撤销失败');
    }

    //职位申请的详情
    /*public function order_info(Order $Order,$id = 0){
        try{
            $data['info'] = $Order->alias('o')
                ->join('cmf_work_student_position p','p.id = o.position_id')
                ->join('cmf_work_student_order_comment c','o.id = c.order_id')
                ->field('o.id,o.position_id,o.user_id,o.company_id,o.resume_id,c.company_grade,c.company_evaluation,c.company_comment,o.complete_money,o.check_status,o.order_status,FROM_UNIXTIME(o.create_time,"%Y-%m-%d %H:%i:%s") as create_time,p.name as position_name')
                ->where('o.id',$id)->find();
        }catch (Exception $e){
            $this->error('系统错误');
            exit;
        }
        $this->success('获取成功',$data);
    }*/

    //公司所有申请列表
    public function company_order(Order $Order,$status = 0,$page = 0,$limit = 10){
        $company_id = request()->user_relation['company_id'];

        $where['o.company_id'] = $company_id;
        $where['o.delete_time'] = 0;
        if($status == 1){
            $where['o.status'] = ['in','1,2'];//未查看、已查看
        }elseif($status == 2){
            $where['o.status'] = ['in','3,4'];//有意向、不合适
        }
        //$where['p.delete_time'] = 0;

        $field = [
            'o.*',
            'IF(p.delete_time=0,0,1) as position_delete',
            'p.name as position_name',
            'p.salary_lowest',
            'p.salary_highest',
            'r.avatar',
            'r.name',
            'e.name as education_name',
            '(TIMESTAMPDIFF(YEAR,r.work_date,CURDATE())) as experience_year'
        ];

        try{
            $list = $Order->alias('o')
                ->join('cmf_job_resume r','r.id = o.resume_id')
                ->join('cmf_job_education e','r.education_id = e.id','left')
                ->join('cmf_job_position p','p.id = o.position_id')
                ->where($where)->limit($page*$limit,$limit)->order('o.status asc,o.create_time desc')->field($field)->select();
            $data['count'] = $Order->alias('o')->where($where)->count();
        }catch (Exception $e){
            return $e->getMessage();
        }

        $data['list'] = $list;
        $data['page'] = $page;
        $data['limit'] = $limit;
        $list?$this->success('获取成功',$data):$this->error('获取失败');
        exit;
    }

    /*//公司未操作的申请列表
    public function company_uncheck_order(Order $Order,$page = 0,$limit = 10){
        //$user_id = request()->uid;
        $company_id = request()->user_relation['company_id'];

        //提示信息条数被阅读
        $Order->save(['company_tip'=>0,'delete_time'=>0],['company_id'=>$company_id]);

        $where['o.company_id'] = $company_id;
        $where['o.delete_time'] = 0;
        $where['o.check_status'] = ['in','0,1'];
        //$where['o.order_status'] = 0;
        $where['p.delete_time'] = 0;

        $field = 'o.id,o.position_id,o.user_id,o.check_status,o.order_status,p.name as position_name,FROM_UNIXTIME(o.create_time,"%Y-%m-%d %H:%i:%s") as create_time,p.salary,IF(p.salary_method=1,"小时","天") as salary_method,compute.name as compute_name,u.real_name,u.user_nickname,u.avatar';

        $list = $Order->get_order($where,$field,$page,$limit);
        try{
            unset($where['p.delete_time']);
            $data['count'] = $Order->alias('o')->where($where)->count();
        }catch (Exception $e){}
        $data['list'] = $list;
        $data['page'] = $page;
        $data['limit'] = $limit;
        $list?$this->success('获取成功',$data):$this->error('获取失败');
    }

    //公司已操作的申请列表
    public function company_check_order(Order $Order,$page = 0,$limit = 10){
        //$user_id = request()->uid;
        $company_id = request()->user_relation['company_id'];

        //提示信息条数被阅读
        $Order->save(['company_tip'=>0,'delete_time'=>0],['company_id'=>$company_id]);

        $where['o.company_id'] = $company_id;
        $where['o.delete_time'] = 0;
        $where['o.check_status'] = ['not in','0,1'];
        //$where['o.order_status'] = 0;
        $where['p.delete_time'] = 0;

        $field = 'o.id,o.position_id,o.user_id,o.check_status,o.order_status,p.name as position_name,FROM_UNIXTIME(o.create_time,"%Y-%m-%d %H:%i:%s") as create_time,p.salary,IF(p.salary_method=1,"小时","天") as salary_method,compute.name as compute_name,u.real_name,u.user_nickname,u.avatar';

        $list = $Order->get_order($where,$field,$page,$limit);
        try{
            unset($where['p.delete_time']);
            $data['count'] = $Order->alias('o')->where($where)->count();
        }catch (Exception $e){}
        $data['list'] = $list;
        $data['page'] = $page;
        $data['limit'] = $limit;
        $list?$this->success('获取成功',$data):$this->error('获取失败');
    }*/

    /**
     * 公司已查看简历
     * @throws \think\Exception
     */
    public function order_check(){
        $id = $this->request->param('id',0,'intval');
        $user_id = request()->uid;

        if(!$info= Db::table('cmf_job_order_post')->where(['id'=>$id])->find()) $this->error('没有该申请');
        Db::startTrans();
        try{
            $comp = Db::table('cmf_company')->where('id',$info['company_id'])->value('company');
            $position = Db::table('cmf_job_position')->where('id',$info['position_id'])->value('name');
            Db::table('cmf_job_order_post')->where(['id'=>$id])->update(['status'=>2]);

            send_message(
                $user_id,//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您投递的全职岗位“'.$position.'”有新进展！',//信息标题
                '@'.$comp.'查看了您的简历！',//信息内容
                config('HR_PARK_MSG'),//redis前缀
                $this->wx_user_order_url.'?id='.$id//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $this->success('更改成功');
        exit;
    }

    /**
     * 公司对简历有意向
     * @throws \think\Exception
     */
    public function order_agree(){
        $id = $this->request->param('id',0,'intval');
        $user_id = request()->uid;

        if(!$info= Db::table('cmf_job_order_post')->where(['id'=>$id])->find()) $this->error('没有该申请');
        Db::startTrans();
        try{
            $comp = Db::table('cmf_company')->where('id',$info['company_id'])->value('company');
            $position = Db::table('cmf_job_position')->where('id',$info['position_id'])->value('name');
            Db::table('cmf_job_order_post')->where(['id'=>$id])->update(['status'=>3]);

            send_message(
                $user_id,//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您投递的全职岗位“'.$position.'”有新进展！',//信息标题
                '@'.$comp.'对您的简历很感兴趣，请留意电话联系！',//信息内容
                config('HR_PARK_MSG'),//redis前缀
                $this->wx_user_order_url.'?id='.$id//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $this->success('更改成功');
        exit;
    }

    /**
     * 公司觉得简历不合适
     * @throws \think\Exception
     */
    public function order_refuse(){
        $id = $this->request->param('id',0,'intval');
        $user_id = request()->uid;

        if(!$info= Db::table('cmf_job_order_post')->where(['id'=>$id])->find()) $this->error('没有该申请');
        Db::startTrans();
        try{
            $comp = Db::table('cmf_company')->where('id',$info['company_id'])->value('company');
            $position = Db::table('cmf_job_position')->where('id',$info['position_id'])->value('name');
            Db::table('cmf_job_order_post')->where(['id'=>$id])->update(['status'=>4]);

            send_message(
                $user_id,//发送者ID
                $info['user_id'],//接收者ID
                $this->table_name,//对象表
                $info['id'],//对象ID
                '您投递的全职岗位“'.$position.'”有新进展！',//信息标题
                '@'.$comp.'感觉您的简历不太合适！',//信息内容
                config('HR_PARK_MSG'),//redis前缀
                $this->wx_user_order_url.'?id='.$id//路径
            );//消息提醒

            // 提交事务
            Db::commit();
        } catch (Exception $e) {
            // 回滚事务
            Db::rollback();
            return $e->getMessage();
        }

        $this->success('更改成功');
        exit;
    }

    //企业兼职下的申请人
    public function position_order(Order $Order,$position_id = 0,$page = 0,$limit = 10){
        $where = ['o.position_id'=>$position_id,'o.delete_time'=>0];
        try{
            $data['count'] = $Order->alias('o')->where($where)->count();
            $data['list'] = $Order->alias('o')
                ->join('cmf_user u','o.user_id = u.id')
                ->field('o.id,o.user_id,o.position_id,u.user_nickname,u.real_name,u.avatar')
                ->where($where)->limit(((int) $page * (int) $limit), $limit)->select()->toArray();
        }catch (Exception $e){
            $this->error('系统错误');
        }
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
    }

    /**
     * 本人的求职招聘消息
     * @throws \think\Exception
     */
    /*public function msg_list(){
        $page = $this->request->param('page',0,'intval');
        $limit = $this->request->param('limit',10,'intval');
        $user_id = request()->uid;
        $data['count'] = Db::table('cmf_user_msg')->where(['to_user_id'=>$user_id,'table_name'=>$this->table_name])->count();
        $data['list'] = Db::table('cmf_user_msg')->where(['to_user_id'=>$user_id,'table_name'=>$this->table_name])->limit($page*$limit,$limit)->order('create_time desc')->select();
        $data['page'] = $page;
        $data['limit'] = $limit;
        $this->success('获取成功',$data);
    }*/

    /**
     * 点击查看消息后标为已读消息
     * @throws \think\Exception
     */
    /*public function read_msg(){
        $id = $this->request->param('id',0,'intval');
        Db::table('cmf_user_msg')->where('id',$id)->update('');
    }*/
}