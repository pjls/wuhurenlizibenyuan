<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: pjls
// +----------------------------------------------------------------------
namespace app\job\controller;

use cmf\controller\AdminBaseController;
use app\job\model\Type;

class TypeController extends AdminBaseController
{
    /**
     * 工作类型管理
     * @adminMenu(
     *     'name'   => '工作类型管理',
     *     'parent' => '',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => 'file',
     *     'remark' => '工作类型管理',
     *     'param'  => ''
     * )
     */
    public function index()
    {
        $Type  =  new Type;
        try{
            $list = $Type->paginate();
        }catch (\Exception $e) {
            return $e;
        }

        $this->assign("arrStatus", $Type::$STATUS);
        $this->assign("list", $list);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    /**
     * 添加工作类型
     * @adminMenu(
     *     'name'   => '添加工作类型',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加工作类型',
     *     'param'  => ''
     * )
     */
    public function add()
    {
        $Type  =  new Type;
        $this->assign("arrStatus", $Type::$STATUS);
        return $this->fetch();
    }

    /**
     * 添加工作类型提交
     * @adminMenu(
     *     'name'   => '添加工作类型提交',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加工作类型提交',
     *     'param'  => ''
     * )
     */
    public function addPost()
    {
        $Type  =  new Type;

        $arrData = $this->request->param();
        $Type->isUpdate(false)->allowField(true)->save($arrData);

        $this->success('添加成功');

    }

    /**
     * 更新工作类型状态
     * @adminMenu(
     *     'name'   => '更新工作类型状态',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '更新工作类型状态',
     *     'param'  => ''
     * )
     */
    public function upStatus()
    {
        $Type  =  new Type;
        $intId     = $this->request->param("id");
        $intStatus = $this->request->param("status");
        $intStatus = $intStatus ? 1 : 0;
        if (empty($intId)) {
            $this->error('没有ID');
        }
        $Type->isUpdate(true)->save(["status" => $intStatus], ["id" => $intId]);

        $this->success('操作成功');

    }

}